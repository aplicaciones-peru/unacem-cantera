-- phpMyAdmin SQL Dump
-- version 5.0.4
-- https://www.phpmyadmin.net/
--
-- Servidor: localhost
-- Tiempo de generación: 10-02-2021 a las 22:01:26
-- Versión del servidor: 5.7.32
-- Versión de PHP: 7.4.13

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Base de datos: `cantera`
--

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `alumnos`
--

CREATE TABLE `alumnos` (
  `id` int(11) UNSIGNED NOT NULL,
  `dni` varchar(50) DEFAULT NULL,
  `apellido_paterno` varchar(50) DEFAULT NULL,
  `apellido_materno` varchar(50) DEFAULT NULL,
  `nombres` varchar(50) DEFAULT NULL,
  `Sexo` varchar(50) DEFAULT NULL,
  `Email` varchar(100) DEFAULT NULL,
  `Telefono` varchar(50) DEFAULT NULL,
  `Celular` varchar(50) DEFAULT NULL,
  `Fecha de Nacimiento` varchar(50) DEFAULT NULL,
  `codigo` varchar(50) DEFAULT NULL,
  `notas` int(11) DEFAULT NULL,
  `password` varchar(50) DEFAULT NULL,
  `grupo` tinyint(4) DEFAULT '0'
) ENGINE=InnoDB AVG_ROW_LENGTH=163 DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `alumnos`
--

INSERT INTO `alumnos` (`id`, `dni`, `apellido_paterno`, `apellido_materno`, `nombres`, `Sexo`, `Email`, `Telefono`, `Celular`, `Fecha de Nacimiento`, `codigo`, `notas`, `password`, `grupo`) VALUES
(1, '46134693', 'QUISQUICHE', 'AVILA', 'JULIO LUIS', 'MASCULINO', 'julioxvoltio@gmail.com', NULL, '956255846', NULL, 'a46134693@capeco.edu.pe', 0, 'macromedia', 1),
(2, '07487140', 'CHAMORRO', 'RAYGAL', 'LUIS ENRIQUE', 'MASCULINO', 'rentalmacsac@gmail.com', NULL, '998155036', NULL, 'a07487140@capeco.edu.pe', 0, 'Virtual-01', 1),
(3, '09736765', 'ROMERO', 'DIAZ', 'ENRIQUE', 'MASCULINO', 'romerodiazenrique@gmail.com', NULL, '995353030', NULL, 'a09736765@capeco.edu.pe', 1, 'Virtual-01', 1),
(4, '08419161', 'CALDERON', 'CAJAHUARINGA', 'MIGUEL CLAVER', 'MASCULINO', 'miguelito.ms115@gmail.com', NULL, '988554112', NULL, 'a08419161@capeco.edu.pe', 1, 'Virtual-01', 1),
(5, '25492671', 'GUTIERREZ', 'CORDOVA', 'GREGORIO', 'MASCULINO', 'walter_08_25@hotmail.com', NULL, '985298236', NULL, 'a25492671@capeco.edu.pe', 0, 'Virtual-01', 1),
(6, '09279947', 'CHALQUE', 'CHALCO', 'JORGE RICARDO', 'MASCULINO', 'jorgechr04@gmail.com', NULL, '991615503', NULL, 'a09279947@capeco.edu.pe', 0, 'Virtual-01', 1),
(7, '40504030', 'ESPEJO', 'PILLACA', 'CARLOS', 'MASCULINO', 'carlosespejopillaca@gmail.com', NULL, '997605612', NULL, 'a40504030@capeco.edu.pe', 1, 'Virtual-01', 1),
(8, '25641191', 'PALOMINO', 'RAYMUNDO', 'HECTOR GASTON', 'MASCULINO', 'hector.palomino.raymundo@hotmail.com', '963396188', '936205444', NULL, 'a25641191@capeco.edu.pe', 0, 'Virtual-01', 1),
(9, '25502632', 'VASQUEZ', 'SIFUENTES', 'HECTOR HERMES', 'MASCULINO', 'hectvasquez09@gmail.com', NULL, '980389818', NULL, 'a25502632@capeco.edu.pe', 0, 'Virtual-01', 1),
(10, '43390115', 'CHOQQUE', 'LEANDRO', 'JUAN PABLO', 'MASCULINO', 'juan22_12@hotmail.com', NULL, '927788206', NULL, 'a43390115@capeco.edu.pe', 0, 'Virtual-01', 1),
(11, '45530541', 'DIAZ', 'ESPINOZA', 'OSCAR', 'MASCULINO', 'oscar.d.espinoza12@gmail.com', NULL, '966409912', NULL, 'a45530541@capeco.edu.pe', 0, 'Virtual-01', 1),
(12, '41944175', 'PAICO', 'SANTAMARIA', 'ALVARO ANTONIO', 'MASCULINO', 'alvaro.paico.santamario@hotmail.com', NULL, '993826094', NULL, 'a41944175@capeco.edu.pe', 0, 'Virtual-01', 1),
(13, '07579892', 'CARRANZA', 'AGREDA', 'RAMON', 'MASCULINO', 'Ramon.carranza.agreda@outlook.com', NULL, '996951012', NULL, 'a07579892@capeco.edu.pe', 0, 'Virtual-01', 1),
(14, '16660228', 'CORDOVA', 'ALVARADO', 'AQUILES', 'MASCULINO', 'aquiles.cordova.a10@gmail.com', NULL, '999141240', NULL, 'a16660228@capeco.edu.pe', 0, 'Virtual-01', 1),
(15, '09625312', 'PEREZ', 'LOZANO', 'GUILLERMO', 'MASCULINO', 'glozano1711@gmail.com', NULL, '995479527', NULL, 'a09625312@capeco.edu.pe', 0, 'Virtual-01', 1),
(16, '42496508', 'ZAMORA', 'RUIZ', 'JOSE PERCY', 'MASCULINO', 'josepercyzamoraruiz78@gmail.com', NULL, '992428070', NULL, 'a42496508@capeco.edu.pe', 0, 'Virtual-01', 1),
(17, '09467841', 'MARMOLEJO', 'ARTEAGA', 'MANUEL', 'MASCULINO', 'manuelmarmolejo1967@gmail.com', NULL, '988095505', NULL, 'a09467841@capeco.edu.pe', 1, 'Virtual-01', 1),
(18, '44415963', 'PACHECO', 'BARTRA', 'LUIS MANUEL', 'MASCULINO', 'Charlieivana22@gmail.com', NULL, '926242865', NULL, 'a44415963@capeco.edu.pe', 1, 'Virtual-01', 1),
(19, '10747468', 'FLORIAN', 'MONSALVE', 'RAUL', 'MASCULINO', 'r.florian2844@yahoo.com', NULL, '993955402', NULL, 'a10747468@capeco.edu.pe', 0, 'Virtual-01', 1),
(20, '06840761', 'PONCE', 'GARCIA', 'LINDER FERNANDO', 'MASCULINO', 'l.ponce1980@yahoo.com', NULL, '996337903', NULL, 'a06840761@capeco.edu.pe', 0, 'Virtual-01', 1),
(21, '46299392', 'SORIANO', 'FERNANDEZ', 'ROBERTO', 'MASCULINO', 'sorianifernandez04@gmail.com', NULL, '967350230', NULL, 'a46299392@capeco.edu.pe', 0, 'Virtual-01', 1),
(22, '41114887', 'PAEZ', 'PAUCAR', 'EVER', 'MASCULINO', 'e.paez.paucar1@gmail.com', NULL, '999883153', NULL, 'a41114887@capeco.edu.pe', 0, 'Virtual-01', 1),
(23, '43198662', 'FACUNDO', 'MARIANO', 'EMERSON', 'MASCULINO', 'facundo.mariano.1984@gmail.com', NULL, '932460645', NULL, 'a43198662@capeco.edu.pe', 0, 'Virtual-01', 1),
(24, '08723039', 'PEREZ', 'TAPIA', 'FELICIANO', 'MASCULINO', 'seporsiempre27@gmail.com', NULL, '954799821', NULL, 'a08723039@capeco.edu.pe', 0, 'Virtual-01', 1),
(25, '40434556', 'BENIGNO', 'VELASQUEZ', 'LUIS MANUEL', 'MASCULINO', 'luis.benigno.velasquez@gmail.com', NULL, '940923321', NULL, 'a40434556@capeco.edu.pe', 0, 'Virtual-01', 1),
(26, '80125277', 'CHIPANA', 'DURAND', 'FORTUNATO LUIS', 'MASCULINO', 'fluischipanaedurand@gmail.com', NULL, '947314509', NULL, 'a80125277@capeco.edu.pe', 0, 'Virtual-01', 1),
(27, '42283980', 'SOSA', 'PEREZ', 'EDWIN ALBERTO', 'MASCULINO', 'unicamente1983@outlook.com', NULL, '934492964', NULL, 'a42283980@capeco.edu.pe', 0, 'Virtual-01', 1),
(28, '19426476', 'LOPEZ', 'GUZMAN', 'JUAN MODESTO', 'MASCULINO', 'Juan.m.Lopez.Guzman.1@gmail.com', NULL, '971088224', NULL, 'a19426476@capeco.edu.pe', 0, 'Virtual-01', 1),
(29, '33344475', 'ALVA', 'OBREGON', 'EZEQUIEL GREGORIO', 'MASCULINO', 'ezequielallvaobregon@gmail.com', NULL, '983267260', NULL, 'a33344475@capeco.edu.pe', 0, 'Virtual-01', 1),
(30, '10048680', 'COPACONDORI', 'CCAPA', 'GRACIANO', 'MASCULINO', 'gracianocopacondori@gmail.com', NULL, '994104152', NULL, 'a10048680@capeco.edu.pe', 0, 'Virtual-01', 1),
(31, '40595126', 'PONTE', 'VARGAS', 'EFRAIN RICARDO', 'MASCULINO', 'construccionespontesac@hotmail.com', NULL, '951417357', NULL, 'a40595126@capeco.edu.pe', 0, 'Virtual-01', 1),
(32, '06265090', 'CERNA', 'BRONCANO', 'ZENON BRONCANO', 'MASCULINO', 'zenon.cerna@outlook.com', NULL, '936544236', NULL, 'a06265090@capeco.edu.pe', 0, 'Virtual-01', 1),
(33, '41474863', 'CALSIN', 'MAMANI', 'JAIME', 'MASCULINO', 'jaimecalsingym@gmail.com', NULL, '933643812', NULL, 'a41474863@capeco.edu.pe', 1, 'Virtual-01', 1),
(34, '46643184', 'QUISPE', 'DIAZ', 'VILZO ANTONIO', 'MASCULINO', 'antonioqdiaz.0205@gmail.com', NULL, '951713840', NULL, 'a46643184@capeco.edu.pe', 0, 'Virtual-01', 1),
(35, '08388513', 'PINO', 'ENRIQUE', 'SANTIAGO', 'MASCULINO', 'serviciosgeneralesermur@hotmail.com', NULL, '981512750', NULL, 'a08388513@capeco.edu.pe', 0, 'Virtual-01', 1),
(36, '09743346', 'TOCAS', 'CHUQUIMANGO', 'ANTONIO', 'MASCULINO', 'toÃ±otocas600@hotmail.com', NULL, '955404916', NULL, 'a09743346@capeco.edu.pe', 1, 'Virtual-01', 1),
(37, '10538785', 'INGA', 'CORMAN', 'FLEMING BERNARDO', 'MASCULINO', 'flemingbernardoingacorman@gmail.com', NULL, '952962538', NULL, 'a10538785@capeco.edu.pe', 0, 'Virtual-01', 1),
(38, '09744447', 'TORRES', 'VASQUEZ', 'ELVER JESUS', 'MASCULINO', 'eltova@hotmail.com', NULL, '941587009', NULL, 'a09744447@capeco.edu.pe', 1, 'Virtual-01', 1),
(39, '07609645', 'FREITAS', 'PEREZ', 'ALEX WAGNER', 'MASCULINO', 'a.freitas.perez@gmail.com', NULL, '994751524', NULL, 'a07609645@capeco.edu.pe', 0, 'Virtual-01', 1),
(40, '06853785', 'YSLACHÃN', 'CASTRO', 'ABELARDO RAFAEL', 'MASCULINO', 'a.yslachin.castro@gmail.com', NULL, '992957071', NULL, 'a06853785@capeco.edu.pe', 0, 'Virtual-01', 1),
(41, '06851545', 'HERENCIA', 'VARGAS', 'MELCHOR', 'MASCULINO', 'm.melchor55.herencia@gmail.com', NULL, '985656056', NULL, 'a06851545@capeco.edu.pe', 1, 'Virtual-01', 1),
(42, '07079670', 'ABANTO', 'ARAUJO', 'LORENZO', 'MASCULINO', 'lorenzoabanto246@gmail.com', NULL, '928610024', NULL, 'a07079670@capeco.edu.pe', 0, 'Virtual-01', 1),
(43, '09783666', 'TELLO', 'MEDINA', 'JUAN CARLOS', 'MASCULINO', 'jctm1234567@gmail.com', NULL, '991231740', NULL, 'a09783666@capeco.edu.pe', 0, 'Virtual-01', 1),
(44, '07973229', 'COTRINA', 'URBINA', 'JOSE ERMOGENES', 'MASCULINO', 'joseermogenescotrina@hotmail.com', NULL, '995942926', NULL, 'a07973229@capeco.edu.pe', 1, 'Virtual-01', 1),
(45, '45257616', 'TERRONES', 'VEGA', 'JOSE ENRIQUE', 'MASCULINO', 'enrique.tervea@gmail.com', NULL, '961893514', NULL, 'a45257616@capeco.edu.pe', 1, 'Virtual-01', 1),
(46, '42784287', 'PRINCIPE', 'OCHOA', 'LUIS ALBERTO', 'MASCULINO', 'principeochoa@gmail.com', NULL, '998113653', NULL, 'a42784287@capeco.edu.pe', 0, 'Virtual-01', 1),
(47, '44183010', 'ALBARRACIN', 'GUERRA', 'NELSON CLAUDIO', 'MASCULINO', 'nelsonclaudioalbarracin@outlook.com', NULL, '991102521', NULL, 'a44183010@capeco.edu.pe', 0, 'Virtual-01', 1),
(48, '21140354', 'BALDEON', 'AMARO', 'OSCAR RAUL', 'MASCULINO', 'oscarbaldeonamaro99@gmail.com', NULL, '930321158', NULL, 'a21140354@capeco.edu.pe', 0, 'Virtual-01', 1),
(49, '33808484', 'OCSOLON', 'VELAYARCE', 'OSCAR EUGENIO', 'MASCULINO', 'o.ocsolon@gmail.com', NULL, '990300313', NULL, 'a33808484@capeco.edu.pe', 0, 'Virtual-01', 1),
(50, '80377509', 'FELIPE', 'ROJAS', 'DAMAZO', 'MASCULINO', 'feliperojasdamazo@gmail.com', NULL, '983568981', NULL, 'a80377509@capeco.edu.pe', 0, 'Virtual-01', 1),
(51, '42289421', 'REQUEJO', 'VASQUEZ', 'OSCAR CRISTIAN', 'MASCULINO', 'constructoracs84@gmail.com', NULL, '965272561', NULL, 'a42289421@capeco.edu.pe', 0, 'Virtual-01', 1),
(52, '08466346', 'CURO', 'HUAYHUA', 'RICARDO ERNESTO', 'MASCULINO', 'rconstruccionesgenerales@gmail.com', NULL, '979072389', NULL, 'a08466346@capeco.edu.pe', 0, 'Virtual-01', 1),
(53, '04210096', 'FUSTER', 'MATEO', 'ITALO', 'MASCULINO', 'italofustermateo@gmail.com', NULL, '913788382', NULL, 'a04210096@capeco.edu.pe', 0, 'Virtual-01', 1),
(54, '20113218', 'OSORIO', 'MENDOZA', 'ANDRES BENERICTO', 'MASCULINO', 'andresosorio836@gmail.com', NULL, '991777117', NULL, 'a20113218@capeco.edu.pe', 1, 'Virtual-01', 1),
(55, '15434077', 'BARTOLO', 'RAMOS', 'JOSE ARMANDO', 'MASCULINO', 'j.bartolo.ramos@outlook.com', NULL, '930671112', NULL, 'a15434077@capeco.edu.pe', 0, 'Virtual-01', 1),
(56, '09363789', 'HUAMAN', 'RAMIREZ', 'MARTIN ALEJANDRO', 'MASCULINO', 'huamanramirezmartinalejandro@gmail.com', NULL, '925480964', NULL, 'a09363789@capeco.edu.pe', 0, 'Virtual-01', 1),
(57, '07894115', 'BOZA', 'HUAYHUARIMA', 'MOISES', 'MASCULINO', 'moisesboza28@gmail.com', NULL, '983556734', NULL, 'a07894115@capeco.edu.pe', 1, 'Virtual-01', 1),
(58, '07894376', 'HUAMAN', 'PEÃ‘A', 'FELIX RAPHAEL', 'MASCULINO', 'raphael.huamanp@gmail.com', NULL, '991407225', NULL, 'a07894376@capeco.edu.pe', 1, 'Virtual-01', 1),
(59, '10125068', 'ARONE', 'TOMAYLLA', 'MILCIADES', 'MASCULINO', 'arone-72@hotmail.com', NULL, '946262985', NULL, 'a10125068@capeco.edu.pe', 1, 'Virtual-01', 1),
(60, '09655813', 'VALVERDE', 'RUIZ', 'RUFINO', 'MASCULINO', 'rufinovalverderuiz9@hotmail.com', NULL, '981519484', NULL, 'a09655813@capeco.edu.pe', 1, 'Virtual-01', 1),
(61, '10412496', 'OLORTEGUI', 'GUARDIA', 'HECTOR FRANCISCO', 'MASCULINO', 'holorteguiguardia@gmail.com', NULL, '988409657', NULL, 'a10412496@capeco.edu.pe', 0, 'Virtual-01', 1),
(62, '43394407', 'VIDAL', 'VEGA', 'MARCO ANTONIO', 'MASCULINO', 'meliliam30@gmail.com', NULL, '996660822', NULL, 'a43394407@capeco.edu.pe', 0, 'Virtual-01', 1),
(63, '09484966', 'CASTILLO', 'CALVO', 'HERNAN NORBERTO', 'MASCULINO', 'hernannorberto@gmail.com', NULL, '945574337', NULL, 'a09484966@capeco.edu.pe', 0, 'Virtual-01', 1),
(64, '10397137', 'RAMIREZ', 'AREVALO', 'SEGUNDO SANTIAGO', 'MASCULINO', 'segundoramirez492@yahoo.com', NULL, '975337870', NULL, 'a10397137@capeco.edu.pe', 0, 'Virtual-01', 1),
(65, '10585519', 'CHAVEZ', 'RAMOS', 'JORGE RAUL', 'MASCULINO', 'jm_sac@hotmail.com', NULL, '994739808', NULL, 'a10585519@capeco.edu.pe', 1, 'Virtual-01', 1),
(66, '27646874', 'SAAVEDRA', 'TENORIO', 'MARINOS', 'MASCULINO', 'saavedramarino39@gmail.com', NULL, '997680473', NULL, 'a27646874@capeco.edu.pe', 0, 'Virtual-01', 1),
(67, '25542503', 'MARTINEZ', 'GOMEZ', 'MAURICIO AMANCIO', 'MASCULINO', 'mauriciomartinezg13@gmail.com', NULL, '995083255', NULL, 'a25542503@capeco.edu.pe', 1, 'Virtual-01', 1),
(68, '80365662', 'BARRERA', 'PEREZ', 'EDGAR', 'MASCULINO', 'edgarbarreraperez@gmail.com', NULL, '992671884', NULL, 'a80365662@capeco.edu.pe', 0, 'Virtual-01', 1),
(69, '08083149', 'ZAMBRANO', 'GUERRERO', 'FRANCISCO CARLOS', 'MASCULINO', 'franciscozambranoguerrero19@outlook.com', NULL, '947449791', NULL, 'a08083149@capeco.edu.pe', 0, 'Virtual-01', 1),
(70, '06886198', 'TARRILLO', 'IPARRAGUIRRE', 'SEGUNDO VICTOR', 'MASCULINO', 'gtarrillod@gmail.com', '992909769', '976116054', NULL, 'a06886198@capeco.edu.pe', 0, 'Virtual-01', 1),
(71, '08701353', 'MAYHUASCA', 'BENDEZU', 'DAVID', 'MASCULINO', 'dmayhuasca@hotmail.com', NULL, '992619341', NULL, 'a08701353@capeco.edu.pe', 0, 'Virtual-01', 1),
(72, '09304038', 'DIAZ', 'LEON', 'ANGEL', 'MASCULINO', 'ad4923479@gmail.com', NULL, '997167554', NULL, 'a09304038@capeco.edu.pe', 0, 'Virtual-01', 1),
(73, '10118692', 'CALIXTO', 'BAUTISTA', 'LEONCIO', 'MASCULINO', 'retassac@gmail.com', NULL, '994947151', NULL, 'a10118692@capeco.edu.pe', 1, 'Virtual-01', 1),
(74, '46821009', 'FERNANDEZ', 'SALDAÃ‘A', 'JESUS TEOFILO', 'MASCULINO', 'jfernandez123@hotmail.com', NULL, '937700561', NULL, 'a46821009@capeco.edu.pe', 0, 'Virtual-01', 1),
(75, '10029137', 'MUÃ‘OZ', 'FLORINDEZ', 'FELIX SANTOS', 'MASCULINO', 'alex_darmuÃ±oz201@hotmail.com', NULL, '986427046', NULL, 'a10029137@capeco.edu.pe', 0, 'Virtual-01', 1),
(76, '10037638', 'HUARCAYA', 'BASILIO', 'HILARIO', 'MASCULINO', 'hilario.huarcaya.b@gmail.com', NULL, '997017249', NULL, 'a10037638@capeco.edu.pe', 0, 'Virtual-01', 1),
(77, '25549324', 'QUITO', 'BERNUY', 'DESIDERIO', 'MASCULINO', 'jonasquito@gmail.com', NULL, '942292310', NULL, 'a25549324@capeco.edu.pe', 0, 'Virtual-01', 1),
(78, '44475327', 'LUNA', 'TAPIA', 'ELMER BENJAMIN', 'MASCULINO', 'elmer_virgo_25@hotmail.com', NULL, '993914778', NULL, 'a44475327@capeco.edu.pe', 0, 'Virtual-01', 1),
(79, '76196332', 'Asmat', 'Espinoza', 'Christian Walter', 'MASCULINO', 'Christian.asmat.6.once@gmail.com', NULL, '977316500', NULL, 'a76196332@capeco.edu.pe', 1, 'Virtual-01', 1),
(80, '45850507', 'GONZALES', 'MONDRAGON', 'JOSE LUIS', 'MASCULINO', 'joseluismondragon@hotmail.com', NULL, '942435925', NULL, 'a45850507@capeco.edu.pe', 0, 'Virtual-01', 1),
(81, '10190103', 'SANDOVAL', 'TICLIA', 'SANTOS JUAN', 'MASCULINO', 'yymv53@gmail.com', NULL, '988181583', NULL, 'a10190103@capeco.edu.pe', 0, 'Virtual-01', 1),
(82, '06939227', 'PUSCAN', 'QUISTAN', 'JOSE CACELES', 'MASCULINO', 'j.puscan9599@yahoo.com', NULL, '992308375', NULL, 'a06939227@capeco.edu.pe', 0, 'Virtual-01', 1),
(83, '09883382', 'COTRINA', 'ZAFRA', 'TEOFILO', 'MASCULINO', 'teofilocotrina@hotmai.com', NULL, '945914468', NULL, 'a09883382@capeco.edu.pe', 1, 'Virtual-01', 1),
(84, '10397910', 'ESPINOZA', 'GILIO', 'FRANCISCO RAFAEL', 'MASCULINO', 'rafael2275@hotmail.com', NULL, '999910047', NULL, 'a10397910@capeco.edu.pe', 1, 'Virtual-01', 1),
(85, '09614194', 'BALDEON', 'PARES', 'SEVERIANO ALFREDO', 'MASCULINO', 'severiano.al1971@gmail.com', NULL, '980108353', NULL, 'a09614194@capeco.edu.pe', 1, 'Virtual-01', 1),
(86, '46852783', 'GALOC', 'GOÃ‘AS', 'SEGUNDO VENIGNO', 'MASCULINO', 'galocsegundo@hotmail.com', NULL, '934066503', NULL, 'a46852783@capeco.edu.pe', 0, 'Virtual-01', 1),
(87, '10457760', 'VALERIO', 'CASTAÃ‘EDA', 'OSCAR RAFAEL', 'MASCULINO', 'oscar26@gmail.com', NULL, '993290411', NULL, 'a10457760@capeco.edu.pe', 0, 'Virtual-01', 1),
(88, '06022218', 'ALFARO', 'ZARABIA', 'DELFIN', 'MASCULINO', 'delfin.alfaro.zarabia@gmail.com', NULL, '992327037', NULL, 'a06022218@capeco.edu.pe', 1, 'Virtual-01', 1),
(89, '80339336', 'CASTILLO', 'CHUQUICONDOR', 'WILMAN', 'MASCULINO', 'wilman.castillo.chuquicondor@hotmail.com', NULL, '978544862', NULL, 'a80339336@capeco.edu.pe', 0, 'Virtual-01', 1),
(90, '41907738', 'YAFAC', 'NECIOSUP', 'MARTIN', 'MASCULINO', 'yafacyosuc811@gmail.com', NULL, '995689652', NULL, 'a41907738@capeco.edu.pe', 1, 'Virtual-01', 1),
(91, '43032089', 'ROMERO', 'FIGUEROA', 'WILMER ABRAHAM', 'MASCULINO', 'felicitaciones_romero@outlook.com', NULL, '978380916', NULL, 'a43032089@capeco.edu.pe', 0, 'Virtual-01', 1),
(92, '80302872', 'PIZARRO', 'TAFUR', 'MARTIN', 'MASCULINO', 'rosio25pizarro@gmail.com', NULL, '991698843', NULL, 'a80302872@capeco.edu.pe', 0, 'Virtual-01', 1),
(93, '10067480', 'PENADILLO', 'ROJAS', 'TEODOSIO', 'MASCULINO', 'teodosio.penadillo@gmail.com   ', NULL, '941493286', NULL, 'a10067480@capeco.edu.pe', 1, 'Virtual-01', 1),
(94, '48585308', 'VILCHEZ', 'RIMARACHIN', 'GEILER', 'MASCULINO', 'geiler_vilchez@gmail.com', NULL, '972436669', NULL, 'a48585308@capeco.edu.pe', 0, 'Virtual-01', 1),
(95, '25821520', 'CASTRO', 'BAZAN', 'CARACCIOLO', 'MASCULINO', 'caracciolocastrobazan@gmail.com', NULL, '954974688', NULL, 'a25821520@capeco.edu.pe', 1, 'Virtual-01', 1),
(96, '07737674', 'ACUÃ‘A  ', 'DURAND', 'VALERIO', 'MASCULINO', 'valerio.a.durand@gmail.com', NULL, '994441862', NULL, 'a07737674@capeco.edu.pe', 0, 'Virtual-01', 1),
(97, '43322633', 'CAMANA', 'TINEO', 'SAUL CELEDONIO', 'MASCULINO', 'camanatineosaul@gmail.com', NULL, '992911630', NULL, 'a43322633@capeco.edu.pe', 0, 'Virtual-01', 1),
(98, '06749693', 'JIRON', 'CUYA', 'ALEJANDRO FREDDY', 'MASCULINO', 'freddyjironcuya@gmail.com', NULL, '927422108', NULL, 'a06749693@capeco.edu.pe', 0, 'Virtual-01', 1),
(99, '08913860', 'SOTAYO', 'MICOILLA', 'NEMECIO', 'MASCULINO', 'nemesiosotaya@gmail.com', NULL, '959481711', NULL, 'a08913860@capeco.edu.pe', 1, 'Virtual-01', 1),
(100, '09115162', 'ARANA', 'QUIROZ', 'ERNESTO', 'MASCULINO', 'ernestoarcuy19@hotmail.com', NULL, '949405523', NULL, 'a09115162@capeco.edu.pe', 1, 'Virtual-01', 1),
(101, '10460198', 'AGÜERO', ' ZEVALLOS', 'HUMBERTO CARLOS', 'MASCULINO', 'humberto_carlos1976@hotmail.com', '991260712', NULL, '18/09/1976 00:00:00', 'a10460198@capeco.edu.pe', 0, 'Virtual-01', 2),
(102, '10485769', 'ALARCON', 'HUALLANCA', 'DIONICIO', 'MASCULINO', 'alarconhuallancadionicio@gmail.com', '990676342', NULL, '23/12/1967 00:00:00', 'a10485769@capeco.edu.pe', 0, 'Virtual-01', 2),
(103, '09745596', 'APONTE', ' TAPIA', 'ALEJANDRO DONATO', 'MASCULINO', 'donatoaponte29@hotmail.com', '996117000', NULL, '28/03/1970 00:00:00', 'a09745596@capeco.edu.pe', 0, 'Virtual-01', 2),
(104, '44770598', 'ARELLANO', ' CHAVEZ', 'RUBEN ELMER', 'MASCULINO', 'rbarellano1088@gmail.com', '945494667', NULL, '10/01/1988 00:00:00', 'a44770598@capeco.edu.pe', 0, 'Virtual-01', 2),
(105, '08917998', 'ARROYO', 'GAVILAN', 'CIRILO', 'MASCULINO', 'ciriloarroyo@hotmail.com', '934490182', NULL, '29/03/1964 00:00:00', 'a08917998@capeco.edu.pe', 0, 'Virtual-01', 2),
(106, '43421144', 'ASTO', 'ALVAREZ', 'FELIX DEMETRIO', 'MASCULINO', 'felixasto221279@hotmail.com', '955179807', NULL, '22/12/1979 00:00:00', 'a43421144@capeco.edu.pe', 0, 'Virtual-01', 2),
(107, '09212836', 'BALTODANO', 'MURGA', 'JULIO', 'MASCULINO', 'juliobr861@gmail.com', '994463049', NULL, '22/07/1950 00:00:00', 'a09212836@capeco.edu.pe', 0, 'Virtual-01', 2),
(108, '28315572', 'BERROCAL', 'HUAMANI', 'CLEMENTE', 'MASCULINO', 'berrocalhc@gmail.com', '900155582', NULL, '18/07/1976 00:00:00', 'a28315572@capeco.edu.pe', 0, 'Virtual-01', 2),
(109, '10533640', 'BOCANEGRA', 'GONZALES', 'SANTOS ISAAC', 'MASCULINO', 'ibocanegra60g@gmail.com', '988347534', NULL, '20190', 'a10533640@capeco.edu.pe', 0, 'Virtual-01', 2),
(110, '15424588', 'BRAVO', 'QUISPE', 'MIGUEL ANGEL', 'MASCULINO', '1969bravoquispe@gmail.com', '939172331', NULL, '6/06/1969 00:00:00', 'a15424588@capeco.edu.pe', 0, 'Virtual-01', 2),
(111, '09013262', 'BRIONES', 'MIRANDA', 'FELIPE', 'MASCULINO', 'felipe_briones_miranda_8@hotmail.com', '999149676', NULL, '8/09/1965 00:00:00', 'a09013262@capeco.edu.pe', 0, 'Virtual-01', 2),
(112, '80395945', 'CABANILLAS', 'ROJAS', 'JOSE', 'MASCULINO', '111162j@hotmail.com', '931909633', NULL, '11/11/1962 00:00:00', 'a80395945@capeco.edu.pe', 0, 'Virtual-01', 2),
(113, '27928412', 'CALDERON', 'COTRINA', 'VALERIO', 'MASCULINO', 'calderonvalerio10@gmail.com', '992912765', NULL, '5/04/1975 00:00:00', 'a27928412@capeco.edu.pe', 0, 'Virtual-01', 2),
(114, '43064219', 'CALERO', 'ROJAS', 'HUGO SOTIL', 'MASCULINO', 'hugo.calero04@gmail.com', '934591915', NULL, '18/06/1985 00:00:00', 'a43064219@capeco.edu.pe', 0, 'Virtual-01', 2),
(115, '43058101', 'CAMPOS', 'VEGA', 'EDUARDO FELIX', 'MASCULINO', 'eduar_2906@hotmail.com', '963705005', NULL, '29/06/1985 00:00:00', 'a43058101@capeco.edu.pe', 0, 'Virtual-01', 2),
(116, '15724458', 'CARLOS', 'ESTRADA', 'HUMBERTO', 'MASCULINO', 'humbertocarlosestrada37@gmail.com', '988724219', NULL, '2/01/1968 00:00:00', 'a15724458@capeco.edu.pe', 0, 'Virtual-01', 2),
(117, '10460018', 'CAUSSO', 'NAUTO', 'MOISES ANTONIO', 'MASCULINO', 'moisescausso_33@hotmail.com', '996566989', NULL, '26/10/1974 00:00:00', 'a10460018@capeco.edu.pe', 0, 'Virtual-01', 2),
(118, '44520191', 'CCORA', 'CASO', 'RAMON SERAPHIO', 'MASCULINO', 'ramonccora2607@gmail.com', '979694274', NULL, '26/07/1987 00:00:00', 'a44520191@capeco.edu.pe', 0, 'Virtual-01', 2),
(119, '76057147', 'CERCADO', 'LOPEZ', 'CARLOS JHONATAN', 'MASCULINO', 'cajhocelobb@gmail.com', '931266847', NULL, '24/05/1994 00:00:00', 'a76057147@capeco.edu.pe', 0, 'Virtual-01', 2),
(120, '18982302', 'CHAVEZ', 'EPIQUEN', 'SIMON PEDRO', 'MASCULINO', 'nuncasoloe_5@hotmail.com', '955634995', NULL, '5/06/1972 00:00:00', 'a18982302@capeco.edu.pe', 0, 'Virtual-01', 2),
(121, '45877019', 'CHUQUIZUTA', 'CHUMBE', 'JULIO CESAR', 'MASCULINO', 'ereuju@gmail.com', '982899272', NULL, '31/07/1989 00:00:00', 'a45877019@capeco.edu.pe', 0, 'Virtual-01', 2),
(122, '18098094', 'CIPRIANO', 'EUSTAQUIO', 'HILDEBRANDO ALFONSO', 'MASCULINO', 'alfonsocipriano1@gmail.com', '934555785', NULL, '1/12/1968 00:00:00', 'a18098094@capeco.edu.pe', 0, 'Virtual-01', 2),
(123, '09564446', 'COLLOHUANCA', 'CARDENAS', 'JORGE', 'MASCULINO', 'collohuancajorge@gmail.com', '935751060', NULL, '7/08/1969 00:00:00', 'a09564446@capeco.edu.pe', 0, 'Virtual-01', 2),
(124, '10189457', 'CUADROS', 'CAHUANA', 'MAXIMO', 'MASCULINO', 'maxcuadros080670@gmail.com', '917708599', NULL, '8/06/1970 00:00:00', 'a10189457@capeco.edu.pe', 0, 'Virtual-01', 2),
(125, '10101585', 'CUYA', 'OMONTE', 'BERNARDO', 'MASCULINO', 'bernardocuyaomonte@gmail.com', '940314085', NULL, '26/10/1969 00:00:00', 'a10101585@capeco.edu.pe', 0, 'Virtual-01', 2),
(126, '40289343', 'DAMIAN', 'SANDOVAL', 'VICTOR CESAR', 'MASCULINO', 'victorcds1978@gmail.com', '988571779', NULL, '8/08/1978 00:00:00', 'a40289343@capeco.edu.pe', 0, 'Virtual-01', 2),
(127, '44244827', 'DE LA CRUZ', 'YANAVILCA', 'ALBERTO', 'MASCULINO', 'albertodelacruzyanavilca@gmail.com', '981352735', NULL, '16/05/2987 00:00:00', 'a44244827@capeco.edu.pe', 0, 'Virtual-01', 2),
(128, '27075078', 'DIAZ', 'RAMIREZ', 'MIGUEL ANGEL', 'MASCULINO', 'migueldiazr4@gmail.com', '996167972', NULL, '15/12/1966 00:00:00', 'a27075078@capeco.edu.pe', 0, 'Virtual-01', 2),
(129, '10378801', 'DOMINGUEZ', 'VALVERDE', 'LUIS ALBERTO', 'MASCULINO', 'luisdominguezvalverde@gmail.com', '923643119', NULL, '7/04/1974 00:00:00', 'a10378801@capeco.edu.pe', 0, 'Virtual-01', 2),
(130, '09547325', 'ESCRIBA', 'TENORIO', 'TEOFILO', 'MASCULINO', 'teofiloescribatenorio@hotmail.com', '997059765', NULL, '6/02/2020 00:00:00', 'a09547325@capeco.edu.pe', 0, 'Virtual-01', 2),
(131, '43140385', 'ESPINOZA', 'LEON', 'ALEX MAYCOL', 'MASCULINO', 'ealexmaycol@gmail.com', '985120518', NULL, '1/01/1985 00:00:00', 'a43140385@capeco.edu.pe', 0, 'Virtual-01', 2),
(132, '15733428', 'ESPINOZA', 'TORRES', 'ELMER VICENTE', 'MASCULINO', 'elmervicenteespinozatorres@hotmail.com', '985382931', NULL, '19/08/1971 00:00:00', 'a15733428@capeco.edu.pe', 0, 'Virtual-01', 2),
(133, '10070907', 'FLORES', 'ESPINOZA', 'ROLANDO MARINO', 'MASCULINO', 'rolandomarinofloresespinoza@gmail.com', '986219499', NULL, '8/07/1975 00:00:00', 'a10070907@capeco.edu.pe', 0, 'Virtual-01', 2),
(134, '10677418', 'GIL', 'CHOQUEHUANCA', 'NICOLAS', 'MASCULINO', 'giln33000@gmail.com', '934074576', NULL, '23/08/1967 00:00:00', 'a10677418@capeco.edu.pe', 0, 'Virtual-01', 2),
(135, '15292864', 'GOMEZ', 'AQUINO', 'ARTURO', 'MASCULINO', 'arturogomez68f@gmail.com', '947414647', NULL, '12/12/1965 00:00:00', 'a15292864@capeco.edu.pe', 0, 'Virtual-01', 2),
(136, '27753221', 'GOMEZ', 'RAMOS', 'ALEJANDRO', 'MASCULINO', 'alejandr.gr@gmail.com', '945046786', NULL, '12/05/1977 00:00:00', 'a27753221@capeco.edu.pe', 0, 'Virtual-01', 2),
(137, '09911230', 'GONZALES', 'CANDAMO', 'JAVIER MARTIN', 'MASCULINO', 'javichitogonzales1975@gmail.com', '990782591', NULL, '4/11/1975 00:00:00', 'a09911230@capeco.edu.pe', 0, 'Virtual-01', 2),
(138, '07167290', 'GONZALES', 'PILLACA', 'MARIO', 'MASCULINO', 'pillacamario1234@gmail.com', '945494677', NULL, '25/07/1955 00:00:00', 'a07167290@capeco.edu.pe', 0, 'Virtual-01', 2),
(139, '07685913', 'GONZALES', 'ROJAS', 'SANTOS GERARDO', 'MASCULINO', 'gonzalesrojassantos@hotmail.com', '920132169', NULL, '1/10/1968 00:00:00', 'a07685913@capeco.edu.pe', 0, 'Virtual-01', 2),
(140, '43451441', 'GUTIERREZ', 'AYQUIPA', 'ARNOL', 'MASCULINO', 'a.gutierrez5982@hotmail.com', '958096579', NULL, '5/09/1982 00:00:00', 'a43451441@capeco.edu.pe', 0, 'Virtual-01', 2),
(141, '10532338', 'HIDALGO', 'VALLES', 'FRANCISCO', 'MASCULINO', 'franhidalgovalles@gmail.com', '942146496', NULL, '10/10/1957 00:00:00', 'a10532338@capeco.edu.pe', 0, 'Virtual-01', 2),
(142, '07058425', 'HILARIO', 'MIRAVAL', 'BRAULIO', 'MASCULINO', 'brauliohilariomiraval@hotmail.com', '997074911', NULL, '26/05/1959 00:00:00', 'a07058425@capeco.edu.pe', 0, 'Virtual-01', 2),
(143, '09759254', 'HUAJARDO', ' SOTO', 'JOSE LUIS', 'MASCULINO', 'huajardojose@gmail.com', '927570159', NULL, '13/06/1971 00:00:00', 'a09759254@capeco.edu.pe', 0, 'Virtual-01', 2),
(144, '44916052', 'HUAMAN', 'POMAHUACRE', 'CARLOS', 'MASCULINO', 'carloshuaman883@gmail.com', '945996049', NULL, '4/11/1984 00:00:00', 'a44916052@capeco.edu.pe', 0, 'Virtual-01', 2),
(145, '09251064', 'HUAMAN', 'TAYÑA', 'FIDEL', 'MASCULINO', 'fidelhuaman10966@hotmail.com', '933292767', NULL, '10/09/1966 00:00:00', 'a09251064@capeco.edu.pe', 0, 'Virtual-01', 2),
(146, '07344759', 'HUAMANCULI', 'TACAS', 'ADRIAN', 'MASCULINO', 'adrianhuamanculitacas@gmail.com', '956536612', NULL, '26/08/1943 00:00:00', 'a07344759@capeco.edu.pe', 0, 'Virtual-01', 2),
(147, '10608696', 'JURADO', 'OBREGON', 'CLARISANO', 'MASCULINO', 'clarisanojuradoobregon@hotmail.com', '979112593', NULL, '12/08/1967 00:00:00', 'a10608696@capeco.edu.pe', 0, 'Virtual-01', 2),
(148, '07277471', 'LLOCLLA', 'CURO', 'VICENTE', 'MASCULINO', 'llocllavicente9@gmail.com', '986228128', NULL, '6/08/1958 00:00:00', 'a07277471@capeco.edu.pe', 0, 'Virtual-01', 2),
(149, '43381821', 'LOPEZ', 'SICCE', 'ANANIAS', 'MASCULINO', 'alopezsicce@hotmail.com', '959540121', NULL, '10/04/1982 00:00:00', 'a43381821@capeco.edu.pe', 0, 'Virtual-01', 2),
(150, '10632843', 'LUQUE', 'VASQUEZ', 'ENRIQUE', 'MASCULINO', 'eluquevasquez@gmail.com', '996708384', NULL, '18/05/1975 00:00:00', 'a10632843@capeco.edu.pe', 0, 'Virtual-01', 2),
(151, '06860294', 'MELGAREJO', 'MEZA', 'FLORENCIO', 'MASCULINO', 'florenciomelgarejomeza@gmail.com', '953678588', NULL, '17/10/1964 00:00:00', 'a06860294@capeco.edu.pe', 0, 'Virtual-01', 2),
(152, '44569523', 'MENDOZA', 'CASTRO', 'YASMANI', 'MASCULINO', 'yasmanicastro2018@hotmail.com', '941961942', NULL, '18/03/1985 00:00:00', 'a44569523@capeco.edu.pe', 0, 'Virtual-01', 2),
(153, '40505577', 'MODESTO', 'SANTACRUZ', 'JOEL FRANKLIN', 'MASCULINO', 'jofrank_79@hotmail.com', '991081978', NULL, '23/06/1979 00:00:00', 'a40505577@capeco.edu.pe', 0, 'Virtual-01', 2),
(154, '40266516', 'MONTANO', 'RICCE', 'WILSON MANUEL', 'MASCULINO', 'costruccionesgenereales.m.r@hotmail.com', '943294611', NULL, '13/04/1979 00:00:00', 'a40266516@capeco.edu.pe', 0, 'Virtual-01', 2),
(155, '42982131', 'MORY', 'JARA', 'ANTONIO JUAN', 'MASCULINO', 'antoniomory123@gmail.com', '980919345', NULL, '10/05/1985 00:00:00', 'a42982131@capeco.edu.pe', 0, 'Virtual-01', 2),
(156, '08323402', 'NEGRETE', 'RAMIREZ', 'JESUS MARCOS', 'MASCULINO', 'jesus.negrete25@hotmail.com', '990814672', NULL, '25/12/1962 00:00:00', 'a08323402@capeco.edu.pe', 0, 'Virtual-01', 2),
(157, '21121376', 'ORDOÑEZ', 'RIVERA', 'NEMESIO ORLANDO', 'MASCULINO', 'nemesio.o.ordoñez31@gmail.com', '930270335', NULL, '31/07/1971 00:00:00', 'a21121376@capeco.edu.pe', 0, 'Virtual-01', 2),
(158, '40403759', 'OROSCO', 'VARGAS', 'ISRAEL SEM', 'MASCULINO', 'oroscovargasisrael@gmail.com', '993367529', NULL, '10/09/1979 00:00:00', 'a40403759@capeco.edu.pe', 0, 'Virtual-01', 2),
(159, '41753344', 'PACHAS', 'ARANIBAR', 'WILMER ANTONIO', 'MASCULINO', 'wapachas@gmail.com', '910471390', NULL, '12/12/1982 00:00:00', 'a41753344@capeco.edu.pe', 0, 'Virtual-01', 2),
(160, '25583680', 'PALOMINO', 'TAYPE', 'ANDRES BASILIO', 'MASCULINO', 'andresbpt@gmail.com', '943736546', NULL, '29/08/1963 00:00:00', 'a25583680@capeco.edu.pe', 0, 'Virtual-01', 2),
(161, '41185546', 'PANDO', 'PACHECO', 'FRANKLIN', 'MASCULINO', 'klin_12_8@hotmail.com', '985210832', NULL, '12/04/1982 00:00:00', 'a41185546@capeco.edu.pe', 0, 'Virtual-01', 2),
(162, '72129019', 'PAREDES', 'DIAS', 'Víctor Eduardo', 'MASCULINO', 'paredesdiazeduardo@gmail.com', '980126770', NULL, '25/07/1995 00:00:00', 'a72129019@capeco.edu.pe', 0, 'Virtual-01', 2),
(163, '10228925', 'PORTUGAL', 'RAMIREZ', 'JHON EDWIN', 'MASCULINO', 'jhonportugal@hotmail.com', '992394728', NULL, '2/02/1975 00:00:00', 'a10228925@capeco.edu.pe', 0, 'Virtual-01', 2),
(164, '42252339', 'PRADO', 'CASANI', 'ERNESTO', 'MASCULINO', 'pradocasani10@gmail.com', '965407694', NULL, '9/11/1981 00:00:00', 'a42252339@capeco.edu.pe', 0, 'Virtual-01', 2),
(165, '07437847', 'QUESQUEN', 'SALVADOR', 'MARCIAL WILFREDO', 'MASCULINO', 'quesquenmarcial@gmail.com', '993381000', NULL, '13/07/1951 00:00:00', 'a07437847@capeco.edu.pe', 0, 'Virtual-01', 2),
(166, '06176097', 'QUILLA', 'PILCO', 'MARIO', 'MASCULINO', 'm.quilla@hotmail.com', '989677587', NULL, '26/11/1961 00:00:00', 'a06176097@capeco.edu.pe', 0, 'Virtual-01', 2),
(167, '15751289', 'QUILLAY', 'MOYA', 'PONCIANO ADRIAN', 'MASCULINO', 'quillaymoyaponciano10@hotmail.com', '985435388', NULL, '10/03/1973 00:00:00', 'a15751289@capeco.edu.pe', 0, 'Virtual-01', 2),
(168, '07983393', 'QUISPE', 'DE LA CRUZ', 'ACELMO', 'MASCULINO', 'acelmoquispe@gmail.com', '999858978', NULL, '17/01/1962 00:00:00', 'a07983393@capeco.edu.pe', 0, 'Virtual-01', 2),
(169, '07271068', 'QUISPE', 'PALOMINO', 'VICTOR', 'MASCULINO', 'vqp47@hotmail.com', '986713110', NULL, '15/12/1962 00:00:00', 'a07271068@capeco.edu.pe', 0, 'Virtual-01', 2),
(170, '41945296', 'RAMOS', 'LAZARO', 'MAXIMO MARCELO', 'MASCULINO', 'maximo_ramos@gmail.com', '991805467', NULL, '15/01/1983 00:00:00', 'a41945296@capeco.edu.pe', 0, 'Virtual-01', 2),
(171, '09894691', 'RIOS', 'ARIAS', 'RAUL DAVID', 'MASCULINO', 'r.rios71@gmail.com', '997045603', NULL, '26/04/1971 00:00:00', 'a09894691@capeco.edu.pe', 0, 'Virtual-01', 2),
(172, '24480368', 'ROMERO', 'ESPETIA', 'JESUS', 'MASCULINO', 'jesusromeroespetia@hotmail.com', '925144231', NULL, '20/01/1963 00:00:00', 'a24480368@capeco.edu.pe', 0, 'Virtual-01', 2),
(173, '09355135', 'ROMERO', 'SAPAYANAY', 'ESTANISLAO MANUEL', 'MASCULINO', 'estanislaomanuel@hotmail.com', '992030684', NULL, '12/01/1967 00:00:00', 'a09355135@capeco.edu.pe', 0, 'Virtual-01', 2),
(174, '45969950', 'ROMERO', 'ZELAES', 'MAYNOR WARNER', 'MASCULINO', 'maynor_warner10@yahoo.com', '948953521', NULL, '20/10/1989 00:00:00', 'a45969950@capeco.edu.pe', 0, 'Virtual-01', 2),
(175, '43918041', 'ROSALES', 'SANTIVAÑEZ', 'RUBEN CESAR', 'MASCULINO', 'rubenrosales1819@gmail.com', '976791825', NULL, '18/12/1984 00:00:00', 'a43918041@capeco.edu.pe', 0, 'Virtual-01', 2),
(176, '43188411', 'RUIZ', 'RAMOS', 'RICHARD RAFAEL', 'MASCULINO', 'richard.ruiz17@gmail.com', '934337798', NULL, '24/07/1985 00:00:00', 'a43188411@capeco.edu.pe', 0, 'Virtual-01', 2),
(177, '06089327', 'SAIZ', 'DE LA CRUZ', 'MARIO', 'MASCULINO', 'mario.s.dc@hotmail.com', '994377882', NULL, '11/10/1966 00:00:00', 'a06089327@capeco.edu.pe', 0, 'Virtual-01', 2),
(178, '08030570', 'SALAS', 'ATAHUALPA', 'RICARDO ASUNCION', 'MASCULINO', 'r.salas.a@gmail.com', '993910128', NULL, '15/08/1964 00:00:00', 'a08030570@capeco.edu.pe', 0, 'Virtual-01', 2),
(179, '42882986', 'SALAS', 'SUAREZ', 'YEMERIS JESUS', 'MASCULINO', 'yemeris.24@gmail.com', '934284311', NULL, '9/03/1985 00:00:00', 'a42882986@capeco.edu.pe', 0, 'Virtual-01', 2),
(180, '10369973', 'SALAZAR', 'YANCCE', 'FELIX', 'MASCULINO', 'felixsalazaryancce@gmail.com', '999969436', NULL, '5/11/1964 00:00:00', 'a10369973@capeco.edu.pe', 0, 'Virtual-01', 2),
(181, '80241316', 'SANCHEZ', 'MACHADO', 'ROGER SANTOS', 'MASCULINO', 'rs61011414@gmail.com', '991089668', NULL, '1/11/1979 00:00:00', 'a80241316@capeco.edu.pe', 0, 'Virtual-01', 2),
(182, '09801140', 'SANDOVAL', 'ALVARADO', 'WAGNER', 'MASCULINO', 'w.sandoval@gmail.com', '999890632', NULL, '25/03/1970 00:00:00', 'a09801140@capeco.edu.pe', 0, 'Virtual-01', 2),
(183, '22865747', 'SEGURA', 'ALARCON', 'FAUSTINO ROSMILIO', 'MASCULINO', 'kenyu.segura@gmail.com', '925881249', NULL, '15/12/1967 00:00:00', 'a22865747@capeco.edu.pe', 0, 'Virtual-01', 2),
(184, '80070266', 'SHUPINGAHUA', 'MOZOMBITE', 'ANDRES', 'MASCULINO', 'andresshupingahua@hotmail.com', '971392355', NULL, '13/05/1968 00:00:00', 'a80070266@capeco.edu.pe', 0, 'Virtual-01', 2),
(185, '07991752', 'SIFUENTES', 'SAAVEDRA', 'REMIGIO', 'MASCULINO', 'remigio.sifuentes@hotmail.com', '948479280', NULL, '1/10/1955 00:00:00', 'a07991752@capeco.edu.pe', 0, 'Virtual-01', 2),
(186, '02749806', 'SILVA', 'NAVARRO', 'PERSI', 'MASCULINO', 'persi_silva1@hotmail.com', '997390611', NULL, '15/06/1965 00:00:00', 'a02749806@capeco.edu.pe', 0, 'Virtual-01', 2),
(187, '10874597', 'SOLORZANO', 'VILLEGAS', 'JOSE CARLOS', 'MASCULINO', 'jocasolvil@gmail.com', '983259202', NULL, '30/04/1978 00:00:00', 'a10874597@capeco.edu.pe', 0, 'Virtual-01', 2),
(188, '42499385', 'SOSA', 'GUTIERREZ', 'ELIO', 'MASCULINO', 'eliososa22@hotmail.com', '966995802', NULL, '23/07/1984 00:00:00', 'a42499385@capeco.edu.pe', 0, 'Virtual-01', 2),
(189, '25786403', 'SURICHAQUI', 'CONDOR', 'NICANOR AGUSTIN', 'MASCULINO', 'nicanoragustinsc@gmail.com', '990451112', NULL, '10/01/1975 00:00:00', 'a25786403@capeco.edu.pe', 0, 'Virtual-01', 2),
(190, '46914959', 'TARAZONA', 'HILARIO', 'ETNAN RUFINI', 'MASCULINO', 'etnantarazona@gmail.com', '994845215', NULL, '11/02/1991 00:00:00', 'a46914959@capeco.edu.pe', 0, 'Virtual-01', 2),
(191, '06543344', 'TEJEDA', 'CONTRERAS', 'CARLOS ALBERTO', 'MASCULINO', 'calito_t@hotnail.com', '938141074', NULL, '10/07/1965 00:00:00', 'a06543344@capeco.edu.pe', 0, 'Virtual-01', 2),
(192, '08178366', 'TIPULA', 'MIRAMIRA', 'JOSE', 'MASCULINO', 'yaninatipula@gmail.com', '987068336', NULL, '18/09/1965 00:00:00', 'a08178366@capeco.edu.pe', 0, 'Virtual-01', 2),
(193, '46201325', 'TIPULA', 'RODRIGUEZ', 'EDUARDO RUBEN', 'MASCULINO', 'etipularodriguez@gmail.com', '963448454', NULL, '12/03/1986 00:00:00', 'a46201325@capeco.edu.pe', 0, 'Virtual-01', 2),
(194, '33418849', 'TRAUCO', 'MUÑOZ', 'LUIS FRANCISCO', 'MASCULINO', 'l_trauco_25@hotmail.com', '965844925', NULL, '25/07/1965 00:00:00', 'a33418849@capeco.edu.pe', 0, 'Virtual-01', 2),
(195, '25632044', 'TRONCOSO', 'ESPINOZA', 'HUGO ABEL', 'MASCULINO', 'hugotroncoso@outlook.com', '978429694', NULL, '22/02/1961 00:00:00', 'a25632044@capeco.edu.pe', 0, 'Virtual-01', 2),
(196, '09053925', 'TRUJILLO', 'VARA', 'NICETO', 'MASCULINO', 'trujillovaraniceto@gmail.com', '963715190', NULL, '8/12/1968 00:00:00', 'a09053925@capeco.edu.pe', 0, 'Virtual-01', 2),
(197, '10409510', 'TUESTA', 'NOVOA', 'BERNABE', 'MASCULINO', 'bernabe1tuesta@gmail.com', '963814762', NULL, '1/06/1977 00:00:00', 'a10409510@capeco.edu.pe', 0, 'Virtual-01', 2),
(198, '47402642', 'VALDEZ', 'RAMIREZ', 'JUAN CARLOS', 'MASCULINO', 'edificacionesjc16@hotmail.com', '983587621', NULL, '16/03/1992 00:00:00', 'a47402642@capeco.edu.pe', 0, 'Virtual-01', 2),
(199, '46102710', 'VILLARROEL', 'PALMA', 'LUIS ENRRIQUE', 'MASCULINO', 'luis_548@hotmail.com', '912887012', NULL, '3/04/1988 00:00:00', 'a46102710@capeco.edu.pe', 0, 'Virtual-01', 2),
(200, '41433231', 'VIVANCO', 'VILCA', 'FREDDY RUBEN', 'MASCULINO', 'fredyvivanco11@gmail.com', '974921558', NULL, '23/07/1982 00:00:00', 'a41433231@capeco.edu.pe', 0, 'Virtual-01', 2),
(201, '12345678', 'Prueba', 'Prueba', 'Prueba', 'MASCULINO', 'prueba@servidor.com', '987654321', NULL, '99/99/9999 00:00:00', '123456', NULL, 'prueba', 1);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `ci_sessions`
--

CREATE TABLE `ci_sessions` (
  `session_id` varchar(40) NOT NULL DEFAULT '0',
  `ip_address` varchar(45) NOT NULL DEFAULT '0',
  `user_agent` varchar(120) NOT NULL,
  `last_activity` int(10) UNSIGNED NOT NULL DEFAULT '0',
  `user_data` text NOT NULL
) ENGINE=MyISAM AVG_ROW_LENGTH=248 DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `ci_sessions`
--

--
-- Estructura de tabla para la tabla `cursos`
--

CREATE TABLE `cursos` (
  `id` int(11) NOT NULL,
  `nombre` varchar(500) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `membership`
--

CREATE TABLE `membership` (
  `id` int(11) NOT NULL,
  `user_name` varchar(255) DEFAULT NULL,
  `pass_word` varchar(255) DEFAULT NULL,
  `pass_word_visible` varchar(255) DEFAULT NULL,
  `nombre` varchar(255) DEFAULT NULL,
  `apellidos` varchar(255) DEFAULT NULL,
  `email` varchar(255) DEFAULT NULL,
  `dni` varchar(255) DEFAULT NULL,
  `celular` varchar(255) DEFAULT NULL,
  `telefono` varchar(255) DEFAULT NULL,
  `ciudad` varchar(500) DEFAULT NULL,
  `perfil` enum('Administrador','Usuario') NOT NULL DEFAULT 'Usuario',
  `fecha` datetime DEFAULT NULL
) ENGINE=InnoDB AVG_ROW_LENGTH=963 DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `membership`
--

INSERT INTO `membership` (`id`, `user_name`, `pass_word`, `pass_word_visible`, `nombre`, `apellidos`, `email`, `dni`, `celular`, `telefono`, `ciudad`, `perfil`, `fecha`) VALUES
(1, 'admin', '60a4a80e285733ca3aabed0d61d2a81a', NULL, 'Jose', 'Chipulina', 'josemanuel2@interactive.pe', NULL, NULL, NULL, 'AREQUIPA', 'Administrador', NULL);


--
-- Estructura de tabla para la tabla `postula`
--

CREATE TABLE `postula` (
  `id` int(11) NOT NULL,
  `nombres` varchar(500) NOT NULL,
  `apellidos` varchar(500) NOT NULL,
  `dni` varchar(500) NOT NULL,
  `email` varchar(500) NOT NULL,
  `celular` varchar(500) NOT NULL,
  `telefono` varchar(500) NOT NULL,
  `experiencia` varchar(500) NOT NULL,
  `ocupacion` varchar(500) NOT NULL,
  `ocupacion_otros` varchar(500) NOT NULL,
  `fecha` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `postula`
--

INSERT INTO `postula` (`id`, `nombres`, `apellidos`, `dni`, `email`, `celular`, `telefono`, `experiencia`, `ocupacion`, `ocupacion_otros`, `fecha`) VALUES
(2, 'Ricardo ', 'Herrera', '41750411', 'rherrera@richmail.ga', '987825254', '3124521', 'Menos de 5 años', 'Albañil/constructor', '', '2021-01-07 03:04:44'),
(3, 'Renato', 'Thayz Peralta', '42237028', 'thayzrenato@gmail.com', '943008868', '943008868', 'Más de 20 años', 'Maestro/constructor', '', '2021-01-22 15:02:00');

-- --------------------------------------------------------

--
-- Indices de la tabla `alumnos`
--
ALTER TABLE `alumnos`
  ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `ci_sessions`
--
ALTER TABLE `ci_sessions`
  ADD PRIMARY KEY (`session_id`),
  ADD KEY `last_activity_idx` (`last_activity`);

--
-- Indices de la tabla `cursos`
--
ALTER TABLE `cursos`
  ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `membership`
--
ALTER TABLE `membership`
  ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `postula`
--
ALTER TABLE `postula`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT de las tablas volcadas
--

--
-- AUTO_INCREMENT de la tabla `alumnos`
--
ALTER TABLE `alumnos`
  MODIFY `id` int(11) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=202;

--
-- AUTO_INCREMENT de la tabla `cursos`
--
ALTER TABLE `cursos`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT de la tabla `membership`
--
ALTER TABLE `membership`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=1658;

--
-- AUTO_INCREMENT de la tabla `postula`
--
ALTER TABLE `postula`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=669;



/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
