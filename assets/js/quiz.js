//href="<?php echo base_url()."capacitaciones/".$slug."/finalizar"; ?>"

$(function() {
  	function onPressValidar(){
	  	var correctas = 0 ;
	  	var preguntas = new Array();
	  	var pcorrectas = new Array();
	  	var respuestas = new Array();
		for(var i=0;i<num_preguntas;i++){
			var pregunta = $("input[name='pregunta_"+i+"']:checked"). val();
			for(var p=0;p<4;p++){
				$("#p_"+i+"_"+p).removeClass("incorrect");
				$("#p_"+i+"_"+p).removeClass("correct");
			}
			if(pregunta==undefined || pregunta==""){
				//Swal.fire({ title: 'Importante', html:"Por favor marque la pregunta "+i, showConfirmButton: true }); return;
				alert("Por favor marque la pregunta "+(i+1));
				return;
			}
			pcorrectas.push(quiz[i].c);
			respuestas.push(pregunta); 
			preguntas.push(pregunta);
		}
		for(var i=0;i<pcorrectas.length;i++){
			var element = "#p_"+i+"_"+pcorrectas[i];
			$("#p_"+i+"_"+pcorrectas[i]).addClass("correct");
			if(respuestas[i]!=pcorrectas[i]){
				$("#p_"+i+"_"+respuestas[i]).addClass("incorrect");
			}
		}
		$("#btn_continuar").show();
		$("#btn_validar").hide();
	}
	$("#btn_validar").click(()=>{onPressValidar();});
	
	$("#btn_continuar").click(()=>{document.location.href=url_finalizar;});
});
