Sugar.extend();

const parametros = [
{
  // Cimientos
  // opciones: false,
  imagen: `${base_url}assets/img/1_CIMIENTOS.png`,
  agregadoGruesoTamagno: "2",
  agregadoGruesoPeso: 698,
  agregadoGruesoValor: 1750,
  agregadoFinoPeso: 729,
  agregadoFinoValor: 1650,
  piedraGrandeTamagno: 10,
  piedraGrandePeso: 630,
  piedraGrandeValor: 1750,
  resistencia: 100,
  aguaPeso: 116.2,
  cementoPeso: 173.6,
  cementoValor: 42.5,
  opciones: [] },

{
  // Sobrecimientos
  // opciones: false,
  imagen: `${base_url}assets/img/2_SOBRECIMIENTOS.png`,
  agregadoGruesoTamagno: "2",
  agregadoGruesoPeso: 752,
  agregadoGruesoValor: 1750,
  agregadoFinoPeso: 786,
  agregadoFinoValor: 1650,
  piedraGrandeTamagno: 4,
  piedraGrandePeso: 525,
  piedraGrandeValor: 1750,
  resistencia: 140,
  aguaPeso: 124.2,
  cementoPeso: 175.5,
  cementoValor: 42.5,
  opciones: [] },

{
  // Zapatas para columnas
  // opciones: false,
  imagen: `${base_url}assets/img/3_ZAPATAS_PARA_COLUMNAS.png`,
  agregadoGruesoTamagno: "3/4",
  agregadoGruesoPeso: [983, 954],
  agregadoGruesoValor: 1750,
  agregadoFinoPeso: [10288, 997],
  agregadoFinoValor: 1650,
  piedraGrandeTamagno: 4,
  piedraGrandePeso: 525,
  piedraGrandeValor: 1750,
  resistencia: [210, 280],
  aguaPeso: 124.2,
  cementoPeso: [263, 332],
  cementoValor: 42.5,
  opciones: ["210 kg/cm<sup>2</sup>", "280 kg/cm<sup>2</sup>"] },

{
  // Zapatas para muros
  // opciones: false,
  imagen: `${base_url}assets/img/4_ZAPATAS_PARA_MUROS.png`,
  agregadoGruesoTamagno: "3/4",
  agregadoGruesoPeso: [983, 954],
  agregadoGruesoValor: 1750,
  agregadoFinoPeso: [10288, 997],
  agregadoFinoValor: 1650,
  piedraGrandeTamagno: 4,
  piedraGrandePeso: 525,
  piedraGrandeValor: 1750,
  resistencia: [210, 280],
  aguaPeso: 124.2,
  cementoPeso: [263, 332],
  cementoValor: 42.5,
  opciones: ["210 kg/cm<sup>2</sup>", "280 kg/cm<sup>2</sup>"] },

/*
{
  // Muros de contención
  // opciones: false,
  imagen: `${base_url}assets/img/5_MUROS.png`,
  agregadoGruesoTamagno: "3/4",
  agregadoGruesoPeso: [983, 954],
  agregadoGruesoValor: 1750,
  agregadoFinoPeso: [10288, 997],
  agregadoFinoValor: 1650,
  piedraGrandeTamagno: 4,
  piedraGrandePeso: 525,
  piedraGrandeValor: 1750,
  resistencia: [210, 280],
  aguaPeso: 124.2,
  cementoPeso: [263, 332],
  cementoValor: 42.5,
  opciones: ["210 kg/cm<sup>2</sup>", "280 kg/cm<sup>2</sup>"] },
*/

{

  // Muros
  // opciones: false,
	imagen: `${base_url}assets/img/5_MUROS.png`,
	opciones: []
},
{
  // Columnas
  // opciones: false,
  imagen: `${base_url}assets/img/6_COLUMNAS.png`,
  agregadoGruesoTamagno: "1/2",
  agregadoGruesoPeso: [1003, 988, 956],
  agregadoGruesoValor: 1750,
  agregadoFinoPeso: [967, 954, 922],
  agregadoFinoValor: 1650,
  piedraGrandeTamagno: 4,
  piedraGrandePeso: 525,
  piedraGrandeValor: 1750,
  resistencia: [175, 210, 285],
  aguaPeso: 124.2,
  cementoPeso: [252, 284, 358],
  cementoValor: 42.5,
  opciones: ["175 kg/cm<sup>2</sup>", "210 kg/cm<sup>2</sup>", "285 kg/cm<sup>2</sup>"] },

{
  // Vigas
  // opciones: false,
  imagen: `${base_url}assets/img/7_VIGAS.png`,
  agregadoGruesoTamagno: "1/2",
  agregadoGruesoPeso: [1003, 988, 956],
  agregadoGruesoValor: 1750,
  agregadoFinoPeso: [967, 954, 922],
  agregadoFinoValor: 1650,
  piedraGrandeTamagno: 4,
  piedraGrandePeso: 525,
  piedraGrandeValor: 1750,
  resistencia: [175, 210, 285],
  aguaPeso: 124.2,
  cementoPeso: [252, 284, 358],
  cementoValor: 42.5,
  opciones: ["175 kg/cm<sup>2</sup>", "210 kg/cm<sup>2</sup>", "285 kg/cm<sup>2</sup>"] },

{
  // Placas
  // opciones: false,
  imagen: `${base_url}assets/img/8_PLACAS.png`,
  agregadoGruesoTamagno: "1/2",
  agregadoGruesoPeso: [1003, 988, 956],
  agregadoGruesoValor: 1750,
  agregadoFinoPeso: [967, 954, 922],
  agregadoFinoValor: 1650,
  piedraGrandeTamagno: 4,
  piedraGrandePeso: 525,
  piedraGrandeValor: 1750,
  resistencia: [175, 210, 285],
  aguaPeso: 124.2,
  cementoPeso: [252, 284, 358],
  cementoValor: 42.5,
  opciones: ["175 kg/cm<sup>2</sup>", "210 kg/cm<sup>2</sup>", "285 kg/cm<sup>2</sup>"] },
  
];





const calculadora = {};

calculadora.item = trkl(-1);

calculadora.agregadoGruesoTamagno = trkl("1");
calculadora.agregadoGruesoPeso = trkl(1);
calculadora.agregadoGruesoValor = trkl(1);

calculadora.agregadoFinoPeso = trkl(1);
calculadora.agregadoFinoValor = trkl(1);

calculadora.piedraGrandeTamagno = trkl(1);
calculadora.piedraGrandePeso = trkl(1);
calculadora.piedraGrandeValor = trkl(1);

calculadora.resistencia = trkl(1);
calculadora.opciones = trkl([]);
calculadora.opcion = trkl(0);

calculadora.aguaPeso = trkl(1);

calculadora.cementoPeso = trkl(1);
calculadora.cementoValor = trkl(1);

calculadora.factor = trkl(1.1);

calculadora.largo = trkl(1);
calculadora.ancho = trkl(1);
calculadora.alto = trkl(1);

calculadora.volumen = trkl.computed(() =>
calculadora.largo() * calculadora.ancho() * calculadora.alto());

calculadora.aguaPorBolsa = trkl.computed(() =>
calculadora.aguaPeso() / (
calculadora.cementoPeso() * calculadora.cementoValor()));


calculadora.agregadoGruesoVolumen = trkl.computed(() =>
calculadora.agregadoGruesoPeso() / calculadora.agregadoGruesoValor() *
calculadora.factor() * calculadora.volumen());

calculadora.agregadoFinoVolumen = trkl.computed(() =>
calculadora.agregadoFinoPeso() / calculadora.agregadoFinoValor() *
calculadora.factor() * calculadora.volumen());

calculadora.piedraGrandeVolumen = trkl.computed(() =>
calculadora.piedraGrandePeso() / calculadora.piedraGrandeValor() *
calculadora.factor() * calculadora.volumen());

calculadora.aguaVolumen = trkl.computed(() =>
calculadora.aguaPeso() * calculadora.factor() * calculadora.volumen());

calculadora.cementoVolumen = trkl.computed(() =>
calculadora.cementoPeso() / calculadora.cementoValor() *
calculadora.factor() * calculadora.volumen());


function inputOnChange(input) {
  calculadora[input.name](parseInt(input.value));
}

const $salida = document.querySelector(".calculadora");
const $entrada = document.querySelector(".calculadora__entrada");
const $calculadora = document.querySelector(".calculadora");
// console.debug($salida)


calculadora.agregadoGruesoTamagno.subscribe(n => $entrada.querySelector("#agregadoGruesoTamagno").value = n);
calculadora.agregadoGruesoTamagno.subscribe(n => $salida.querySelector(".agregadoGruesoTamagno").textContent = n);

calculadora.largo.subscribe(n => $entrada.querySelector("#largo").value = n);
calculadora.ancho.subscribe(n => $entrada.querySelector("#ancho").value = n);
calculadora.alto.subscribe(n => $entrada.querySelector("#alto").value = n);

calculadora.resistencia.subscribe(n => $entrada.querySelector("#resistencia").value = n);


calculadora.agregadoGruesoPeso.subscribe(n => $salida.querySelector(".agregadoGruesoPeso").textContent = n);
calculadora.agregadoFinoPeso.subscribe(n => $salida.querySelector(".agregadoFinoPeso").textContent = n);
calculadora.cementoPeso.subscribe(n => $salida.querySelector(".cementoPeso").textContent = n);
calculadora.volumen.subscribe(n => $salida.querySelector(".volumen").textContent = n);

calculadora.volumen.subscribe(n => $salida.querySelector(".volumen").textContent = n);

calculadora.aguaVolumen.subscribe(n => $salida.querySelector(".aguaVolumen").textContent = n.round(2));
calculadora.agregadoGruesoVolumen.subscribe(n => $salida.querySelector(".agregadoGruesoVolumen").textContent = n.round(2));
calculadora.agregadoFinoVolumen.subscribe(n => $salida.querySelector(".agregadoFinoVolumen").textContent = n.round(2));
// calculadora.piedraGrandeVolumen.subscribe(n => $salida.querySelector(".piedraGrandeVolumen").textContent = n)
// calculadora.aguaVolumen.subscribe(n => $salida.querySelector(".aguaVolumen").textContent = n)
calculadora.cementoVolumen.subscribe(n => $salida.querySelector(".cementoVolumen").textContent = n.round(2));

const $imagen = document.getElementById("imagen");
calculadora.item.subscribe(n => {

  // console.log(n)
  // console.log(parametros[n])
  const p = parametros[n];
  $imagen.setAttribute("src", p.imagen);
  
  if( n == 4 ){
	
	calculadoraMuro.forma(0);
    $calculadora.classList.add("calculadora--muro");
  }else{

	$calculadora.classList.remove("calculadora--muro");
	
	  calculadora.opcion(0);
	  calculadora.opciones(p.opciones);

	  calculadora.agregadoGruesoTamagno(p.agregadoGruesoTamagno);
	  if (Object.isArray(p.agregadoGruesoPeso)) {
		calculadora.agregadoGruesoPeso(parseInt(p.agregadoGruesoPeso[0]));
	  } else {
		calculadora.agregadoGruesoPeso(p.agregadoGruesoPeso);
	  }
	  calculadora.agregadoGruesoValor(p.agregadoGruesoValor);


	  if (Object.isArray(p.agregadoFinoPeso)) {
		calculadora.agregadoFinoPeso(parseInt(p.agregadoFinoPeso[0]));
	  } else {
		calculadora.agregadoFinoPeso(p.agregadoFinoPeso);
	  }
	  calculadora.agregadoFinoValor(p.agregadoFinoValor);

	  calculadora.piedraGrandeTamagno(p.piedraGrandeTamagno);
	  calculadora.piedraGrandePeso(p.piedraGrandePeso);
	  calculadora.piedraGrandeValor(p.piedraGrandeValor);

	  calculadora.aguaPeso(p.aguaPeso);

	  if (Object.isArray(p.cementoPeso)) {
		calculadora.cementoPeso(parseInt(p.cementoPeso[0]));
	  } else {
		calculadora.cementoPeso(p.cementoPeso);
	  }
	  calculadora.cementoValor(p.cementoValor);

	  if (Object.isArray(p.resistencia)) {
		calculadora.resistencia(parseInt(p.resistencia[0]));

	  } else {
		calculadora.resistencia(p.resistencia);
	  }
  }

});

const $opciones = document.getElementById("opciones");
calculadora.opciones.subscribe(v => {
  if (v.isEmpty()) {
    // console.log("oculta")
    $opciones.classList.add("d-none");
  } else {
    // console.log("muestra")
    $opciones.classList.remove("d-none");
    const $lista = $opciones.querySelector(".opciones");
    $lista.innerHTML = "";
    v.forEach((item, i) => {
      const opcion = document.createElement("div");
      opcion.classList.add("form-check");

      const input = document.createElement("input");
      input.classList.add("form-check-input");
      input.setAttribute("type", "radio");
      input.setAttribute("id", `opcion${i}`);
      input.setAttribute("name", "opcion");
      input.setAttribute("value", i);
      if (!i) {
        input.setAttribute("checked", "true");
      }
      input.onchange = () => inputOnChange(input);

      opcion.appendChild(input);

      const label = document.createElement("label");
      label.classList.add("form-check-label");
      label.setAttribute("for", `opcion${i}`);
      label.innerHTML = item;
      opcion.appendChild(label);

      $lista.appendChild(opcion);
    });
  }

});

calculadora.opcion.subscribe(v => {
  // console.log(v)
  const p = parametros[calculadora.item()];
  if (Object.isArray(p.agregadoGruesoPeso)) {
    calculadora.agregadoGruesoPeso(parseInt(p.agregadoGruesoPeso[v]));
  }

  if (Object.isArray(p.agregadoFinoPeso)) {
    calculadora.agregadoFinoPeso(parseInt(p.agregadoFinoPeso[v]));
  }

  if (Object.isArray(p.cementoPeso)) {
    calculadora.cementoPeso(parseInt(p.cementoPeso[v]));
  }

  if (Object.isArray(p.resistencia)) {
    calculadora.resistencia(parseInt(p.resistencia[v]));

  }
});

const $selector = document.getElementById("selector");
$selector.addEventListener(
"change",
evt => {
  const valor = $selector.value;
  calculadora.item(parseInt($selector.value));
});

// console.log("item")

calculadora.factor(1.1);

calculadora.largo(10);
calculadora.ancho(0.4);
calculadora.alto(3);

// calculadora.agregadoGruesoTamagno("2")
// calculadora.agregadoGruesoPeso(698)
// calculadora.agregadoGruesoValor(1750)

// calculadora.agregadoFinoPeso(729)
// calculadora.agregadoFinoValor(1650)

// calculadora.piedraGrandeTamagno(10)
// calculadora.piedraGrandePeso(630)
// calculadora.piedraGrandeValor(1750)

// calculadora.resistencia(100)

// calculadora.aguaPeso(116.2)

// calculadora.cementoPeso(173.6)
// calculadora.cementoValor(42.5)



const parametrosMuros = {
  ladrillos: [66, 39, 29],
  bolsas: [2, 1, 0.5] };


const calculadoraMuro = {};
calculadoraMuro.forma = trkl(-1);
calculadoraMuro.largo = trkl(5);
calculadoraMuro.altura = trkl(2.8);
calculadoraMuro.factor = trkl(1.1);

calculadoraMuro.area = trkl.computed(() =>
calculadoraMuro.largo() * calculadoraMuro.altura());


calculadoraMuro.ladrillo = trkl.computed(() =>
parametrosMuros.ladrillos[calculadoraMuro.forma()] * calculadoraMuro.area() * calculadoraMuro.factor());


calculadoraMuro.bolsas = trkl.computed(() =>
parametrosMuros.bolsas[calculadoraMuro.forma()] * calculadoraMuro.area() * calculadoraMuro.factor());


calculadoraMuro.arena = trkl.computed(() =>
calculadoraMuro.bolsas() * 5);


calculadoraMuro.ladrillo.subscribe(valor => {
  $salida.querySelector(".muro-ladrillos").textContent = valor.round(2);
});

calculadoraMuro.bolsas.subscribe(valor => {
  $salida.querySelector(".muro-cemento").textContent = valor.round(2);
});

calculadoraMuro.arena.subscribe(valor => {
  $salida.querySelector(".muro-arena").textContent = valor.round(2);
});

function radioMuroOnChange(input) {
  calculadoraMuro.forma(parseInt(input.value));
}

function inputMuroChange(input) {
  calculadoraMuro[input.name](parseInt(input.value));
}

calculadoraMuro.forma.subscribe(valor => {
  $salida.querySelector(`#forma${valor}`).setAttribute("checked", true);
});

calculadoraMuro.forma(0);

calculadora.item(id);