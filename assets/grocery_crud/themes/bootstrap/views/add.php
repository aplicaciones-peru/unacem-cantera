<?php
    $this->set_css($this->default_theme_path.'/bootstrap/css/bootstrap/bootstrap.min.css');
    $this->set_css($this->default_theme_path.'/bootstrap/css/font-awesome/css/font-awesome.min.css');
    $this->set_css($this->default_theme_path.'/bootstrap/css/common.css');
    $this->set_css($this->default_theme_path.'/bootstrap/css/general.css');
    $this->set_css($this->default_theme_path.'/bootstrap/css/add-edit-form.css');
    
    $ci =& get_instance(); if($ci->uri->segment(2)=="socios"){ 
	$this->set_css($this->default_theme_path.'/bootstrap/css/style-form.css');}

    if ($this->config->environment == 'production') {
        $this->set_js_lib($this->default_javascript_path.'/'.grocery_CRUD::JQUERY);
        $this->set_js_lib($this->default_theme_path.'/bootstrap/build/js/global-libs.min.js');
        $this->set_js_config($this->default_theme_path.'/bootstrap/js/form/add.js');
    } else {
        $this->set_js_lib($this->default_javascript_path.'/'.grocery_CRUD::JQUERY);
        $this->set_js_lib($this->default_theme_path.'/bootstrap/js/jquery-plugins/jquery.form.min.js');
        $this->set_js_lib($this->default_theme_path.'/bootstrap/js/common/common.min.js');
        $this->set_js_config($this->default_theme_path.'/bootstrap/js/form/add.js');
    }

	$this->set_js_lib($this->default_javascript_path.'/jquery_plugins/jquery.noty.js');
	$this->set_js_config($this->default_javascript_path.'/jquery_plugins/config/jquery.noty.config.js');
?>
<style>
.chosen-choices li{
	display: block;
	float: none !important;
		
}	
</style>

<script src='/assets/js/jquery-sortable.js'></script> 
<div class="crud-form" data-unique-hash="<?php echo $unique_hash; ?>">
    <div class="container gc-container">
        <div class="row">
            <div class="col-md-12">
                <div class="table-label">
                    <div class="floatL l5">
                        <?php echo $this->l('form_add'); ?> <?php echo $subject?>
                    </div>
                    <div class="floatR r5 minimize-maximize-container minimize-maximize">
                        <i class="fa fa-caret-up"></i>
                    </div>
                    <div class="floatR r5 gc-full-width">
                        <i class="fa fa-expand"></i>
                    </div>
                    <div class="clear"></div>
                </div>
                <div class="form-container table-container">
                        <?php echo form_open( $insert_url, 'method="post" id="crudForm"  enctype="multipart/form-data" class="form-horizontal"'); ?>
						
						
						<?php $i=0; $mid = intval(count($fields)/2)+1;  ?>
						
						
						<div class='lcolumn'>
						
						
						
						
						
                            <?php foreach($fields as $field) { $i++; ?>
                                <div class="form-group <?php echo $field->field_name; ?>_form_group">
                                    <label class="col-sm-3 control-label">
                                        <?php echo $input_fields[$field->field_name]->display_as; ?><?php echo ($input_fields[$field->field_name]->required) ? "<span class='required'>*</span> " : ""; ?>
                                    </label>
                                    <div class="col-sm-9">
	                                    
                                        <?php  echo $input_fields[$field->field_name]->input;  ?>

                                    </div>
                                </div>
                                
                                <?php  if($i==$mid){  ?>
									</div>	
									<div class='rcolumn'>	
									<?php  }  ?>
									
									 <?php  if($i==count($fields)){  ?>
									</div>	
									<?php  }  ?>
							
                            <?php }?>
                            
                            
                            
                            
                            
                            <!-- Start of hidden inputs -->
                            <?php
                            foreach ($hidden_fields as $hidden_field) {
                                echo $hidden_field->input;
                            }
                            ?>
                            <!-- End of hidden inputs -->
                            <?php if ($is_ajax) { ?><input type="hidden" name="is_ajax" value="true" /><?php }?>

							<div class="form_bottom">
								
                            <div class='small-loading hidden' id='FormLoading'><?php echo $this->l('form_insert_loading'); ?></div>

                            <div class="form-group">
                                <div id='report-error' class='report-div error bg-danger' style="display:none"></div>
                                <div id='report-success' class='report-div success bg-success' style="display:none"></div>
                            </div>
                            <div class="form-group">
                                <div class="col-sm-offset-3 col-sm-7">
                                    <button class="btn btn-default btn-success b10" type="submit" id="form-button-save">
                                        <i class="fa fa-check"></i>
                                        <?php echo $this->l('form_save'); ?>
                                    </button>
                                    <?php 	if(!$this->unset_back_to_list) { ?>
                                        <button class="btn btn-info b10" type="button" id="save-and-go-back-button">
                                            <i class="fa fa-rotate-left"></i>
                                            <?php echo $this->l('form_save_and_go_back'); ?>
                                        </button>
                                        <button class="btn btn-default cancel-button b10" type="button" id="cancel-button">
                                            <i class="fa fa-warning"></i>
                                            <?php echo $this->l('form_cancel'); ?>
                                        </button>
                                    <?php } ?>
                                </div>
                            </div>
                            
                            </div>
                            
                        <?php echo form_close(); ?>
                </div>
            </div>
        </div>
    </div>
</div>
<script>
	var validation_url = '<?php echo $validation_url?>';
	var list_url = '<?php echo $list_url?>';

	var message_alert_add_form = "<?php echo $this->l('alert_add_form')?>";
	var message_insert_error = "<?php echo $this->l('insert_error')?>";
	
	
	function onchange_tipocanje(evt, params){
		var tipo_canje = $("#field-tipo_canje option:selected").text();
		if(tipo_canje=="Temporal"){
			$(".tiempo_canje_form_group").show();
		}else{
			$(".tiempo_canje_form_group").hide();
		}
		
	}
	function onchange_n_n_chosen(evt, params){
															//alert("EDIT !");
															if(params.selected){
																var t = $(evt.currentTarget).find('[value='+params.selected+']');
																var nItem = $( "<li>" );
																nItem.attr( "data-id", t.val() );
																nItem.attr( "data-name", t.text() );
																nItem.text( t.text() );
																$('ol.example').append( nItem );
																
															}else if(params.deselected){
																$('ol.example').find( 'li[data-id="' + params.deselected + '"]' ).remove();
															}
															var data = $("ol.example").sortable("serialize").get()[0];
															var ndata = new Array();
															for(var i=0;i<data.length;i++){
																ndata.push(data[i].id);
															}
															$('#ordered_list').val(ndata);
	}
	
	

	<?php if(isset($_GET["dni"])){ ?>$(function  () {  $("#field-dni").val("<?php echo $_GET["dni"]; ?>");});<?php } ?>  
	function changeZona(){
		var zona = $("#field-zona option:selected").text();
			$('#field-ciudad').empty();
			$.post( "/admin/ciudadesbyzona",{zona:zona},function( ciudades ) {
			  for(var k=0;k<ciudades.length;k++){
			  	$('#field-ciudad').append('<option value="'+ciudades[k].value+'">'+ciudades[k].label+'</option>');
		   	  }
		   	  $("#field-ciudad").trigger("chosen:updated");
		   	  changeCiudad();
			},'json');
	}
	function changeCiudad(){
		var ciudad = $("#field-ciudad option:selected").text();
			$('#field-lubricentro').empty();
			$.post( "/admin/lubricentrobyciudad",{ciudad:ciudad},function( lubricentros ) {
			  for(var k=0;k<lubricentros.length;k++){
			  	$('#field-lubricentro').append('<option value="'+lubricentros[k].value+'">'+lubricentros[k].label+'</option>');
		   	  }
		   	  $("#field-lubricentro").trigger("chosen:updated");
			},'json');
	}
	$(function  () { 
		if ($("#field-zona").length > 0){
			$('#field-zona').chosen().change(changeZona);
	    	$('#field-ciudad').chosen().change(changeCiudad);
	    	$('#field-ciudad').empty();
			$('#field-lubricentro').empty();
			$("#field-ciudad").trigger("chosen:updated");
			$("#field-lubricentro").trigger("chosen:updated");
			changeZona();
		}
		/*
		if ($("#field-tipo_canje").length > 0){
			onchange_tipocanje();
			$('#field-tipo_canje').chosen().change(onchange_tipocanje);
		}*/
		
		
		if($("[name='dni']").length > 0){
			$("[name='dni']").attr('maxlength','8');
		}
		<?php if($ci->uri->segment(2)=="reconocimiento_lubricentro"){ ?>
			$('#field-trimestre').empty();
			$("#field-trimestre").trigger("chosen:updated");
			$('#field-id_lubricentro').chosen().change(function(){
				var lub = $("#field-id_lubricentro option:selected").val();
				$('#field-trimestre').empty();
				$.post( "/api/trimestrebylubricentro",{id:lub},function( trimestres ) {
					for(var k=0;k<trimestres.length;k++){
						$('#field-trimestre').append('<option value="'+trimestres[k].value+'">'+trimestres[k].label+'</option>');
					}
					$("#field-trimestre").trigger("chosen:updated");
				},'json');
			});
		<?php } ?>
	});
	
	
</script>
