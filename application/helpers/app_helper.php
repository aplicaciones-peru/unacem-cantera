<?php
if (!defined('BASEPATH')) exit('No direct script access allowed');

if (!function_exists('clases')) {
	function clases($criterios = [])
	{
		return implode(" ", array_keys(array_filter($criterios)));
	}
}

if (!function_exists('plantilla')) {
	function plantilla($plantilla, $contenidos)
	{
		$claves = array_map(
			function ($clave) {
				return sprintf("{{%s}}", $clave);
			},
			array_keys($contenidos)
		);

		$resultado = str_replace($claves, array_values($contenidos), $plantilla);
		$resultado = preg_replace('/{{[\w\-_]+}}+\s?/', '', $resultado);

		return $resultado;
	}
}


if (!function_exists('arreglo_buscar')) {
	function arreglo_buscar($arreglo, $columna, $valor)
	{
		$i = array_search($valor, array_column($arreglo, $columna));
		$resultado = [];

		if ($i >= 0) {
			$resultado = $arreglo[$i];
		}

		return $resultado;
	}
}



if (!function_exists('fragmento')) {
	function fragmento($ruta, $unico = true)
	{
		if (is_array($ruta)) {
			$ruta = implode(DIRECTORY_SEPARATOR, $ruta);
		}
		if (!terminaCon($ruta, ".php", true)) {
			$ruta .= ".php";
		}

		$ruta = APPPATH . "views" . DIRECTORY_SEPARATOR . $ruta;

		if (file_exists($ruta)) {
			if ($unico) {
				include_once($ruta);
			} else {
				include($ruta);
			}
		} else {
			echo "No existe $ruta";
		}
	}
}



if (!function_exists('empiezaCon')) {
	function empiezaCon($cadena, $subcadena, $mayusculas = false)
	{
		return substr_compare($cadena, $subcadena, 0, strlen($subcadena), $mayusculas) === 0;
	}
}

if (!function_exists('terminaCon')) {
	function terminaCon($cadena, $subcadena, $mayusculas = false)
	{
		$l = strlen($subcadena);
		return substr_compare($cadena, $subcadena, -$l, $l, $mayusculas) === 0;
	}
}

if (!function_exists('base64url_encode')) {
	function base64url_encode($data) {
		return rtrim(strtr(base64_encode($data), '+/', '-_'), '=');
	}
}


if (!function_exists('base64url_decode')) {
	function base64url_decode($data) {
		return base64_decode(str_pad(strtr($data, '-_', '+/'), strlen($data) % 4, '=', STR_PAD_RIGHT));
	}
}
