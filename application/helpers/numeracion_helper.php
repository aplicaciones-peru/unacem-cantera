<?php

if (!defined('BASEPATH'))
	exit('No direct script access allowed');

class Base36{
	
	const D = ["0", "1", "2", "3", "4", "5", "6", "7", "8", "9", "A", "B", "C", "D", "E", "F", "G", "H", "I", "J", "K", "L", "M", "N", "O", "P", "Q", "R", "S", "T", "U", "V", "W", "X", "Y", "Z"];
	
	public static function desdeBase10( $valor ) {
		
		if ($valor < 36) {
			return self::D[$valor];
		} else {
			return self::desdeBase10(floor($valor / 36)) . self::D[$valor % 36];
		}
	}
	
	public static function aBase10( $valor ) {
		$digitos = array_reverse(str_split( $valor));
		
		$r = 0;
		
		for ( $i = count( $digitos ) - 1; $i>=0; $i-- ){
			$r += array_search($digitos[$i], self::D) * 36**$i;
		}
		
		return $r;
	}
}
