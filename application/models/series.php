<?php

class Series extends CI_Model {

	public function __construct()
	{
		$this->load->database();
	}
	
	function agregar( $producto, $cantidad ) {
		$query = $this->db->select("inicio + cantidad as siguiente")
			->limit(1)
			->order_by( "inicio DESC" )
			->where( "YEAR(fecha)", date("Y"))
			->get("serie");
		//return $query;
		$r = $query->result();
		$siguiente = $r?$r[0]->siguiente:1;
		
		$this->db->insert("serie", [
			"inicio" => $siguiente,
			"cantidad" => $cantidad,
			"producto" => $producto
		]);
		
		return $this->db->insert_id();
	}
	
	function obtener( $id ){
		$query = $this->db
			->where( "id", $id)
			->get("serie");
		
		$r = $query->result();
		return $r?$r[0]:null;
	}
	
	function asignarArchivo( $id, $archivo ) {
		$this->db->where( "id", $id )
			->update( "serie", [
				"archivo" => $archivo
			]);
	}
	
}
