<?php

class Beneficios extends CI_Model {

	public function __construct()
	{
		$this->load->database();
	}
	

	public function obtener_beneficios_lubricentro( $id ){

		$fields = 'a.id,a.titulo,a.subtitulo,a.descripcion,a.descripcion_larga,a.vigencia_dias as vigencia,a.tipo_canje,a.tiempo_canje,a.fecha_caducidad,a.terminos,';
		$fields .= 'b.nombre_categoria as categoria,b.icono as icon, a.imagen,a.marca_logo as logo,a.marca';
		$sql = "SELECT $fields FROM beneficios_lubricentro a JOIN beneficios_categoria b ON a.categoria = b.id WHERE a.user='$id'";

		$query = $this->db->query($sql);

		$lista = [];
		foreach ($query->result() as $fila)
		{
			$lista[] = $fila;
		}
		return $lista;
	}
	public function get_tickets($id){
		
		$fields = 'a.fecha as fecha_promo,';
		$fields .= 'b.fecha_caducidad,';
		$fields .= 'a.usado,';
		$fields .= 'b.delivery,';
		$fields .= 'b.tiempo_canje,';
		$fields .= 'b.terminos,';
		$fields .= 'a.id,';
		$fields .= 'b.tipo_canje,';
		$fields .= 'b.titulo,';
		$fields .= 'a.fecha_canje,';
		$fields .= 'a.code,';
		$fields .= 'c.razon_social,';
		$fields .= 'c.direccion';
		
		$sql = "SELECT $fields FROM promo_codes a ";
		$sql .= "JOIN beneficios b ON a.promo = b.id ";
		$sql .= "LEFT JOIN lubricentro c ON b.lubricentro_canje = c.id ";
		$sql .= "WHERE a.user ='$id'";
		$query = $this->db->query($sql);

		$lista = [];
		foreach ($query->result() as $fila)
		{
			$lista[] = $fila;
		}
		return $lista;
	}
	public function canjes($iduser){
		$sql = "SELECT * FROM canjes a JOIN beneficios b ON a.beneficio = b.id WHERE lubricentro = '$iduser' AND a.fecha_redencion IS NULL ORDER BY fecha_canje DESC";
		$query = $this->db->query($sql);
		$lista = [];
		foreach ($query->result() as $fila){
			$item = array();
			array_push($lista, $fila);
		}
		return $lista;
	}
	public function canjeados($iduser){
		$sql = "SELECT * FROM canjes a JOIN beneficios b ON a.beneficio = b.id WHERE lubricentro = '$iduser' AND a.fecha_redencion IS NOT NULL ORDER BY fecha_canje DESC";
		$query = $this->db->query($sql);
		$lista = [];
		foreach ($query->result() as $fila){
			$item = array();
			array_push($lista, $fila);
		}
		return $lista;
	}
	
	public function buscar_id($id){
		$sql = "SELECT * FROM beneficios WHERE id = '$id' ";
		$query = $this->db->query($sql);
		$r = $query->result();
		return $r?$r[0]:null;
	}
	public function obtener_nuevos($iduser){
		$fields = 'a.id,a.titulo,a.grupo,a.stock,a.puntos,a.descripcion,a.descripcion_larga,a.tipo_canje,a.fecha_caducidad,a.terminos,';
		$fields .= 'b.nombre_categoria as categoria,a.imagen,a.imagen2,a.imagen3,c.logo,a.marca,a.activo';
		
		$sql = "SELECT $fields FROM beneficios a ";
		$sql .= "JOIN beneficios_categoria b ";  
		$sql .= "ON a.categoria = b.id  ";
		$sql .= "JOIN marcas_afiliadas c ";  
		$sql .= "ON a.marca = c.id  ";
		$sql .= "WHERE a.stock>0 ORDER BY a.fecha DESC LIMIT 0,3";

 		$query = $this->db->query($sql);

		$lista = [];
		foreach ($query->result() as $fila)
		{
			$lista[] = $fila;
		}
		return $lista;
	}
	public function obtener3($iduser,$categoria,$grupo){
		$sql = "SELECT $fields FROM beneficios a ";
		
		$sql .= "JOIN beneficios_categoria b ";  
		$sql .= "ON a.categoria = b.id  ";
		
		//$sql .= "JOIN grupos d ";  
		//$sql .= "ON a.grupo = d.id  ";
		
		$sql .= "JOIN marcas_afiliadas c ";  
		$sql .= "ON a.marca = c.id  ";
		
		
		if(intval($categoria)==-1 && intval($grupo)!=-1){
			$sql .= " WHERE grupo=$grupo AND a.activo=1 ";
		}else if(intval($grupo)==-1 && intval($categoria)!=-1){
			$sql .= " WHERE categoria=$categoria AND a.activo=1 ";
		}else if(intval($categoria)!=-1 && intval($grupo)!=-1){
			$sql .= " WHERE grupo=$grupo AND a.activo=1 ";
			$sql .= " AND categoria=$categoria ";
		}else{
			$sql .= " WHERE a.activo=1 ";
		}
		
		
		$sql .= "ORDER BY a.id DESC ";
		echo $sql;
	}
	public function obtener($iduser,$categoria,$grupo){

		$fields = 'a.id,a.titulo,a.grupo,a.puntos,a.descripcion,a.descripcion_larga,a.tipo_canje,a.fecha_caducidad,a.terminos,';
		$fields .= 'b.nombre_categoria as categoria,a.imagen,a.imagen2,a.imagen3,c.logo,a.marca,a.activo';
		
		$sql = "SELECT $fields FROM beneficios a ";
		
		$sql .= "JOIN beneficios_categoria b ";  
		$sql .= "ON a.categoria = b.id  ";
		
		//$sql .= "JOIN grupos d ";  
		//$sql .= "ON a.grupo = d.id  ";
		
		$sql .= "JOIN marcas_afiliadas c ";  
		$sql .= "ON a.marca = c.id  ";
		
		
		if(intval($categoria)==-1 && intval($grupo)!=-1){
			$sql .= " WHERE grupo=$grupo AND a.activo=1 AND a.stock>0 ";
		}else if(intval($grupo)==-1 && intval($categoria)!=-1){
			$sql .= " WHERE categoria=$categoria AND a.activo=1 AND a.stock>0 ";
		}else if(intval($categoria)!=-1 && intval($grupo)!=-1){
			$sql .= " WHERE grupo=$grupo AND a.activo=1 AND a.stock>0 ";
			$sql .= " AND categoria=$categoria ";
		}else{
			$sql .= " WHERE a.activo=1 AND a.stock>0 ";
		}
		
		
		$sql .= "ORDER BY a.id DESC ";

 
		$query = $this->db->query($sql);

		$lista = [];
		foreach ($query->result() as $fila)
		{
			$lista[] = $fila;
		}
		return $lista;
	}
	
	public function promociones($iduser){

		$fields = 'a.id,a.titulo,a.grupo,a.puntos,a.descripcion,a.descripcion_larga,a.tipo_canje,a.fecha_caducidad,a.terminos,';
		$fields .= 'b.nombre_categoria as categoria,a.imagen,c.logo,a.marca,a.activo,a.delivery';
		
		$sql = "SELECT * FROM promociones";
 
		$query = $this->db->query($sql);

		$lista = [];
		foreach ($query->result() as $fila)
		{
			$lista[] = $fila;
		}
		return $lista;
	}
	
	public function obtener2( $id ){

		$fields = 'a.id,a.titulo,a.subtitulo,a.descripcion,a.descripcion_larga,a.vigencia_dias as vigencia,a.tipo_canje,a.tiempo_canje,a.meta_canje,a.fecha_caducidad,a.fecha_inicio_promo,a.terminos,';
		$fields .= 'b.nombre_categoria as categoria,b.icono as icon, a.imagen,a.imagen2,a.imagen3,a.marca_logo as logo,a.marca,c.usado,a.gatillo,a.dias_actividad,a.activo';
		
		$sql = "SELECT $fields FROM beneficios a ";
		$sql .= "LEFT JOIN promo_codes c ";
		$sql .= "ON a.id = c.promo ";
		$sql .= "LEFT JOIN beneficios_categoria b ";  
		$sql .= "ON a.categoria = b.id  ";
		$sql .= "WHERE a.user='$id' ";
		$sql .= "ORDER BY a.id DESC ";
		

		$query = $this->db->query($sql);
		$lista = [];
		foreach ($query->result() as $fila){
			if($fila->tipo_canje=="Meta" && intval($fila->activo)==1){
				$fecha_inicio_promo = $fila->fecha_inicio_promo;
				$fecha_caducidad = $fila->fecha_caducidad;
				$sql = "SELECT SUM(galones) as total FROM evidencia_total ";
				$sql .= "WHERE socio = '".$id."' ";
				$sql .= "AND (fecha BETWEEN '$fecha_inicio_promo' AND '$fecha_caducidad') ";
				$query_inner = $this->db->query($sql);
				foreach ($query_inner->result() as $fila_inner){
					$fila->galones_actuales = intval($fila_inner->total);
					break;
				}
			}
			$lista[] = $fila;
		}
		return $lista;
	}
}
