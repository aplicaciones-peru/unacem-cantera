<?php

class Codigos extends CI_Model {

	public function __construct()
	{
		$this->load->database();
		$this->load->helper("numeracion");
	}

	public function analizar( $codigo ) {
		$cuerpo = substr($codigo, 0, 5);
		$crce = substr($codigo, -1);
		$crci = substr(crc32($cuerpo),-1);

		$rpta = (Object)[
			"texto" => $codigo,
			"valido" => (bool)( $crce == $crci ),
			"agno" => Base36::aBase10( substr( $cuerpo, -1 )) + 2000 - 1,
			"numero" => Base36::aBase10( substr( $cuerpo, 0, 4 ))
		];

		return $rpta;
	}

	public function buscar( $codigo ){
		$query = $this->db
			->where("id", $codigo->texto)
			->get("codigo");
		$r = $query->result();
		return $r?$r[0]:null;
	}
	
	public function buscar_producto( $codigo , $serie){
		if($serie->producto->id==23){
			return true;
		}
		else return false;
	}
	public function buscar_promo( $codigo ){
		$query = $this->db
			->where("codigo", $codigo->texto)
			->get("promo_redencion");
		$r = $query->result();
		return $r?$r[0]:null;
	}
	public function buscar_codigo( $codigo ){
		$query = $this->db
			->where("id", $codigo)
			->get("codigo");
		$r = $query->result();
		return $r?$r[0]:null;
	}
	public function buscarSerie( $codigo ) {
		$query = $this->db
			->limit(1)
			->order_by( "inicio DESC" )
			->where("YEAR(fecha) = {$codigo->agno} AND ( inicio <= {$codigo->numero} AND cantidad > {$codigo->numero} - inicio )")
			->get("serie");

		//print_r( $this->db->queries );
		$r = $query->result();

		$s = $r?$r[0]:null;

		if ( $s != null ) {
			$query2 = $this->db
				->get_where('productos', array('id' => $s->producto), 1);
			$r2 = $query2->result();

			$s->producto = $r2?$r2[0]:null;
		}

		return $s;
	}
	public function getdate() {
		return date('Y-m-d h:i:s');
	}
	public function asignar( $codigo, $serie, $socio, $lubricentro, $app ){
		
		$now = $this->getdate();
		if($lubricentro==''){
			$lubricentro=0;
		}
		$sql = "UPDATE socio SET estado='a. ACTIVO',ultimo_consumo='$now' WHERE id='$socio'";
		$this->db->query($sql);
		$lubricentroid = intval($lubricentro);
		if($lubricentroid==0){
			$sql = "SELECT lubricentro FROM socio WHERE id='$socio'";
			$query2 = $this->db->query($sql);
			$r2 = $query2->result_array();
			$lubricentroid = $r2[0]['lubricentro'];

		}
		return $this->db->insert("codigo", [
			"id" => $codigo->texto,
			"serie" => $serie->id,
			"socioid" => $socio,
			"timestamp" => $now,
			"lubricentroid" => $lubricentroid,
			"app" => $app
		]);
	}
	public function asignar_promo( $codigo, $serie){
		
		$now = $this->getdate();
		return $this->db->insert("promo_redencion", [
			"codigo" => $codigo->texto,
			"id_producto" => $serie->producto->id,
			"serie" => $serie->id,
			"fecha" => $now
		]);
	}

	public function contar( $socio ){
		$query = $this->db
			->select( "count( id ) as cuenta" )
			->where("socioid", $socio)
			->get("codigo");
		$r = $query->result();
		return $r?$r[0]->cuenta:0;
	}
}
