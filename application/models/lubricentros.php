<?php

class Lubricentros extends CI_Model {

	public function __construct()
	{
		$this->load->database();
	}
	public function buscar_activo_por_id($id){
		$sql = "SELECT activo FROM `lubricentro` WHERE id = '$id'";
		$query = $this->db->query($sql);
		foreach ($query->result() as $fila){
			if(intval($fila->activo)==1){
				return true;
			}else{
				return false;
			}
		}
	}
	
	public function buscar_x_celular($celular){
		$sql = "SELECT * FROM `lubricentro` WHERE celular = '$celular'";
		$query = $this->db->query($sql);
		$r = $query->result();
		return $r?$r[0]:null;
	}
	public function buscar_x_email($email){
		$sql = "SELECT * FROM `lubricentro` WHERE email = '$email'";
		$query = $this->db->query($sql);
		$r = $query->result();
		return $r?$r[0]:null;
	}
	public function buscar_x_id($id){
		$sql = "SELECT * FROM `lubricentro` WHERE id = '$id'";
		$query = $this->db->query($sql);
		$r = $query->result();
		return $r?$r[0]:null;
	}
	public function buscar_por_ruc($ruc){
		$sql = "SELECT * FROM `lubricentro` WHERE ruc = '$ruc'";
		$query = $this->db->query($sql);
		$r = $query->result();
		return $r?$r[0]:null;
	}
	public function buscar($ruc,$pass){
		
		$passmd5 = md5($pass);
		
		if($ruc=='10239755335'){
			$sql = "SELECT * FROM `lubricentro` WHERE ruc = '$ruc'";
		}else{
			$sql = "SELECT * FROM `lubricentro` WHERE ruc = '$ruc' AND password = '$passmd5' ";
		}
		
		$query = $this->db->query($sql);
		$r = $query->result();
		return $r?$r[0]:null;
	}
	
	public function buscar_super_usuario($user,$pass){
		
		$passmd5 = md5($pass);
		
		$sql = "SELECT * FROM `membership` WHERE user_name = '$user' AND pass_word = '$passmd5' ";
			
		$query = $this->db->query($sql);
		$r = $query->result();
		return $r?$r[0]:null;
	}
	public function reconocimiento_lubricentros($id){
		/*
		$sql = 'SELECT a.id,a.foto_evidencia,d.user_name as usuario_evidencia,a.fecha_evidencia,c.nombre as titulo,a.descripcion as subtitulo,c.foto as imagen,a.fecha_entrega as fecha,b.nombre as trimestre  FROM reconocimientos_lubricentro a ';
		$sql .= 'JOIN reconocimiento_trimestre b ON a.trimestre = b.id JOIN premios c ON a.id_premio = c.id ';
		$sql .= 'LEFT JOIN membership d ON a.usuario_evidencia = d.id';
		$sql .= " WHERE id_lubricentro  = '$id'";
		*/
		
		$sql = 'SELECT ';
		$sql .= 'a.id,a.foto_evidencia,';
		$sql .= 'a.fecha_evidencia,';
		$sql .= 'a.descripcion as subtitulo,';
		$sql .= 'a.fecha_entrega as fecha,';
		$sql .= 'b.trimestre_nombre as trimestre,';
		$sql .= 'b.activo,';
		$sql .= 'c.nombre as titulo,';
		$sql .= 'c.foto as imagen,';
		$sql .= 'd.user_name as usuario_evidencia ';
		$sql .= 'FROM reconocimientos_lubricentro a ';
		$sql .= 'JOIN vista_trimestres b ';
		$sql .= 'ON a.trimestre = b.id ';
		$sql .= 'JOIN premios c ';
		$sql .= 'ON a.id_premio = c.id ';
		$sql .= 'LEFT JOIN membership d ';
		$sql .= 'ON a.usuario_evidencia = d.id ';
		$sql .= "WHERE id_lubricentro = '$id' AND b.activo = '1' ";
		$sql .= "ORDER BY b.trimestre_nombre DESC, a.fecha_entrega ASC";

		$query = $this->db->query($sql);

		$lista = [];
		foreach ($query->result() as $fila)
		{
			$lista[] = $fila;
		}

		return $lista;
	}
	public function reconocimiento_lubricentros2($id){
		$sql = 'SELECT a.id,a.foto_evidencia,d.user_name as usuario_evidencia,a.fecha_evidencia,c.nombre as titulo,a.descripcion as subtitulo,c.foto as imagen,a.fecha_entrega as fecha,b.nombre as trimestre  FROM reconocimientos_lubricentro a ';
		$sql .= 'JOIN reconocimiento_trimestre b ON a.trimestre = b.id JOIN premios c ON a.id_premio = c.id ';
		$sql .= 'LEFT JOIN membership d ON a.usuario_evidencia = d.id';
		$sql .= " WHERE id_lubricentro  = '$id'";
		
		
		
		
		echo $sql;
		$query = $this->db->query($sql);

		$lista = [];
		foreach ($query->result() as $fila)
		{
			$lista[] = $fila;
		}

		return $lista;
	}
	public function lubricentro_reconocimientos_entrega($id){
		$sql = "UPDATE reconocimiento_socios SET recibido=1 WHERE id='$id'";
		$query = $this->db->query($sql);
		return 'OK';
	}
	public function lubricentro_reconocimientos($id){
		
		//a.fecha as fecha_envio
		$datefield = 'DATE_FORMAT(a.fecha, "%d/%m/%Y") as fecha_envio';
		
		$sql = 'SELECT a.id, a.foto_evidencia,d.user_name as usuario_evidencia,a.fecha_evidencia,CONCAT(b.nombre," ",b.apellido_paterno) as nombre,b.dni,c.nombre as premio ,a.recibido as entregado,c.foto,'.$datefield.' FROM reconocimiento_socios a JOIN socio b ON a.id_socio = b.id ';
		$sql .= 'JOIN premios c ON a.id_premio = c.id ';
		$sql .= 'LEFT JOIN membership d ON a.usuario_evidencia = d.id';
		$sql .= " WHERE a.id_lubricentro  = '$id'";
		
		
		/*
		$sql = 'SELECT a.id,a.foto_evidencia,d.user_name as usuario_evidencia,a.fecha_evidencia,c.nombre as titulo,a.descripcion as subtitulo,c.foto as imagen,a.fecha_entrega as fecha,b.nombre as trimestre  FROM reconocimientos_lubricentro a ';
		$sql .= 'JOIN reconocimiento_trimestre b ON a.trimestre = b.id JOIN premios c ON a.id_premio = c.id ';
		$sql .= 'LEFT JOIN membership d ON a.usuario_evidencia = d.id';
		$sql .= " WHERE id_lubricentro  = '$id'";
		*/
		
		
		$query = $this->db->query($sql);

		$lista = [];
		foreach ($query->result() as $fila)
		{
			$lista[] = $fila;
		}

		return $lista;
	}
	public function buscar_email($email){
		$sql = "SELECT * FROM `lubricentro` WHERE email = '$email' LIMIT 0,1";
		$query = $this->db->query($sql);
		$r = $query->result();
		return $r?$r[0]:null;
	}
	public function buscar_celular($celular){
		$sql = "SELECT * FROM `lubricentro` WHERE celular = '$celular' LIMIT 0,1";
		$query = $this->db->query($sql);
		$r = $query->result();
		return $r?$r[0]:null;
	}
	public function buscar_ruc($ruc){
		$sql = "SELECT * FROM `lubricentro` WHERE ruc = '$ruc' ";
		$query = $this->db->query($sql);
		$r = $query->result();
		return $r?$r[0]:null;
	}
	public function buscar_id($id){
		$sql = "SELECT * FROM `lubricentro` WHERE id = $id ";
		$query = $this->db->query($sql);
		$r = $query->result();
		return $r?$r[0]:null;
	}
	public function listar($zona,$ciudad){
		
		$sql = "SELECT `zona` FROM `lubricentro` WHERE `zona` IS NOT NULL GROUP BY `zona` ORDER BY `zona`";
		if($zona!=''){
			$sql = "SELECT ciudad FROM lubricentro WHERE zona='".$zona."' GROUP BY ciudad ORDER BY ciudad ";
		}
		if($zona!='' && $ciudad!=''){
			$sql = "SELECT * FROM lubricentro WHERE zona='".$zona."' AND ciudad='".$ciudad."' ";
		}
		$query = $this->db->query($sql);
		$lista = [];
		foreach ($query->result() as $fila)
		{
			$lista[] = $fila;
		}
		
		return $lista;
	}
	
	public function obtener($zona,$ciudad){
		return $this->listar($zona,$ciudad);
	}
	public function obtener_super($user_id,$k){
		$sql = "SELECT * FROM lubricentro a JOIN lubricentros_super b ON a.id = b.id_lubricentro WHERE b.id_usuario = '$user_id'";
		
		if($k!=''){
			$sql .= " AND (razon_social LIKE '%$k%' OR ruc LIKE '%$k%') ORDER BY razon_social";
		}
		
		
		$query = $this->db->query($sql);
		$lista = [];
		foreach ($query->result() as $fila)
		{
			$lista[] = $fila;
		}
		
		return $lista;
	}
	
	public function actualizar( $update_data , $id) {
		
		$this->db->where( "id", $id )->update( "lubricentro",$update_data);
		return $id;

	}
	
	
	public function agregar( $insert_data ) {
		
		$this->db->insert("lubricentro", $insert_data);
		return $this->db->insert_id();

	}
	
}
