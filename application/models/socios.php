<?php

class Socios extends CI_Model {

	public function __construct()
	{
		$this->load->database();
	}

	public function agregar( $nombre, $apellido_pat,$apellido_mat, $dni, $celular, $email, $vehiculo,$lubricentro,$empresa,$iv,$flag,$app_type) {
		
		$lubricentro_razon = "";
		$lubricentro_id = 0;
		$lubricentro_zona = "";
		$lubricentro_ciudad = "";
		
		if($lubricentro!=""){ 
 
			$query = $this->db->where( "codigo", $lubricentro)->get("lubricentro");
			$r = $query->result();
			if($r){ 
				$lubricentro_razon = $r[0]->razon_social; 
				$lubricentro_id = $r[0]->id; 
				$lubricentro_zona = $r[0]->zona; 
				$lubricentro_ciudad = $r[0]->ciudad; 
	
			}
		}
		 
		$v_01_data = '';
		if($vehiculo!=''){
			$v_01 = intval($vehiculo);
			if($v_01==0){
				$v_01_data = 'Trailer';
			}
			if($v_01==1){
				$v_01_data = 'Combi';
			} 
			if($v_01==2){
				$v_01_data = 'Bus/Custer';
			}
		}
		if($v_01_data!=""){ 
			$this->db->insert("socio", [
				"app_type" => $app_type,
				"nombre" => $nombre,
				"apellido_paterno" => $apellido_pat,
				"apellido_materno" => $apellido_mat,
				"dni" => $dni,
				"celular" => $celular,
				"email" => $email,
				"tipo_veh01" => $v_01_data,
				"cod_estb" => $lubricentro,
				"establecim" => $lubricentro_razon,
				"zona" => $lubricentro_zona,
				"ciudad" => $lubricentro_ciudad,
				"lubricentro" => $lubricentro_id,
				"cantidad_vehiculos" => '1',
				"fecha_afiliacion" => date('Y/m/d'),
				"fecha_actualizacion" => date('Y/m/d')
				
			]);
		}else if($flag==false){
			$this->db->insert("socio", [
				"app_type" => $app_type,
				"nombre" => $nombre,
				"apellido_paterno" => $apellido_pat,
				"apellido_materno" => $apellido_mat,
				"dni" => $dni,
				"celular" => $celular,
				"email" => $email,
				"cod_estb" => $lubricentro,
				"establecim" => $lubricentro_razon,
				"zona" => $lubricentro_zona,
				"ciudad" => $lubricentro_ciudad,
				"lubricentro" => $lubricentro_id,
				"fecha_afiliacion" => date('Y/m/d'),
				"fecha_actualizacion" => date('Y/m/d')
				
			]);
		}else{
			
					$insert_data = array();
					$insert_data["app_type"] 				= $app_type;
					$insert_data["nombre"] 				= $nombre;
					$insert_data["apellido_paterno"] 		= $apellido_pat;
					$insert_data["apellido_materno"] 		= $apellido_mat;
					$insert_data["dni"] 					= $dni;
					$insert_data["celular"] 				= $celular;
					$insert_data["email"] 					= $email;
					$insert_data["cod_estb"] 				= $lubricentro;
					$insert_data["establecim"] 			= $lubricentro_razon;
					$insert_data["zona"] 					= $lubricentro_zona;
					$insert_data["ciudad"] 				= $lubricentro_ciudad;
					$insert_data["lubricentro"] 			= $lubricentro_id;
					$insert_data["empresa"] 				= $empresa;
					
					$insert_data["fecha_afiliacion"] 		= date('Y/m/d');
					$insert_data["fecha_actualizacion"] 	= date('Y/m/d');

					if($iv['tipo_veh01']!=''){	
						$insert_data["tipo_veh01"]	= $iv['tipo_veh01']; 
					}else{
						$insert_data["tipo_veh01"]	= null; 
					}
					$insert_data["placa_veh01"] 			= $iv['placa_veh01'];
					$insert_data["marca_veh01"] 			= $iv['marca_veh01'];
					if($iv['tipo_veh02']!=''){	
						$insert_data["tipo_veh02"]	= $iv['tipo_veh02']; 
					}else{
						$insert_data["tipo_veh02"]	= null; 
					}
					$insert_data["placa_veh02"] 			= $iv['placa_veh02'];
					$insert_data["marca_veh02"] 			= $iv['marca_veh02'];
					if($iv['tipo_veh03']!=''){	
						$insert_data["tipo_veh03"]	= $iv['tipo_veh03']; 
					}else{
						$insert_data["tipo_veh03"]	= null; 
					}
					$insert_data["placa_veh03"] 			= $iv['placa_veh03'];
					$insert_data["marca_veh03"] 			= $iv['marca_veh03'];
					if($iv['tipo_veh04']!=''){	
						$insert_data["tipo_veh04"]	= $iv['tipo_veh04']; 
					}else{
						$insert_data["tipo_veh04"]	= null; 
					}
					$insert_data["placa_veh04"] 			= $iv['placa_veh04'];
					$insert_data["marca_veh04"] 			= $iv['marca_veh04'];
					if($iv['tipo_veh05']!=''){	
						$insert_data["tipo_veh05"]	= $iv['tipo_veh05']; 
					}else{
						$insert_data["tipo_veh05"]	= null; 
					}
					$insert_data["placa_veh05"] 			= $iv['placa_veh05'];
					$insert_data["marca_veh05"] 			= $iv['marca_veh05'];
	
					
			$this->db->insert("socio",$insert_data);
		}

		return $this->db->insert_id();

	}

	//public function agregar(

	public function actualizar( $id, $nombre, $apellido_pat,$apellido_mat, $dni, $celular, $email, $vehiculo, $iv = null, $empresa = '', $nacimiento = '', $ol = '') {
		$v_01_data = '';
		if($vehiculo!=''){
			$v_01 = intval($vehiculo);
			if($v_01==0){
				$v_01_data = 'Trailer';
			}
			if($v_01==1){
				$v_01_data = 'Combi';
			}
			if($v_01==2){
				$v_01_data = 'Bus/Custer';
			}
			if($v_01==3){
				$v_01_data = 'Otro';
			}
		}		

		if($v_01_data!=""){
			$this->db->where( "id", $id )
			->update( "socio", [
				"nombre" => $nombre,
				"apellido_paterno" => $apellido_pat,
				"apellido_materno" => $apellido_mat,
				"dni" => $dni,
				"celular" => $celular,
				"empresa" => $empresa,
				"email" => $email,
				"tipo_veh01" => $v_01_data,
				"fecha_actualizacion" => date('Y/m/d')
			]);
		}else{
			if($iv!=null){

				$update_data = array();
				
					$update_data["nombre"] 				= $nombre;
					$update_data["apellido_paterno"] 		= $apellido_pat;
					$update_data["apellido_materno"] 		= $apellido_mat;
					$update_data["dni"] 					= $dni;
					$update_data["celular"] 				= $celular;
					$update_data["email"] 					= $email;
					$update_data["empresa"] 				= $empresa;
					$update_data["observacion_lubricentro"]= $ol;
					$update_data["fecha_actualizacion"] 	= date('Y/m/d');
					
					if($nacimiento!=''){
						$update_data["nacimiento"] 	= $nacimiento;
					}
					if($iv['tipo_veh01']!=''){	
						$update_data["tipo_veh01"]	= $iv['tipo_veh01']; 
					}else{
						$update_data["tipo_veh01"]	= null; 
					}
					$update_data["placa_veh01"] 			= $iv['placa_veh01'];
					$update_data["marca_veh01"] 			= $iv['marca_veh01'];
					if($iv['tipo_veh02']!=''){	
						$update_data["tipo_veh02"]	= $iv['tipo_veh02']; 
					}else{
						$update_data["tipo_veh02"]	= null; 
					}
					$update_data["placa_veh02"] 			= $iv['placa_veh02'];
					$update_data["marca_veh02"] 			= $iv['marca_veh02'];
					if($iv['tipo_veh03']!=''){	
						$update_data["tipo_veh03"]	= $iv['tipo_veh03']; 
					}else{
						$update_data["tipo_veh03"]	= null; 
					}
					$update_data["placa_veh03"] 			= $iv['placa_veh03'];
					$update_data["marca_veh03"] 			= $iv['marca_veh03'];
					if($iv['tipo_veh04']!=''){	
						$update_data["tipo_veh04"]	= $iv['tipo_veh04']; 
					}else{
						$update_data["tipo_veh04"]	= null; 
					}
					$update_data["placa_veh04"] 			= $iv['placa_veh04'];
					$update_data["marca_veh04"] 			= $iv['marca_veh04'];
					if($iv['tipo_veh05']!=''){	
						$update_data["tipo_veh05"]	= $iv['tipo_veh05']; 
					}else{
						$update_data["tipo_veh05"]	= null; 
					}
					$update_data["placa_veh05"] 			= $iv['placa_veh05'];
					$update_data["marca_veh05"] 			= $iv['marca_veh05'];
					
				
				
				$this->db->where( "id", $id )
				->update( "socio", $update_data);

			}else{
				$this->db->where( "id", $id )
				->update( "socio", [
					"nombre" => $nombre,
					"apellido_paterno" => $apellido_pat,
					"apellido_materno" => $apellido_mat,
					"dni" => $dni,
					"celular" => $celular,
					"email" => $email,
					"fecha_actualizacion" => date('Y/m/d')
				]);
			}
			
			
			
			
			
			
			
		}
		

		return $this->db->insert_id();

	}

	public function buscar( $dni ){
		$query = $this->db
			->where( "dni", $dni)
			->get("socio");

		$r = $query->result();
		return $r?$r[0]:null;
	}
	public function buscar_id( $id ){
		$query = $this->db
			->where( "id", $id)
			->get("socio");

		$r = $query->result();
		return $r?$r[0]:null;
	}
	public function existe( $dni ){
		return $this->buscar( $dni ) != null;
	}
	public function canjes(){
		//$sql = "SELECT * FROM canjes WHERE socio = '".$id."'";
		$sql = "SELECT * FROM canjes a JOIN beneficios b ";
		$sql .= "ON a.beneficio = b.id";
		$query = $this->db->query($sql);

		$lista = [];
		foreach ($query->result() as $fila)
		{
			$lista[] = $fila;
		}
		return $lista;
	}
	public function obtener_evidencias($id){
		$sql = "SELECT * FROM evidencia_total WHERE socio = '".$id."'";
		$query = $this->db->query($sql);

		$lista = [];
		foreach ($query->result() as $fila)
		{
			$lista[] = $fila;
		}
		return $lista;
	}
	public function obtener_galones($id,$promoid){
		
		$sql = "SELECT fecha,fecha_inicio_promo,fecha_caducidad,dias_actividad,gatillo FROM beneficios WHERE id = '".$promoid."'";
		$query = $this->db->query($sql);
		$beneficios = $query->result_array();

		$fecha_inicio_promo = $beneficios[0]['fecha_inicio_promo'];
		$dias_actividad = intval($beneficios[0]['dias_actividad']);
		$fecha_caducidad = $beneficios[0]['fecha_caducidad'];
		if(intval($beneficios[0]['gatillo'])==1){
			$fecha_caducidad = date('Y-m-d H:i:s', strtotime($fecha_inicio_promo. ' + '.$dias_actividad.' days'));
		}
		
		$sql = "SELECT SUM(galones) as total FROM evidencia_total ";
		$sql .= "WHERE socio = '".$id."' ";
		$sql .= "AND (fecha BETWEEN '".$beneficios[0]['fecha_inicio_promo']."' AND '".$fecha_caducidad."') ";
		//echo $sql;
		$query = $this->db->query($sql);

		$lista = [];
		foreach ($query->result() as $fila)
		{
			$lista[] = $fila;
		}
		return $lista[0];
	}
	public function listarBeneficios( $id ){
		$sql = "SELECT * FROM beneficios WHERE user = '$id'";
		$query = $this->db->query($sql);

		$lista = [];
		foreach ($query->result() as $fila)
		{
			$lista[] = $fila;
		}
		return $lista;
	}
	public function listarCodigos( $id ){
		//$query = $this->db
		//	->where("socio", $id)
		//	->get("codigo");

		//$id = 10577;
		$sql = 'SELECT a.id,a.timestamp,CONCAT(c.nombre," ",c.tipo," ",c.galones," galones") as nombre ';
		$sql .= 'FROM codigo a LEFT JOIN serie b ';
		$sql .= 'ON a.serie = b.id ';
		$sql .= 'LEFT JOIN productos c ';
		$sql .= 'ON b.producto =c.id ';
		$sql .= "WHERE a.socioid = '$id' ORDER BY a.timestamp DESC";
		$query = $this->db->query($sql);

		$lista = [];
		foreach ($query->result() as $fila)
		{
			$lista[] = $fila;
		}
		/*
		$sql = 'SELECT a.id,a.fecha,CONCAT(c.nombre," ",c.tipo," ",c.galones," galones") as nombre '; 
		$sql .= ' FROM evidencia a';
		$sql .= ' LEFT JOIN productos c';
		$sql .= ' ON a.producto =c.id';
		$sql .= " WHERE a.socio = '$id' ORDER BY a.fecha DESC";
		$query = $this->db->query($sql);
		foreach ($query->result() as $fila)
		{
			$item = array();
			$item['id'] = "Anfora";
			$item['timestamp'] = $fila->fecha;
			if($fila->nombre==""){
				$item['nombre'] = "No especificado";
			}else{
				$item['nombre'] = $fila->nombre;
			}
			array_push($lista, $item);
		}
		*/
		return $lista;
	}

	public function obtener( $id ){
		$query = $this->db
			->where( "id", $id)
			->get("socio");

		$r = $query->row();
		$r->codigos = $this->listarCodigos( $id );
		$r->beneficios = $this->listarBeneficios( $id );
		return $r;
	}

	/*NOTIFICACIONES*/
	public function setBadgeCountZero($id){
		$data = array();
	    $data["notcount"] = 0;
	    $this->db->where('id', $id);
		$this->db->update('usuario', $data);
    }
	public function getBadgeCount($id){
	    $this->db->select('notcount');
        $query = $this->db->get_where('socio', array('id' => $id));
        $return = null;
        if($query->num_rows()==1){
            $return = $query->first_row();
        }
        return $return;
    }
	public function infoPorId($id){
        $this->db->select('notification_user_id');
        $query = $this->db->get_where('socio', array('id' => $id));
        $return = null;
        if($query->num_rows() == 1){
            $return = $query->first_row();
        }
        return $return;
    }
    
	public function addNotification($content,$notification_id){
        $sql = "INSERT INTO notificaciones (content,notification_id,fecha) VALUES ('".stripslashes($content)."','".$notification_id."',NOW())";
        $query = $this->db->query($sql);
    }
    public function removeNotification($notid){
        $sql = "DELETE FROM notificaciones WHERE id='".$notid."'";
        $query = $this->db->query($sql);
    }
	public function getNotificationes($userid,$tipo){
	    //$info = $this->infoPorId($userid);
	    //$notification_id = '';
	    //if($info){
		  //  $notification_id = $info->notification_user_id;
	    //}
        //$sql = "SELECT * FROM notificaciones WHERE (notification_id='') OR notification_id='".$notification_id."' ORDER BY timestamp DESC LIMIT 0,20";
        $sql = "SELECT * FROM notificaciones WHERE user_id='".$userid."' AND tipo = '".$tipo."' ORDER BY timestamp DESC LIMIT 0,20";
        $query = $this->db->query($sql);
        $result_a = $query->result_array();
        return $result_a;
    }



}
