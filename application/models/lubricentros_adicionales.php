<?php

class Lubricentros_adicionales extends CI_Model {

	public function __construct()
	{
		$this->load->database();
	}

	public function agregar( $ruc, $razon_social, $direccion, $codigo_vendedor, $telefono_contacto, $email, $password,$n1,$n2 ) {
		
			$this->db->insert("lubricentro", [
				"ruc" => $ruc,
				"razon_social" => $razon_social,
				"direccion" => $direccion,
				"codigo_vendedor" => $codigo_vendedor,
				"password" => md5($password),
				"pass_visible" => $password,
				"telefono_contacto" => $telefono_contacto,
				"email" => $email,
				"fecha_actualizacion" => date('Y/m/d'),
				"fecha_registro" => date('Y/m/d')
			]);
		
		
			
			
			
		$insert_id = $this->db->insert_id();
		
		
			$this->db->where("id",$insert_id)->update("lubricentro",["notification_push_id" =>$n1]);
			$this->db->where("id",$insert_id)->update("lubricentro",["notification_user_id" =>$n2]);
			
			
			
		$code_generated = 'L'.substr( strtoupper(md5(uniqid(rand(), true))), 0, 3).$insert_id;
		$this->db->where( "id", $insert_id )
			->update( "lubricentro", [ "codigo" => $code_generated ]);	
			
		$params = array();	
		$params['id'] = $insert_id;
		$params['codigo'] = $code_generated;
		return $params;

	}
	public function check_status($id){
		$sql = "SELECT COUNT(*) as total FROM lubricentro WHERE estado=1 AND id='$id'";
		$query = $this->db->query( $sql );
		foreach ($query->result() as $fila)
		{
			return $fila->total;
		}
	}
	public function actualizar( $id, $ruc, $razon_social, $direccion, $codigo_vendedor, $telefono_contacto, $email, $password, $aniversario ,$nombre_propietario) {
		
		
			$update = array();
			$update["ruc"] = $ruc;
			$update["razon_social"] = $razon_social;
			
			if($password!=""){
				$update["password"] = md5($password);
				$update["pass_visible"] = $password;
			}

			$update["direccion"] = $direccion;
			$update["codigo_vendedor"] = $codigo_vendedor;
			$update["telefono_contacto"] = $telefono_contacto;
			$update["email"] = $email;
			if($aniversario!=''){
				$update["aniver"] = $aniversario;
			}
			$update["nombre_propietario"] = $nombre_propietario;
			$update["fecha_actualizacion"] = date('Y/m/d');

			$this->db->where( "id", $id )->update( "lubricentro",$update);
			
		return $this->db->insert_id();

	}
	//lubricentro_socios_info
	public function lubricentro_socios_info($id){
		$sql = "SELECT COUNT(*) as total FROM socio WHERE lubricentro  = '$id' AND estado != 'e. EXCLUIDO' AND estado != 'd. BAJA'";
		$query = $this->db->query($sql);
		$lista = array();
		foreach ($query->result() as $fila){
			$lista['total'] = $fila->total;
		}
		$sql = "SELECT COUNT(*) as total FROM socio WHERE lubricentro  = '$id' AND estado = 'a. ACTIVO'";
		$query = $this->db->query($sql);
		foreach ($query->result() as $fila){
			$lista['activos'] = $fila->total;
		}
		
		$sql = "SELECT COUNT(*) as total FROM socio WHERE lubricentro  = '$id' AND estado = 'c. INACTIVO'";
		$query = $this->db->query($sql);
		foreach ($query->result() as $fila){
			$lista['inactivos'] = $fila->total;
		}
		
		$sql = "SELECT COUNT(*) as total FROM socio WHERE lubricentro  = '$id' AND estado = 'b. ALERTA'";
		$query = $this->db->query($sql);
		foreach ($query->result() as $fila){
			$lista['alerta'] = $fila->total;
		}
		$sql = "SELECT COUNT(*) as total FROM socio WHERE lubricentro  = '$id' AND estado = 'd. BAJA'";
		$query = $this->db->query($sql);
		foreach ($query->result() as $fila){
			$lista['baja'] = $fila->total;
		}
		$sql = "SELECT meta_socios FROM lubricentro WHERE id = '$id'";
		$query = $this->db->query($sql);
		foreach ($query->result() as $fila){
			$lista['meta_socios'] = $fila->meta_socios;
		}
		
		return $lista;
		
	}
	public function lubricentro_socios($id,$k,$f){
		
		$fields = "tipo_veh01,placa_veh01,marca_veh01";
		$fields .= ",tipo_veh02,placa_veh02,marca_veh02";
		$fields .= ",tipo_veh03,placa_veh03,marca_veh03";
		$fields .= ",tipo_veh04,placa_veh04,marca_veh04";
		$fields .= ",tipo_veh05,placa_veh05,marca_veh05";
		
		$estado = "";
		if($k=='-1'){
			$k='';
		}
		if($f!='all' && $f!=''){

			if($f=="ACTIVO")	$f="a. ACTIVO";
			if($f=="ALERTA")	$f="b. ALERTA";
			if($f=="INACTIVO")	$f="c. INACTIVO";
			if($f=="BAJA")		$f="d. BAJA";
			$estado = " AND estado = '$f' ";
		}
		
		$sql = 'SELECT id,dni,estado,fecha_afiliacion,celular,observacion_lubricentro,CONCAT(nombre," ",apellido_paterno) as nombre,empresa,'.$fields;
		if($k==''){
			$sql .= " FROM socio WHERE lubricentro  = '$id' AND estado != 'e. EXCLUIDO' $estado ORDER BY estado";
		}else{
			$sql .= " FROM socio WHERE lubricentro  = '$id' AND estado != 'e. EXCLUIDO' $estado AND (nombre LIKE '%$k%' OR apellido_paterno LIKE '%$k%' OR empresa LIKE '%$k%') ORDER BY estado";
		}
		//echo $sql;

		$query = $this->db->query($sql);
		$lista = [];
		foreach ($query->result() as $fila)
		{
			$idsocio = $fila->id;
			$sql = "SELECT COUNT(*) as total FROM beneficios WHERE user='$idsocio'";
			$query = $this->db->query($sql);
			foreach ($query->result() as $row){
				$fila->beneficios = $row->total;
			}
			$lista[] = $fila;
		}
		return $lista;
	}
	public function buscar( $ruc ){
		$query = $this->db
			->where( "ruc", $ruc)
			->get("lubricentro");

		$r = $query->result();
		return $r?$r[0]:null;
	}

	public function existe( $ruc ){
		return $this->buscar( $ruc ) != null;
	}
	
	public function obtener( $ruc ){
		$query = $this->db
			->where( "ruc", $ruc)
			->get("lubricentro");

		$r = $query->row();
		return $r;
	}

	
}
