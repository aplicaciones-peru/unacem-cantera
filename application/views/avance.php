<script>
	var nombres = sessionStorage.getItem('nombres');
	var modulo = sessionStorage.getItem('modulo');
	if(nombres==null || nombres==undefined){
		document.location.href = "<?php echo base_url(); ?>login_alumno";
	}
</script><style>
	.texto .contenido.hidden{
		display: none;
	}
</style>
<main id="main" class="mb-3">
	<div class="container">
        <div class="row">
        <div class="col-lg-12 col-md-12 avance_content">
          <h1>REVISA TU AVANCE</h1>
          <div class="line_avance">
            <div></div>
          </div>
          <p>
            Revisa aquí, tu avance en el programa. Podrás ver los cursos que ya has completado y los que te faltan por desarrollar. Además, descubre los premios* que ganarás cuando culmines de manera satisfactoria cada uno de los 3 módulos** del programa. ¡Continúa capacitándote, tu esfuerzo será reconocido!

          </p>
        </div>
        </div>
        
        <div class="row">
        	<div class="col-lg-12 col-md-12">
	        	<div class="box_avance_content">
		        	<div class="box_avance disable" id="box_avance_1">
			        	<div class="info">
				        	<img src="<?php echo base_url(); ?>assets/img/_p1.png" id="box_img_avance_1" alt="" width="145" height="147">
				        	<div class="texto">
					        	<div class="titulo" style="text-transform: uppercase">módulo 1</div>
					        	<div class="contenido hidden" id="box_contenido_avance_1">¡Lograste culminar el primer módulo satisfactoriamente! ¡Este es el primer gran paso, por ello te premiamos con 01 par de guantes de cuero para que estés siempre protegido en obra.
<br/>¡Sigue así, Maestro! 
</div>
				        	</div>
			        	</div>
		        	</div>
		        	
		        	<div class="box_avance disable" id="box_avance_2">
			        	<div class="info">
				        	<img src="<?php echo base_url(); ?>assets/img/_p2.png" id="box_img_avance_2" alt="" width="145" height="147">
				        	<div class="texto">
					        	<div class="titulo" style="text-transform: uppercase">módulo 2</div>
					        	<div class="contenido hidden" id="box_contenido_avance_2">¡Seguimos avanzando, muy bien! Reconocemos tu esfuerzo y te premiamos por terminar los cursos del módulo 2 con una espátula para que siempre tengas tus herramientas y estés listo para tu jornada laboral<br/>
¡Continua Maestro, sigue aprendiendo y sigue ganando!
</div>
				        	</div>
			        	</div>
		        	</div>
		        	
		        	<div class="box_avance disable" id="box_avance_3">
			        	<div class="info">
				        	<img src="<?php echo base_url(); ?>assets/img/_p3.png" id="box_img_avance_3" alt="" width="145" height="147">
				        	<div class="texto">
					        	<div class="titulo" style="text-transform: uppercase">módulo 3</div>
					        	<div class="contenido hidden" id="box_contenido_avance_3">¡Excelente Maestro! Lograste culminar los 6 cursos. Siéntete orgulloso porque este ha sido un gran avance para ti. Te enviaremos tu  certificado de maestro graduado de Unacem Cantera junto con el resto de premios obtenidos.<br/>¡Felicitaciones nuevamente!</div>
				        	</div>
			        	</div>
		        	</div>
	        	</div>
	        	
	        	
        	</div>
        </div>
        
        
        <hr/>
        <div class="footer_text">* Imagenes referenciales. La entrega de los premios se realizará  al culminar satisfactoriamente el programa. El tiempo referencial de cada entrega es de aproximadamente 30 días después de haber concluido el programa. **Cada módulo consta de 2 cursos de 36 horas de estudio cada uno.</div>
	</div>		
</main>
<script>
	console.log("modulo "+modulo);
	if(Number(modulo)==1){
		 document.getElementById("box_avance_1").classList.remove("disable"); 
		 document.getElementById('box_img_avance_1').src='https://unacemcantera.com.pe/assets/img/p1.png';
		 
		 document.getElementById("box_contenido_avance_1").classList.remove("hidden"); 

	}
	if(Number(modulo)==2){
		document.getElementById("box_avance_1").classList.remove("disable"); 
		 document.getElementById('box_img_avance_1').src='https://unacemcantera.com.pe/assets/img/p1.png';
		 
		 document.getElementById("box_avance_2").classList.remove("disable"); 
		 document.getElementById('box_img_avance_2').src='https://unacemcantera.com.pe/assets/img/p2.png';
		 
		 document.getElementById("box_contenido_avance_1").classList.remove("hidden");
		 document.getElementById("box_contenido_avance_2").classList.remove("hidden");
	}
	if(Number(modulo)==3){
		document.getElementById("box_avance_1").classList.remove("disable"); 
		 document.getElementById('box_img_avance_1').src='https://unacemcantera.com.pe/assets/img/p1.png';
		 
		 document.getElementById("box_avance_2").classList.remove("disable"); 
		 document.getElementById('box_img_avance_2').src='https://unacemcantera.com.pe/assets/img/p2.png';
		 
		 document.getElementById("box_avance_3").classList.remove("disable"); 
		 document.getElementById('box_img_avance_3').src='https://unacemcantera.com.pe/assets/img/p3.png';
		 
		 document.getElementById("box_contenido_avance_1").classList.remove("hidden");
		 document.getElementById("box_contenido_avance_2").classList.remove("hidden");
		 document.getElementById("box_contenido_avance_3").classList.remove("hidden");

	}
</script>