<script>
	var nombres = sessionStorage.getItem('nombres');
	var id = sessionStorage.getItem('id');
	console.log("nombres "+nombres);
	if(nombres!=null && nombres!=undefined){
		document.location.href = "<?php echo base_url(); ?>alumnos/ingreso/";
	}
</script>
<link rel="stylesheet" href="<?php echo base_url(); ?>assets/css/registro.css">
<main id="main" class="mb-3">
			<div class="container">
			<div class="row">
				<div class="col-lg-12 col-md-12">
							<h1>INGRESO DE ALUMNOS</h1>
				</div>
			</div>
			
			
			<div class="row">
				<div class="col-lg-12 col-md-12 login_content">
					<div class="control">
						<label>Código de alumno</label>
						<input id="codigo" name="codigo" type="text"/>
					</div>
				</div>

			</div>

			<div class="row">
				<div class="col-lg-12 col-md-12 login_content">
					<div class="control">
					<label>Clave</label>
					<input id="password" name="password" type="password"/>
					<div style="color: white; margin-top: 10px; font-size: 13px;font-family: 'silkamedium';" id="error"></div>
					</div>
				</div>
				
			</div>

			<div class="row">
				<div class="col-lg-12 col-md-12 mt-auto d-flex justify-items-center">
						  <input type="submit" class="btn_cantera blanco" title="ENTRAR"value="ENTRAR" onclick="onLoginAlumno();">
					
						  
				</div>
			</div>
			
			<script>
				function onLoginAlumno(){
					var params = {};
					params.codigo = $("#codigo").val();
					params.password = $("#password").val();
					$.post( "<?php echo base_url(); ?>user/validate_alumno",params, function( response ) {
					  	if(response.status=="OK"){
						  	sessionStorage.setItem('nombres', response.data.nombres);
						  	sessionStorage.setItem('id', response.data.id);
						  	sessionStorage.setItem('modulo', response.data.modulo);
						  	sessionStorage.setItem('apellido_paterno', response.data.apellido_paterno);
						  	sessionStorage.setItem('apellido_materno', response.data.apellido_materno);
						  	
						  	document.location.href = "<?php echo base_url(); ?>alumnos/ingreso/";
					  	}else{
						  	$("#error").html("Alumno no encontrado");
						  	setTimeout(()=>{
							  	$("#error").html("");
						  	}, 3000);
					  	}
					},'json');
				}
			</script>
			
			<div class="row">
				<div class="col-lg-12  d-flex justify-items-center" style="font-family: 'silkamedium';">
					<?php if(isset($message_error) && $message_error){
				          echo '<div class="alert alert-error">';
				            echo 'Usuario o contraseña errada.';
				          echo '</div>';
						  } ?>
				</div>
			</div>
			
			
			<!--<div class="row">
				<div class="col-lg-12 col-md-12 mt-auto d-flex justify-items-center">
					<span class="login_footer_link">Si todavía no eres alumno, <a style="color: white; text-decoration: none;" href="<?php echo base_url(); ?>registro">“entérate y POSTULA”</a></span>
			</div>-->
		</div>
		</main>