
<style>
	#btn_siguiente{
		display: none;
	}
	#btn_siguiente_video{
		display: none;
	}
	@media (max-width: 767px) {
		#btn_siguiente_video{
			display: block;
		}
		iframe{
			display: none;
		}
		.video-turorial{
			display: none;
		}
	}
</style>
<main id="main" class="mb-3">
			<div class="container">
			<div class="row">
				<div class="col-lg-5 col-md-12">
					<div class="box_capacitaciones">
						<div class="title"><?php echo $titulo;?></div>
						<?php echo $descripcion; ?>
						
					</div>
				</div>
				<div class="col-lg-7 col-md-12">

					<div class="curso_video">
						
						
						<?php if($slug=="como_mejorar"){ ?>
							<iframe width="560" height="315" src="<?php echo base_url(); ?>curso3" frameborder="0"></iframe>
							<a class="video-turorial" href="<?php echo base_url(); ?>curso3">
								<div class="fullscreen"></div>
							</a>

						<?php } ?>
						<?php if($slug=="riesgos"){ ?>
							<iframe width="560" height="315" src="<?php echo base_url(); ?>curso4" frameborder="0"></iframe>
							<a class="video-turorial" href="<?php echo base_url(); ?>curso4">
								<div class="fullscreen"></div>
							</a>

						<?php } ?>
						
						<?php if($slug=="calculo"){ ?>
							<a href="<?php echo base_url(); ?>files/tema_2.pdf" target="_blank" onclick="return showSiguiente();">
								<img src="<?php echo base_url(); ?>assets/img/pdf_image.png" style="width: 100%;"/>
							</a> 
							<script type='text/javascript'>
							    function showSiguiente() {
							        $("#btn_siguiente").show();
							    }
							</script>
						<?php } ?>
						
						<?php if($slug=="normativas"){ ?>
							<video id="video" controls>
							  <source src="<?php echo base_url(); ?>files/video_normativas.mp4" type="video/mp4">
							  Your browser does not support the video tag.
							</video>
							<script type='text/javascript'>
							    document.getElementById('video').addEventListener('ended',showSiguiente,false);
							    function showSiguiente() {
							        $("#btn_siguiente").show();
							    }
							</script>
						<?php } ?>
						
						
						<?php if($certificado_directo){ ?>
							
							<?php if($slug=="como_mejorar"){ ?>
						<a id="btn_siguiente_video" class="btn_cantera" style="width:180px; margin-bottom: 50px;" href="<?php echo base_url(); ?>curso3">SIGUIENTE</a><?php } ?>
						
						<?php if($slug=="riesgos"){ ?>
						<a id="btn_siguiente_video" class="btn_cantera" style="width:180px; margin-bottom: 50px;" href="<?php echo base_url(); ?>curso4">SIGUIENTE</a><?php } ?>
						
						
						
						<?php }else{ ?>
						<a id="btn_siguiente" class="btn_cantera" style="width:280px; margin-bottom: 50px;" href="<?php echo base_url(); ?>capacitaciones/quiz/<?php echo $slug; ?>">PRUEBA TU CONOCIMIENTO</a>
						
						<?php } ?>
						
					</div>
				</div>
			</div>
		</main>

