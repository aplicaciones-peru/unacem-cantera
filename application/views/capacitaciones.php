<?php
	$red = "";
	$flecha = "flecha-larga-derecha.svg";
	$flecha_small = "flecha-derecha.svg";
	if($tipo=="gris_claro"){
		$red="_red";
		$flecha = "fecha-larga-derecha_black.svg";
		$flecha_small = "flecha-derecha-black.svg";
	}
?>
<style>
	.pop_gracias{
		width: 80%;
		background-color: #E9E9E9;
		margin: 0 auto;
		max-width: 400px;
		color: #E52822;
		padding: 20px;
		font-family: 'silkabold';
	}
	.border_red{
		border-width: 2px;
		padding: 20px;
		padding-top: 100px;
		padding-bottom: 100px;
		border-style: solid;
		border-color: #000;
		color: #E52822;
		font-size: 22px;
		line-height: 25px;
		display: flex;
		justify-content: center;
		align-items: center;
		text-align: center;
	}
</style>
<main id="main" class="mb-3">
			<div class="container">
			<div class="row">
				<div class="col-lg-5 col-md-12">
					<div class="box_capacitaciones">
						<div class="arrow_content">
								<div class="arrow">
									<img class="arrow-larga-derecha" src="<?php echo base_url(); ?>assets/img/<?php echo $flecha; ?>" alt="fecha-larga-derecha" />
							 </div>
						</div>
						<!--En Unacem creemos que es importante fomentar el desarrollo de capacidades para construir una industria más formal y segura. Sabemos que los maestros de obra cumplen un rol importante y, por ello requieren de oportunidades y herramientas necesarias para seguir aprendiendo. Por eso, desarrollamos capacitaciones de libre acceso para apoyarte a mejorar tus técnicas de construcción y brindarte información relevante para llevar a cabo tus proyectos. Descúbrelas aquí. -->
						<div class="title">CAPACITACIONES</div>
					</div>
				</div>
				<div class="col-lg-7 col-md-12">
					<div class="row">
						<div class="col-lg-12 col-md-12">
							<p class="top_text">En Unacem creemos que es importante fomentar el desarrollo de capacidades para construir una industria más formal y segura. Sabemos que los maestros de obra cumplen un rol importante y, por ello requieren de oportunidades y herramientas necesarias para seguir aprendiendo. Por eso, desarrollamos capacitaciones de libre acceso para apoyarte a mejorar tus técnicas de construcción y brindarte información relevante para llevar a cabo tus proyectos. Descúbrelas aquí. </p>
						</div>
					</div>
					<div class="row">
						<div class="col-lg-4 col-md-4">
							<hr>
							<div class="item">
								<div class="numero">
									<img class="numero_uno" src="<?php echo base_url(); ?>assets/img/n1<?php echo $red; ?>.svg" alt="n1" />
									<hr>
								</div>
								<a class="contenido" href="<?php echo base_url(); ?>capacitaciones/normativas">
									Normativa vigente para la reanudación de actividades ...	
								</a>
								<div class="arrow_next">
									<img src="<?php echo base_url(); ?>assets/img/<?php echo $flecha_small; ?>" alt="flecha-derecha" />
								</div>
							</div>
						</div>
						<div class="col-lg-4 col-md-4">
							<hr>
							<div class="item">
								<div class="numero">
									<img src="<?php echo base_url(); ?>assets/img/n2<?php echo $red; ?>.svg" alt="n1" />
									<hr>
								</div>
								<a class="contenido" href="<?php echo base_url(); ?>capacitaciones/calculo">
									ELABORACIÓN DE PRESUPUESTOS DE OBRAS DE CONSTRUCCIÓN

								</a>
								<div class="arrow_next">
									<img src="<?php echo base_url(); ?>assets/img/<?php echo $flecha_small; ?>" alt="flecha-derecha" />
								</div>
							</div>
						</div>
						
						<div class="col-lg-4 col-md-4">
							<hr>
							<div class="item">
								<div class="numero">
									<img src="<?php echo base_url(); ?>assets/img/n3<?php echo $red; ?>.svg" alt="n1" />
									<hr>
								</div>
								<a class="contenido" href="<?php echo base_url(); ?>capacitaciones/como_mejorar">
									CÓMO MEJORAR LA PRODUCTIVIDAD EN LA CONSTRUCCIÓN

								</a>
								<div class="arrow_next">
									<img src="<?php echo base_url(); ?>assets/img/<?php echo $flecha_small; ?>" alt="flecha-derecha" />
								</div>
							</div>
						</div>
					</div>
					
					


					<div class="puntos"></div>
					<div class="row">
						<div class="col-lg-4 col-md-4">
							<hr>
							<div class="item">
								<div class="numero">
									<img src="<?php echo base_url(); ?>assets/img/n4<?php echo $red; ?>.svg" alt="n1" />
									<hr>
								</div>
								<a class="contenido" href="<?php echo base_url(); ?>capacitaciones/riesgos">
									LOS SEIS PRINCIPALES RIESGOS DE CONSTRUCCIÓN Y COMO REDUCIRLOS
								</a>
								<div class="arrow_next">
									<img src="<?php echo base_url(); ?>assets/img/<?php echo $flecha_small; ?>" alt="flecha-derecha" />
								</div>
							</div>
							
						</div>
						<!--
						<div class="col-lg-4 col-md-4" style="opacity: 0.3">
							<hr>
							<div class="item">
								<div class="numero">
									<img src="<?php echo base_url(); ?>assets/img/n4<?php echo $red; ?>.svg" alt="n1" />
									<hr>
								</div>
								<?php
									//capacitaciones/tendencias
								?>
								<a class="contenido" href="#">
									TENDENCIAS TECNOLOGICAS PARA CONSTRUIR MEJOR.
								</a>
								<div class="arrow_next">
									<img src="<?php echo base_url(); ?>assets/img/<?php echo $flecha_small; ?>" alt="flecha-derecha" />
								</div>
							</div>
						</div>
						-->
					</div>
				</div>
			</div>
		</main>