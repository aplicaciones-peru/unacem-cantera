<div class="container">
    <div class="row">
      <div class="col-lg-3">
        <h2 class="my-4">Construyendo Maestros</h2>
        <h3 class="my-4">Programa Profesional</h3>
        <div>
	        <a href="#">Contáctanos</a>
	        <i class="fa fa-whatsapp" aria-hidden="true"></i>
        </div>
        
        <div>
	         <a href="#">Visitanos</a>
	        <i class="fa fa-youtube-play" aria-hidden="true"></i>
        </div>
        <br/>
      </div>
      <div class="col-lg-9">
        
        <div class="row" style="padding: 20px; display: block;">
			<p>
				Aquí va el contenido “¿De qué se trata?”
			</p>
			<p>
				<ul>
					<li><a href="<?php echo base_url(); ?>alumnos/postula">Postula aquí</a> </li>
				
					<li><a href="<?php echo base_url(); ?>alumnos/ingreso">Ingreso alumno</a></li>
				
					<li><a href="<?php echo base_url(); ?>alumnos/graduados">Conoce a nuestros graduados</a></li>
				</ul>


			</p>

        </div>
      </div>
    </div>
  </div>
 </div>
  