  <?php
	$red = "";
	$flecha = "flecha-larga-derecha.svg";
	$flecha_small = "flecha-derecha.svg";
	if($tipo=="gris_claro"){
		$red="_red";
		$flecha = "fecha-larga-derecha_black.svg";
		$flecha_small = "flecha-derecha-black.svg";
	}
?>
<style>
	.pop_gracias{
		width: 80%;
		background-color: #E9E9E9;
		margin: 0 auto;
		max-width: 400px;
		color: #E52822;
		padding: 20px;
		font-family: 'silkabold';
	}
	.border_red{
		border-width: 2px;
		padding: 20px;
		padding-top: 0px;
		padding-bottom: 0px;
		border-style: solid;
		border-color: #000;
		color: #E52822;
		font-size: 22px;
		line-height: 25px;
		display: flex;
		justify-content: center;
		align-items: center;
		text-align: center;
	}
	label{
	  color:white;
	  display: block;
	  font-family: 'silkamedium';
	  margin-top: 10px;
	  padding-bottom: 5px;
	  margin-bottom: 0px;
	  font-size: 14px;
	  line-height: 17px;
	  color:black;
	  background-color: transparent;
	  width: 100%;
	}
	input{
	  background-color: transparent;
	  border-color: black;
	  border-width: 2px;
	  border-style: solid;
	  text-shadow: none;
	  padding: 10px;
	  padding-top: 3px;
	  padding-bottom: 3px;
	  color:black;
	  width: 280px;
	  font-size: 15px;
	  font-family: 'silkamedium';
	}
	.pop{
		display: flex;
		flex-direction: column;
	}
	.pop .control{
		display: flex;
	    justify-content: flex-start;
	    flex-direction: column;
	    text-align: left;
	}
	.pop .control select{
		font-size: 15px;
		font-family: 'silkamedium';
		height: 35px;
		width: 280px;
		padding-left: 5px;
	}
	.titulo_pop{
		width: 100%;
		color: #E52822;
		text-align: center;
		margin-top: 15px;
		margin-bottom: 10px;
		font-family: 'silkabold';
	}
	#postula{
		position: relative;
	}
	#postula .mfp-close{
		right: -20px;
		top:-20px;
		background-color: black !important;
		opacity: 1 !important;
		border-radius: 20px;
		width: 40px;
		height: 40px;
		color: white;
		font-size: 35px;
		display: flex;
		justify-content: center;
		align-items: center;
		font-family: 'silkabold';
		/*font-family: 'silkabold';*/
	}
	.btn_postula{
		margin-top: 20px;
		margin-bottom: 0px;
		border-color: #E52822;
		color: #E52822;
		font-size: 15px;
	}
	.error{
		color: #E52822;
		font-size: 13px;
		line-height: 15px;
		margin-top: 5px;
		margin-bottom: 10px;
	}
	.btn_postula:hover{
		background-color: #E52822 !important;
	}
	.pop_postula{
		width: 90%;
		background-color: #E9E9E9;
		margin: 0 auto;
		max-width: 700px;
		color: #E52822;
		padding: 20px;
		font-family: 'silkamedium';
	}
	#gracias{
		padding-top: 100px;
		padding-bottom: 100px;
		text-transform: uppercase;
		font-family: 'silkabold';
		display: none;
	}
	#ocupacion_otros_container{
		display: none;
	}
	@media (max-width: 767px){
		.pop .control select{
			width: 100%;
			margin: 0 auto;
		}
		input{
			width: 100%;
			margin: 0 auto;
			font-size: 13px;
		}
		.pop .control select{
			font-size: 13px;
		}
		label{
			font-size: 12px;
			line-height: 14px;	
		}
		.titulo_pop{
			font-size: 20px;
			margin-bottom: 0px;
		}
		
	}
</style>
<main id="main" class="mb-3">
			<div class="container">
			<div class="row">
				<div class="col-lg-5 col-md-12">
					<div class="box_capacitaciones">
						<div class="arrow_content">
								<div class="arrow">
									<img class="arrow-larga-derecha" src="<?php echo base_url(); ?>assets/img/<?php echo $flecha; ?>" alt="fecha-larga-derecha" />
							 </div>
						</div>
						<div class="title">Programa Profesional</div>
					</div>
				</div>
				<div class="col-lg-7 col-md-12">
					<div class="row">
						<div class="col-lg-12 col-md-12">
							<p class="top_text">Con el objetivo de contribuir en el desarrollo profesional de los trabajadores de construcción
e impulsar la formalización del proceso constructivo en el país, hemos desarrollado un
programa virtual de formación profesional y certificación gratuita, “Unacem Cantera”, en
alianza con el Instituto Capeco.<br/>
El programa consta de <a href="<?php echo base_url(); ?>constructores/cursos"><b>6 cursos especializados</b></a> que complementarán la experiencia de cada
participante con conocimiento teórico, lo que les permitirá profesionalizar su labor y mejorar
el resultado final de sus obras.<br/>
Si estás interesado en iniciar tu formación profesional postula 
<a href="#postula" class="btn-postula"><b>AQUÍ.</b></a>

						
								
							    
							    <div id="postula" class="zoom-anim-dialog mfp-hide pop_postula">
								    <div class="btn_close">
									    
								    </div>
								    <div class="border_red" id="gracias">
								    	¡Gracias por postular!
								    </div>
								    <div class="border_red pop" id="form">

										<div class="titulo_pop">
										Postula por la Beca
										</div>
									    <script>
										    var timer;
										    function emailIsValid (email) {
											  return /^[^\s@]+@[^\s@]+\.[^\s@]+$/.test(email)
											}
											
										    function onShowError(error){
											    $("#error").html(error);
											    window.clearTimeout(timer);
											    timer = setTimeout(()=>{
												    $("#error").html("");
											    }, 3000);
										    }
										    function onPressPostula(){	  	
											    var params = {};
											    params.nombres = $("#nombres").val();
											    params.apellidos = $("#apellidos").val();
											    params.dni = $("#dni").val();
											    params.email = $("#email").val();
											    params.celular = $("#celular").val();
											    params.telefono = $("#telefono").val();
											    params.ocupacion = $("#ocupacion").val();
											    params.ocupacion_otros = $("#ocupacion_otros").val();
											    params.experiencia = $("#experiencia").val();
											    
											    console.log("params.ocupacion_otros "+params.ocupacion_otros)
											    
											    if(params.nombres==""){
												    onShowError("Por favor ingrese sus nombres.");
												    return
											    }
											    if(params.apellidos==""){
												    onShowError("Por favor ingrese sus apellidos.");
												    return
											    }
											    if(params.dni==""){
												    onShowError("Por favor ingrese su DNI.");
												    return
											    }
											    if(params.email==""){
												    onShowError("Por favor ingrese su email.");
												    return
											    }
											    if(!emailIsValid(params.email)){
												    onShowError(" Por favor ingrese un email válido.");
												    return
											    }
											    if(params.celular==""){
												    onShowError("Por favor ingrese su celular.");
												    return
											    }
											    if(params.telefono==""){
												    onShowError("Por favor ingrese su teléfono.");
												    return
											    }
											    
											    if(params.ocupacion==null){
												    onShowError("Por favor ingrese su ocupación.");
												    return
											    }
											    if(params.ocupacion=="Otros"){
												    if(params.ocupacion_otros==""){
												    	onShowError("Por favor especifique su profesión/ocupación.");
													    return
											    	}
											    }
											    if(params.experiencia==null){
												    onShowError("Por favor ingrese su experiencia.");
												    return
											    }
											    $.post( "<?php echo base_url(); ?>postula",params, function( response ) {
												  	if(response.status=="OK"){
													  	
													  	$("#nombres").val('');
													    $("#apellidos").val('');
													    $("#dni").val('');
													    $("#email").val('');
													    $("#celular").val('');
													    $("#telefono").val('');
													    $("#ocupacion").val('');
													    $("#ocupacion_otros").val('');
													    $("#experiencia").val('');
											    
													  	$("#form").hide();
													  	$("#gracias").show();
												  	}else{
													  	onShowError(response.message);
												  	}
												},'json');
												
										    }
									    </script>
										<div class="row">
											<div class="col-md-6">
												<div class="control">
													<label>Nombres*</label>
													<input class="input-lg" placeholder="Nombres" name="nombres" id="nombres" value="" type="text" required="">
												</div>
												<div class="control">
													<label>Apellidos*</label>
													<input class="input-lg" placeholder="Apellidos" name="apellidos" id="apellidos" value="" type="text" required="">
												</div>
												<div class="control">
													<label>DNI*</label>
													<input class="input-lg" maxlength="8" placeholder="DNI" name="dni" id="dni" value="" type="text" required="">
												</div>
												<div class="control">
													<label>Email*</label>
													<input class="input-lg" placeholder="Email" name="email" id="email" value="" type="email" required="">
												</div>
											</div>
											<div class="col-md-6">
												<div class="control">
													<label>Celular*</label>
													<input class="input-lg" maxlength="9" placeholder="Celular" name="celular" id="celular" value="" type="text" required="">
												</div>
												<div class="control">
													<label>Teléfono*</label>
													<input class="input-lg" maxlength="12" placeholder="Teléfono" name="telefono" id="telefono" value="" type="text" required="">
												</div>
												<div class="control">
													<label>¿Cuál es su profesión/ocupación principal actualmente?*</label>
													<select name="ocupacion" id="ocupacion">
														<option disabled selected value>Seleccione</option>
														<option value="Albañil/constructor">Albañil/constructor</option>
														<option value="Maestro/constructor">Maestro/constructor</option>
														<option value="Operario">Operario</option>
														<option value="Otros">Otros</option>
													</select>
												</div>
												<div class="control" id="ocupacion_otros_container">
													<label>Especifique su profesión/ocupación</label>
													<input class="input-lg" maxlength="12" placeholder="Profesión/Ocupación" name="ocupacion_otros" id="ocupacion_otros" value="" type="text">
												</div>
												<div class="control">
													<label>¿Cuántos años de experiencia tiene usted, en el rubro de construcción?*</label>
													<select name="experiencia" id="experiencia">
														<option disabled selected value>Seleccione</option>
														<option value="Menos de 5 años">Menos de 5 años</option>
														<option value="De 5 a 9 años">De 5 a 9 años</option>
														<option value="De 10 a 15 años">De 10 a 15 años</option>
														<option value="De 16 a 20 años">De 16 a 20 años</option>
														<option value="Más de 20 años">Más de 20 años</option>
													</select>
												</div>
											</div>
										</div>
										
										
										
										<button class="btn_cantera btn_postula" onclick="onPressPostula();">
											POSTULA
										</button>
										<div class="error" id="error"></div>

									</div>
								</div>

						
						
						
						</div>
					</div>
					<div class="row">
						<!--<div class="col-lg-4 col-md-4">
							<hr>
							<div class="item">
								<div class="numero">
									<img class="numero_uno" src="<?php echo base_url(); ?>assets/img/n1<?php echo $red; ?>.svg" alt="n1" />
									<hr>
								</div>
								<a class="contenido" href="<?php echo base_url(); ?>constructores/dequesetrata">
									¿De qué se trata?

								</a>
								<div class="arrow_next">
									<img src="<?php echo base_url(); ?>assets/img/<?php echo $flecha_small; ?>" alt="flecha-derecha" />
								</div>
							</div>
						</div>
						<div class="col-lg-4 col-md-4">
							<hr>
							<div class="item">
								<div class="numero">
									<img src="<?php echo base_url(); ?>assets/img/n2<?php echo $red; ?>.svg" alt="n1" />
									<hr>
								</div>
								<a class="contenido" href="#">
									Postula aquí

								</a>
								<div class="arrow_next">
									<img src="<?php echo base_url(); ?>assets/img/<?php echo $flecha_small; ?>" alt="flecha-derecha" />
								</div>
							</div>
						</div>-->
						<div class="col-lg-4 col-md-4">
							<hr>
							<div class="item">
								<div class="numero">
									<img class="numero_uno" src="<?php echo base_url(); ?>assets/img/n1<?php echo $red; ?>.svg" alt="n1" />
									<hr>
								</div>
								<a class="contenido" href="<?php echo base_url(); ?>login_alumno">
									Ingreso alumno

								</a>
								<div class="arrow_next">
									<img src="<?php echo base_url(); ?>assets/img/<?php echo $flecha_small; ?>" alt="flecha-derecha" />
								</div>
							</div>
						</div>
						<!--
						<div class="col-lg-4 col-md-4">
							<hr>
							<div class="item">
								<div class="numero">
									<img src="<?php echo base_url(); ?>assets/img/n2<?php echo $red; ?>.svg" alt="n1" />
									<hr>
								</div>
								<a class="contenido" href="<?php echo base_url(); ?>alumnos/graduados">
									Conoce a nuestros graduados

								</a>
								<div class="arrow_next">
									<img src="<?php echo base_url(); ?>assets/img/<?php echo $flecha_small; ?>" alt="flecha-derecha" />
								</div>
							</div>
						</div>
						-->
					</div>
					<!--<div class="puntos"></div>
					<div class="row">
						<div class="col-lg-4 col-md-4">
							<hr>
							<div class="item">
								<div class="numero">
									<img src="<?php echo base_url(); ?>assets/img/n4<?php echo $red; ?>.svg" alt="n1" />
									<hr>
								</div>
								<a class="contenido" href="<?php echo base_url(); ?>alumnos/graduados">
									Conoce a nuestros graduados

								</a>
								<div class="arrow_next">
									<img src="<?php echo base_url(); ?>assets/img/<?php echo $flecha_small; ?>" alt="flecha-derecha" />
								</div>
							</div>
						</div>
					</div>-->
				</div>
			</div>
		</main>