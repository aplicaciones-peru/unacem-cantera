<link rel="stylesheet" href="<?php echo base_url(); ?>assets/css/registro.css">
<main id="main" class="mb-3">
			<div class="container">
			<div class="row">
				<div class="col-lg-12 col-md-12">
							<h1>INGRESO</h1>
				</div>
			</div>
			
			<?php
				echo form_open('/user/validate_credentials');
			?>
			
			<div class="row">
				<div class="col-lg-12 col-md-12 login_content">
					<div class="control">
						<label>DNI</label>
						<input id="user_name" name="user_name" type="text"/>
						<?php 
							if(isset($_GET["r"])){ ?>
							<input type="hidden" value="<?php echo $_GET["r"]?>" name="r"/>
						<?php } ?>
					</div>
				</div>

			</div>

			<div class="row">
				<div class="col-lg-12 col-md-12 login_content">
					<div class="control">
					<label>Clave</label>
					<input id="password" name="password" type="password"/>
					</div>
				</div>
			</div>

			<div class="row">
				<div class="col-lg-12 col-md-12 mt-auto d-flex justify-items-center">
						  <input type="submit" class="btn_cantera blanco" title="ENTRAR"value="ENTRAR">
					
						  
				</div>
			</div>
			<div class="row">
				<div class="col-lg-12  d-flex justify-items-center" style="font-family: 'silkamedium';">
					<?php if(isset($message_error) && $message_error){
				          echo '<div class="alert alert-error">';
				            echo 'Usuario o contraseña errada.';
				          echo '</div>';
						  } ?>
				</div>
			</div>
			
			<?php	
				echo form_close();
			?>
			<div class="row">
				<div class="col-lg-12 col-md-12 mt-auto d-flex justify-items-center">
					<span class="login_footer_link">Si todavía no estas registrado, <a style="color: white; text-decoration: none;" href="<?php echo base_url(); ?>registro">“Regístrate aquí”</a></span>
			</div>
		</div>
		</main>