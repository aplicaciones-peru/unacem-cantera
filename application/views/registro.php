
<link rel="stylesheet" href="<?php echo base_url(); ?>assets/css/registro.css">

<main id="main" class="mb-3">
			<div class="container">
			<div class="row">
				<div class="col-lg-12 col-md-12">
							<h1 style="text-transform: uppercase">Regístrate</h1>
				</div>
			</div>
			<form class="needs-validation" action="<?php echo base_url(); ?>registrar" method="post" novalidate>
			<div class="row">
				<div class="col-lg-6 col-md-6 lcontent">
					<div class="control">
						<label>Nombre</label>
						<input class="input-lg" placeholder="" name="nombre" id="nombre"
										value="" type="text" required>
						<div class="invalid-feedback">
							Por favor ingrese su nombre.
						</div>
					</div>
				</div>
				<div class="col-lg-6 col-md-6 rcontent">
					<div class="control">
					<label>Email</label>
					<input class="input-lg" placeholder="" name="email" id="email" value=""
										type="email" required>
									<div class="invalid-feedback">
										Por favor ingrese su Email.
									</div>
					</div>
				</div>
			</div>

			<div class="row">
				<div class="col-lg-6 col-md-6 lcontent">
					<div class="control">
						<div class="control">
						<label>Apellidos</label>
						<input class="input-lg" placeholder="" name="apellidos" id="apellidos"
										value="" type="text" required>
									<div class="invalid-feedback">
										Por favor ingrese sus apellidos.
									</div>
						</div>
					</div>
				</div>
				<div class="col-lg-6 col-md-6 rcontent">
					<div class="control">
					<label>DNI</label>
					<input class="input-lg" maxlength="8" placeholder="" name="dni" id="dni" value=""
										type="text" required>
									<div class="invalid-feedback">
										Por favor ingrese su DNI.
									</div>
					</div>
				</div>
			</div>

			<div class="row">
				<div class="col-lg-6 col-md-6 lcontent">
					<div class="control">
						<label>Clave</label>
						<input class="input-lg" placeholder="" name="password" id="password" value=""
										type="password" required>
									<div class="invalid-feedback">
										Por favor ingrese su clave.
									</div>
					</div>
				</div>
				<div class="col-lg-6 col-md-6 rcontent">
					<div class="control">
					<label>Confirmar clave</label>
					<input class="input-lg" placeholder="" name="password_validar" id="password_validar" value=""
										type="password" required>
									<div class="invalid-feedback">
										Por favor confirme su clave.
									</div>
					</div>
				</div>
			</div>
			<div class="row">
				<div class="col-lg-6 col-md-6 lcontent">
					<div class="control">
						<label>Celular</label>
						<input class="input-lg" placeholder="" maxlength="9" name="celular" id="celular" value=""
										type="text" required>
									<div class="invalid-feedback">
										Por favor ingrese su celular.
									</div>
					</div>
				</div>
				<div class="col-lg-6 col-md-6 rcontent">
					<div class="control">
						<label>Teléfono</label>
						<input class="input-lg" placeholder="" maxlength="9" name="telefono" id="telefono" value=""
										type="text" required>
									<div class="invalid-feedback">
										Por favor ingrese su teléfono.
									</div>
					</div>
				</div>

			</div>
			<div class="row">
				<div class="col-lg-6 col-md-6 lcontent">
					<div class="control">
						<label>Ciudad</label>
						<input class="input-lg" placeholder="" name="ciudad" id="ciudad" value=""
										type="text" required>
									<div class="invalid-feedback">
										Por favor ingrese su ciudad.
									</div>
					</div>
				</div>
				<div class="col-lg-6 col-md-6 rcontent">
				</div>
			</div>
			<div class="row">
				<div class="col-lg-12 col-md-12 terminos_field">
					<input class="form-check-input" style="width: auto;" type="checkbox" value="" id="chk_terminos" required>
					Debes aceptar los <a href="#popup1" style="color: white; text-decoration: underline;">Términos y Condiciones</a>
					<div class="invalid-feedback">
											Debes aceptar los términos y condiciones
					</div>
				</div>
			</div>
			
			<div class="row">
				<div class="col-lg-12 col-md-12 mt-auto d-flex justify-items-center">
						
						<input type="submit" name="submit" value="ENVIAR"
										class="btn_cantera blanco" style="margin: 0 auto; margin-top: 20px; display: block;">
										
										
				</div>
			</div>
			</form>
			<script>
			(function() {
				'use strict';
				window.addEventListener('load', function() {
					var forms = document.getElementsByClassName('needs-validation');
					var validation = Array.prototype.filter.call(forms, function(form) {
						form.addEventListener('submit', function(event) {
							if (form.checkValidity() === false) {
								event.preventDefault();
								event.stopPropagation();
							}
							form.classList.add('was-validated');
						}, false);
					});
				}, false);
			})();
			</script>
			<div id="popup1" class="overlay">
				<div class="popup">
					<br/>
					<h2 style="color: white; ">T&C PROGRAMA CAPACITACIONES UNACEM</h2>
					<a class="close" href="#">&times;</a>
					<div class="terminos">
						<p>Sobre el uso correcto de los datos entregados por los registrados en la plataforma:
La información personal que el usuario ingresa es totalmente confidencial y se mantiene en forma estricta entre el usuario y UNACEM, conforme a lo establecido en la Ley Nº 29733 “Ley de Datos Personales”, su Reglamento aprobado por D.S.N° 003-2013-JUS y demás normas pertinentes. En ese sentido, resultan aplicables los siguientes aspectos:</p>

<p>1. Toda la información proporcionada por el usuario deberá ser verdadera, exacta y completa. El usuario es el único y exclusivo responsable de la información que brinda mediante el uso del portal de UNACEM y de las consecuencias que genere cualquier inexactitud o falsedad de la información brindada.</p>

<p>2. Los datos personales nombres, teléfono y correo electrónico, son recogidos para el cumplimiento de las finalidades expuestas en este documento y no se harán extensivos para otras finalidades incompatibles con las especificadas.</p>

<p>3. Los datos que se solicitan al usuario son datos imprescindibles para poder ofrecer y gestionar las consultas de forma correcta, siendo adecuados, relevantes y no excesivos en relación con la finalidad para la que se solicitan.</p>

<p>4. Los datos registrados por el usuario tienen que ser veraces, exactos, actualizados, pertinentes y adecuados, de forma que respondan verazmente con su situación actual.</p>

<p>5. De acuerdo a Ley N° 29733 y su Reglamento, los datos personales también podrán ser comunicados a las entidades administrativas, autoridades judiciales y/o policiales, siempre y cuando se encuentre en algún supuesto establecido en la Ley.</p>

<p>6. Los datos personales serán tratados con total confidencialidad. UNACEM, en su calidad de titular del banco de datos personales se compromete a guardar secreto profesional respecto de los mismos y garantizar el deber de guardarlos adoptando todas las medidas de seguridad necesarias.</p>

<p>Sobre la postulación al programa de certificación gratuita:</p>
*Considerará todo lo mencionado en los 6 puntos previos. 
<p>A todo aquel que postule, toda la información proporcionada deberá ser verdadera, exacta y completa, respondiendo las siguientes 3 preguntas:</p>

<p>1.¿Cuál es su profesión/ocupación principal actualmente?</p>

<p>  
<table>
	<tr>
		<td>Maestro Constructor</td>
		<td>Albañil / Constructor</td>
	</tr>
	<tr>
		<td>Operario</td>
		<td>Otros (Esp.)</td>
	</tr>
</table>         
</p>
 	
<p>2.¿Cuántos años de experiencia tiene usted en el rubro construcción?</p>  
<p> 
<table border="2">
	<tr>
		<td>Menos de 5 años </td>
		<td>De 5 a 9 años</td>
		<td>De 10 a 15 años</td>
	</tr>
	<tr>
		<td>De 16 a 20 años</td>
		<td>Más de 20 años</td>
		<td></td>
	</tr>
</table>   
</p>

<p>3.¿En qué tipo de obras tiene más experiencia? (Puede marcar más de una)</p> 
<p>
<table border="2">
	<tr> <td>Construcción Residencial</td></tr>
	<tr> <td>Construcción Comercial </td></tr>
	<tr> <td>Construcción Industrial</td></tr>
	<tr> <td>Construcción de obras públicas</td></tr>
	<tr> <td>Construcción Institucional</td></tr>
</table>  
</p> 
<p>

Según la información enviada, se confirmará y elegirá a los ganadores que formarán parte del programa de capacitación profesional. Los ganadores serán elegidos mes a mes, abriendo vacantes de 100 personas por periodo. Son 800 vacantes en total, por lo que se contará con 8 periodos de estudio desde Setiembre 2020 hasta Abril 2021.</p> 
<p>
Sobre los ganadores del concurso, </p> 
<p>1.Deberán residir en Lima Metropolitana y ser mayores de 18 años.
2.Aceptan formar parte del programa e inscribirse para cursar durante el periodo de 6 meses el programa completo de certificación profesional. 
3.El usuario acepta y entiende que tiene un periodo máximo de 1 mes para culminar cada curso virtual que conforma el programa de certificación profesional y 6 meses para culminar el programa total. A continuación, el detalle de los cursos:</p> 

<p>
<ul>
<li>Curso 1: Lectura de planos</li>
<li>Curso 2: Especificaciones de los materiales de construcción</li>
<li>Curso 3: Dosificación y mezcla del concreto</li>
<li>Curso 4: Sistemas constructivos de albañilería</li>
<li>Curso 5: Acabados de concreto caravista</li>
<li>Curso 6: Acabados húmedos y secos</li>
<ul>
</p>

<p>2.El usuario acepta y entiende que para obtener el certificado debe alcanzar un promedio final aprobatorio mayor a 13 como promedio ponderado final y haber participado en las actividades programadas en el curso. El certificado se emite por un total de 36 horas lectivas por cada curso en alianza con el Instituto Capeco.</p>


<p>Sobre el acompañamiento y seguimiento de avance:</p>
<p>1.Los datos que se solicitan al usuario son datos imprescindibles para poder ofrecer y gestionar de forma correcta el seguimiento de avance en el curso durante el periodo de 6 meses. </p>

Sobre las entregas de Kits de reconocimiento:
<p>
1.Se realizará en Lima Metropolitana, y al entregar el premio el ganador deberá aceptar una foto con su DNI y una foto recibiendo el mismo. 
2.Kit de bienvenida: será entregado una vez haya sido elegido e iniciado el primero curso del programa. 
3.Kit del graduado: será entregado una vez culmine satisfactoriamente del programa de certificación profesional (después de haber cumplido satisfactoriamente las 216 horas de estudio en su totalidad, considerando los 06 cursos del programa).
4.La entrega de los kits podrá demorar algunas semanas por el Estado de Emergencia Nacional</p>

Sobre la certificación del programa:
<p>
1.Se debe considerar que al término satisfactorio del total de los cursos que constituyen el programa de estudio, se entregará una certificación a nombre de Unacem. Esta certificación no deberá ser considerada como un título académico ni de especialización profesional.
2.La certificación entregada por Unacem tiene el objetivo de avalar las horas de estudios de los temas señalados en el T&C y asimismo ser un soporte por el interés y constancia del alumno al haber culminado el programa.</p>

Sobre el papel de los soportes e información brindados por Unacem en obra
<p>
Es sumamente importante considerar que todo proyecto de construcción debe ser supervisado por especialistas y profesionales en construcción que garanticen y validen la seguridad de la obra y para que esta sea trabajada de la manera correcta. Las herramientas, capacitaciones y tips que ofrece Unacem, a través del programa Unacem Cantera tienen el objetivo de ofrecer mayores conocimientos y mejorar lo que en práctica los maestros de obra conocen. Dichos soportes no deberían ser considerados como únicos referentes ya que no representan ni reemplazan a un profesional de construcción ni la supervisión del mismo dentro de una obra.</p>
<p>
Sobre el uso correcto de Whatsapp Business de Capacitaciones Maestros Unacem:
Agradecemos el interés de la comunidad por participar y compartir con nosotros sus sugerencias, comentarios, dudas y preguntas, de forma privada (mediante mensajes al número de Whatsapp Web).
Basándonos en los principios básicos de una sana convivencia y respeto mutuo en una sociedad, establecemos aquí nuestras normas de conducta para esta comunidad:</p>
<p>
1.Usar un lenguaje adecuado y respetuoso, sin atentar contra la integridad de personas y/o empresas. 
2.No está permitido el uso de insultos y/o comentarios ofensivos hacia otras personas. No toleramos la discriminación ya sea por culturas, razas, religiones u orientación sexual. 
3.Por la seguridad de los usuarios se solicita no exponer información privada personal o de terceros (cuentas de correo, dirección o teléfonos); así como tampoco exponer fotografías o información de menores de edad.
4.No enviar mensajes o comentarios repetidos o contenido irrelevante. Estos serán considerados como SPAM y serán eliminados.
5.No hacer publicidad ni promociones de otras marcas, servicios, y/o productos. 
6.Respetar los derechos de terceros, la propiedad intelectual y moral de otros usuarios, marcas y empresas. 
7.No subir fotografías y/o videos obscenos, que comprometan la integridad de cualquier persona y/o empresa, sea miembro o no de la Comunidad de UNACEM. </p>
<p>
Si un usuario tuviera alguna queja, reclamo o inquietud, no deben dudar en comunicarlo mediante un mensaje privado. Les responderemos a la brevedad posible siempre intentando lograr una solución.</p>


					</div>
				</div>
			</div>
		</main>