<!DOCTYPE html>
<html lang="es" class="h-100">
<head>
	
	<meta charset="UTF-8">
	<meta name="viewport" content="width=device-width, initial-scale=1.0">
	<title>UNACEM - CANTERA</title>
	<link rel="icon" type="image/x-icon" href="/favicon.ico">
	<link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.5.2/css/bootstrap.min.css" integrity="sha384-JcKb8q3iqJ61gNV9KGb8thSsNjpSL0n8PARn9HuZOnIxN0hoP+VmmDGMN5t9UJ0Z" crossorigin="anonymous">
	<link rel="stylesheet" href="<?php echo base_url(); ?>assets/css/fonts.css">
	<link rel="stylesheet" href="<?php echo base_url(); ?>assets/css/global.css">
	<link rel="stylesheet" href="<?php echo base_url(); ?>assets/css/general.css">
	<link rel="stylesheet" href="<?php echo base_url(); ?>assets/css/<?php echo $tipo; ?>.css">
	<script type="text/javascript">
		function onBack(){
			var f = window.history.back();	
		}
	</script>
	<!-- Google Tag Manager -->
	<script>(function(w,d,s,l,i){w[l]=w[l]||[];w[l].push({'gtm.start':
	new Date().getTime(),event:'gtm.js'});var f=d.getElementsByTagName(s)[0],
	j=d.createElement(s),dl=l!='dataLayer'?'&l='+l:'';j.async=true;j.src=
	'https://www.googletagmanager.com/gtm.js?id='+i+dl;f.parentNode.insertBefore(j,f);
	})(window,document,'script','dataLayer','GTM-WR3W5TM');</script>
	<!-- End Google Tag Manager -->
</head>
<body class="h-100">
	
	<!-- Google Tag Manager (noscript) -->
<noscript><iframe src="https://www.googletagmanager.com/ns.html?id=GTM-WR3W5TM"
height="0" width="0" style="display:none;visibility:hidden"></iframe></noscript>
<!-- End Google Tag Manager (noscript) -->

	<div class="menu_ico">
	<?php if($tipo=="gris_claro" || $tipo=="blanco"){ ?>
    <img src="<?php echo base_url(); ?>assets/img/menu_black.svg">
    <?php }else{ ?>
    <img src="<?php echo base_url(); ?>assets/img/menu.svg">
    <?php } ?>
  </div>
<div class="menu_mobile_container">
			<div class="close"></div>
			<div class="menu_mobile" id="menu_mobile">
				<ul>
					<li>
						<a data-toggle="collapse"  href="#submenuMovil1" role="button">
							<img class="numero uno" src="<?php echo base_url(); ?>assets/img/n1.svg" /> Programa profesional
						</a>
						<ul class="collapse mt-3 mx-3" id="submenuMovil1" data-parent="#menu_mobile">
							<li><a href="<?php echo base_url(); ?>alumnos/programa_profesional">Información de cursos</a></li>
							<li><a href="<?php echo base_url(); ?>alumnos/ingreso">Ingreso Alumno</a></li>
						</ul>
					</li>
					<li>
						<a data-toggle="collapse" href="#submenuMovil2" role="button">
							<img class="numero" src="<?php echo base_url(); ?>assets/img/n2.svg" /> Capacitaciones</a>

						<ul class="collapse mt-3 mx-3" id="submenuMovil2" data-parent="#menu_mobile">
							
							<li><a href="<?php echo base_url(); ?>capacitaciones/">Información de capacitaciones</a></li>
							
							<li><a href="<?php echo base_url(); ?>capacitaciones/normativas">Protocolos Anticovid</a></li>
							<li><a href="<?php echo base_url(); ?>capacitaciones/calculo">Elaboración de Presupuestos</a></li>
							
							<li><a href="<?php echo base_url(); ?>capacitaciones/como_mejorar">Productividad en Construcción</a></li>
							<li><a href="<?php echo base_url(); ?>capacitaciones/riesgos">Riesgos en la Construcción</a></li>
							<!--
							<li><a href="<?php echo base_url(); ?>capacitaciones/tendencias">Evolución en Construcción</a></li>
							<li><a href="<?php echo base_url(); ?>capacitaciones/riesgos">Riesgos en la Construcción</a></li>-->
						</ul>
					</li>
					<li>
						<a data-toggle="collapse" href="#submenuMovil3" role="button">
							<img class="numero" src="<?php echo base_url(); ?>assets/img/n3.svg" /> Soluciones
							contructivas
						</a>

						<ul class="collapse mt-3 mx-3" id="submenuMovil3" data-parent="#menu_mobile">
							<li><a href="<?php echo base_url(); ?>constructores/soluciones_constructivas">Información de Soluciones
							contructivas</a></li>
							<li><a href="<?php echo base_url(); ?>constructores/calculadora">Calculadora de Materiales de construcción</a></li>
							<li><a href="<?php echo base_url(); ?>constructores/planos">Planos Casas Tipo</a></li>
						</ul>
					</li>
				</ul>
			</div>
		</div>
   <?php
	   $sec="";
	   if(isset($seccion)){
		   $sec=$seccion;
	   }
	?>
 <div id="pagina" class="d-flex flex-column h-100">
		<header id="header">
			<div class="container py-4">
				<div class="row align-items-end justify-content-between">
					<div class="col-md-auto order-md-1">
						<nav id="nav" class="navbar navbar-expand-md navbar-dark p-md-0 text-white align-items-center">
    						<div class="collapse navbar-collapse" id="menu">
								<ul class="navbar-nav align-items-center">
									<li class="nav-item">
										<a class="nav-link <?php if($sec=="programa_profesional")echo "selected"; ?>" href="<?php echo base_url(); ?>alumnos/programa_profesional">Programa profesional</a>
									</li>
									<li class="nav-item">
										<span class="divisor d-none d-md-block">|</span>
									</li>
									<li class="nav-item">
										<a class="nav-link <?php if($sec=="capacitaciones")echo "selected"; ?>" href="<?php echo base_url(); ?>capacitaciones">Capacitaciones</a>
									</li>
									<span class="divisor d-none d-md-block">|</span>
									<li class="nav-item">
										<a class="nav-link <?php if($sec=="soluciones_constructivas")echo "selected"; ?>" href="<?php echo base_url(); ?>constructores/soluciones_constructivas">Soluciones constructivas</a>
									</li>
									<?php  if($this->session->userdata('is_logged_in')){ ?>
									<span class="divisor d-none d-md-block">|</span>
									<li class="nav-item">
										<a class="nav-link" href="<?php echo base_url(); ?>user/logout">Salir</a>
									</li>
									<?php } ?>
								</ul>
							</div>
						</nav>
					</div>
					<div class="col-md-auto order-md-0">
						<?php if($tipo=="gris_claro" || $tipo=="blanco"){ ?>
						<a href="<?php echo base_url(); ?>constructores">
							<img class="logo d-block mx-auto" src="<?php echo base_url(); ?>assets/img/unacem-cantera-rojo.svg" alt="unacem-cantera" width="230"/>
						</a>
						<?php }else{ ?>
						<a href="<?php echo base_url(); ?>constructores">
							<img class="logo d-block mx-auto" src="<?php echo base_url(); ?>assets/img/unacem-cantera.svg" alt="unacem-cantera" width="230"/>
						</a>
						<?php } ?>
					</div>
				</div>
				<hr>
			</div>
		</header>
		<div class="top_space"></div>