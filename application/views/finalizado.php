<?php $ci =& get_instance();?>
<script>
	function enviarEmail(){
		if(!$("#btn_envio_email").hasClass("disabled")){
			var url = "/email/enviar/<?php echo $curso; ?>";
			$("#btn_envio_email").addClass("disabled");
			$.post( url,function( response ) {
				if(response.status=="OK"){
					$("#btn_envio_email").removeClass("disabled");
					alert("Email enviado al correo "+response.email+" con exito.");
				}
			},'json');
		}
	}
</script>
<link rel="stylesheet" href="<?php echo base_url(); ?>assets/css/cuestionario.css">
<main id="main" class="mb-3">
			<div class="container">
			<div class="row">
				<div class="col-lg-12 col-md-12">
					<div class="box_temario">
						<div class="linea_central"></div>
						<h1><?php echo $titulo; ?></h1>
						<p class="contenido">Gracias por haber participado de la capacitación "<?php echo $titulo; ?>" de Unacem Cantera. <br/><br/>
Descarga tu constancia de participación aquí.  <br/><br/>
Asimismo, podrás descargar el contenido completo y/o enviarlo a tu correo para que siempre  puedas acceder a él, incluso sin internet.<br/><br/>
Te invitamos a seguir aprendiendo con nosotros a través de nuestras siguientes capacitaciones.</p>
						<div class="linea_final"></div>
					</div>
				</div>
			</div>
			<div class="row">
				<div class="col-lg-12 col-md-12">
					<div class="botonera">
						<div class="lcontent">
							<a class="btn_cantera" href="<?php echo base_url(); ?>exports/pdf/certificado.php?u=<?php echo $ci->session->userdata('id'); ?>&curso=<?php echo $curso; ?>" target="_blank">DESCARGA TU CONSTANCIA</a>
						</div>
						<div class="rcontent">
							<a class="btn_cantera" target="_blank" href="<?php echo base_url(); ?>files/tema_<?php echo $curso; ?>.pdf">DESCARGA CURSO COMPLETO</a>
							<button class="btn_cantera" id="btn_envio_email" onclick="enviarEmail()">ENVIO AL EMAIL</button>
						</div>
					</div>
					</br>&nbsp;</br>
				</div>
			</div>
		</main>