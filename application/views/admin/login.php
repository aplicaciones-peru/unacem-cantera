<!DOCTYPE html>
<html lang="en-US">
  <head>
    <title>Cantera CMS</title>
    <meta name="viewport" content="width=device-width, user-scalable=no">
    <meta charset="utf-8">
    <link href="<?php echo base_url(); ?>assets/css/admin/global.css" rel="stylesheet" type="text/css">
    <link href="<?php echo base_url(); ?>assets/css/admin/stylesheet.css" rel="stylesheet">

	<style>
		body{
			background-color: #FFF !important;
			font-family: 'TradeGothic LT';
		}
		h2{
			color: #009CDE;
		}
		.form-signin{
			background-color: transparent;
			border: none;
			box-shadow: none;
		}
		input[type="text"]{
			  font-family: 'Open Sans', sans-serif;
		}
		input[type="password"]{
			  font-family: 'Open Sans', sans-serif;
		}
		.btn-primary{
			background-color: #F15D3D !important;
		}
		button.btn, input[type="submit"].btn{
			background-color: #000 !important;
			border: none;
			background-image: none;
			  font-family: 'Open Sans', sans-serif;
			font-weight: 600;
			width: 87%;
		}
		.container{
			text-align: center;
		}
		.login{
			text-align: center;
			padding-top: 20px;
		}
		.login form{
			padding-top: 30px;
		}
		.logo{
			
			margin: 0 auto;
			display: block;
			margin-bottom: 20px;
			margin-top: 20px;
		}
		.btn-registrate{
			margin-top: 10px;
			border: none;
			width: 87% !important;
			background-color: #01A8E1;
			color: white;
			text-shadow:none!important;
			font-weight: 600 !important;
		}
		.btn-registrate:focus{
			background-color: #01A8E1;
			color: white !important;
			font-weight: 600 !important;
		}
		.btn-registrate:hover{
			background-color: #01A8E1;
			color: white;
			font-weight: 600 !important;
		}
		.btn-registrate:active{
			background-color: #01A8E1;
			color: white;
			font-weight: 600 !important;
		}
		.alert{
			padding: 0 !important;

			background-color: transparent;
			border: none !important; 
			box-shadow: none !important;
			color: red;
			font-weight: 500;
		}
	</style>
<script async src="https://www.googletagmanager.com/gtag/js?id=UA-140462687-1"></script>

<script>
  window.dataLayer = window.dataLayer || [];
  function gtag(){dataLayer.push(arguments);}
  gtag('js', new Date());

  gtag('config', 'UA-140462687-1');
</script>
	<link href="https://fonts.googleapis.com/css2?family=Open Sans:wght@300;400;700&family=Abel&display=swap" rel="stylesheet">
	 <link href="https://fonts.googleapis.com/css2?family=Open+Sans:wght@400;600;700;800&display=swap" rel="stylesheet"> 
  </head>
  <body>

		        <img class="logo" src="https://unacemcantera.com.pe/assets/img/unacem-cantera-rojo.svg" width="250" height="50" alt="" style="margin-top: 50px;">
		      <?php
		      $attributes = array('class' => 'form-signin');
		      echo form_open('user/validate_credentials_admin', $attributes);
		      //echo '<h2 class="form-signin-heading">Mobil Contigo</h2>';
		      echo form_input('user_name', '', 'placeholder="Usuario"');
		      echo form_password('password', '', 'placeholder="Contraseña"');
		      if(isset($message_error) && $message_error){
		          echo '<div class="alert alert-error">';
		            echo 'Usuario o contraseña errada!';
		          echo '</div>';
		      }
		      echo form_submit('submit', 'Iniciar sesión', 'class="btn btn-large btn-primary"');
		     // echo '<a href="/registro"><input type="button" value="Registrate" class="btn btn-large btn-registrate"></a>';
		      echo form_close();
		      ?>
		      
 
    <!-- Latest compiled and minified CSS -->
<link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/3.4.1/css/bootstrap.min.css" integrity="sha384-HSMxcRTRxnN+Bdg0JdbxYKrThecOKuH5zCYotlSAcp1+c8xmyTe9GYg1l9a69psu" crossorigin="anonymous">

<!-- Optional theme -->
<link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/3.4.1/css/bootstrap-theme.min.css" integrity="sha384-6pzBo3FDv/PJ8r2KRkGHifhEocL+1X2rVCTTkUfGk7/0pbek5mMa1upzvWbrUbOZ" crossorigin="anonymous">



  </body>
</html>
