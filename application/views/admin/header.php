<?php 	$ci =& get_instance();
		$is_admin = ($ci->session->userdata('perfil')=="Administrador")?true:false;
		$perfil = $ci->session->userdata('perfil');
?>
<!DOCTYPE html>
<html lang="en-US">
<head>
  <title>Unacem Cantera CMS</title>
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1.0">

  	<link href="<?php echo base_url(); ?>assets/css/admin/stylesheet.css?v=1" rel="stylesheet">

  	<link href="<?php echo base_url(); ?>assets/grocery_crud/themes/bootstrap/css/bootstrap/bootstrap.min.css" rel="stylesheet" type="text/css">

  	<script src="<?php echo base_url(); ?>assets/grocery_crud/js/jquery-1.11.1.min.js"></script>
  	
  	<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.12.9/umd/popper.min.js" crossorigin="anonymous"></script>

  	<link href="<?php echo base_url(); ?>assets/css/admin/admin.css" rel="stylesheet" type="text/css">
	<link type="text/css" rel="stylesheet" href="<?php echo base_url(); ?>assets/grocery_crud/themes/bootstrap/css/bootstrap/bootstrap.min.css" />
	
		 <link href="https://fonts.googleapis.com/css2?family=Open+Sans:wght@400;600;700;800&display=swap" rel="stylesheet"> 
		 
	<style>
		.navbar-inverse{
			background-color: #000 !important;
		}
		.pagination>li>a{
			color: #000 !important;
		}
		@media only screen and (max-width: 600px) {
			.fa-expand{
				display: none !important;
			}
			.fa-caret-up{
				display: none !important;
			}
			.minimize-maximize-container{
				display: none !important;
			}
			.header-tools{
				display: none !important;
			}
			.table-label{
				background-color: white !important;
				color: black;
			}
		}

	</style>

</head>
<body>

	<nav class="navbar navbar-inverse navbar-fixed-top">
      <div class="container">
        <div class="navbar-header">

          <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#navbar" aria-expanded="false" aria-controls="navbar">
            <span class="sr-only">Toggle navigation</span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
          </button>
          <a class="navbar-brand" href="#">

	          <img width="120px" src="https://unacemcantera.com.pe/assets/img/unacem-cantera.svg" style=" margin-top: 6px;"/>

          </a>
		</div>

        <div id="navbar" class="collapse navbar-collapse">
          <ul class="nav navbar-nav">


		  	
		  <?php if($is_admin){ 
		        $segment = $this->uri->segment(2);
	        ?>
	        <li class="<?php echo ($segment=='usuarios')?'active':''; ?>">
				<a href="<?php echo base_url(); ?>admin/usuarios">Usuarios</a>
			</li>
			<?php } ?>
	        
	        
	        <?php if($is_admin){ 
		        $segment = $this->uri->segment(2);
	        ?>
	        <li class="<?php echo ($segment=='cursos')?'active':''; ?>">
				<a href="<?php echo base_url(); ?>admin/cursos">Cursos</a>
			</li>
			<?php } ?>
			
			<?php if($is_admin){ 
		        $segment = $this->uri->segment(2);
	        ?>
	        <li class="<?php echo ($segment=='alumnos')?'active':''; ?>">
				<a href="<?php echo base_url(); ?>admin/alumnos">Alumnos</a>
			</li>
			<?php } ?>
			
			
	        <?php if($is_admin){ 
		        $segment = $this->uri->segment(2);
	        ?>
	        <li class="<?php echo ($segment=='postula')?'active':''; ?>">
				<a href="<?php echo base_url(); ?>admin/postula">Postula</a>
			</li>
			<?php } ?>
	     

            <li class="dropdown">
	          <a href="#" class="dropdown-toggle" data-toggle="dropdown">Sistema <b class="caret"></b></a>
	          <ul class="dropdown-menu">


	            <li>
	              <a href="<?php echo base_url(); ?>user/logout_sistema">Salir</a>
	            </li>
	          </ul>
	        </li>
          </ul>
		</div><!--/.nav-collapse -->


      </div>
    </nav>
    
  
