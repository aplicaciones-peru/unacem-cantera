<link rel="stylesheet" href="<?php echo base_url(); ?>assets/css/calculadora.css">
<main id="main" class="mb-3">
	<div class="container">
        <div class="row">
          <div class="col-lg-6 col-md-12 line_right">
            <h1 class="title_big">CALCULADORA DE MATERIALES DE CONSTRUCCIÓN</h1>
            <div class="line_separation"></div>
            <p class="texto_calculadora">Determina de una manera sencilla la cantidad adecuada de cemento, agregados y agua que requerirá la mezcla de concreto para cada uno de tus proyectos. Selecciona el tipo de construcción, ingresa los datos y obtén la proporción adecuada de cada material.<br/><br/>
* Recuerda que, para garantizar la dosificación es importante utilizar materiales de calidad, respetar el proceso constructivo y contar con la supervisión de un ingeniero civil, ya que estos pueden afectar en el resultado final de tu mezcla de concreto.
</p>
          </div>
          <div class="col-lg-6 col-md-12">
            <div class="square_calc_container">
              <div class="selecciona_construccion">
                <h5>SELECCIONA LA CONSTRUCCION A REALIZAR</h5>
                
                <select id="cbo_construccion" onchange="onChangeConstruccion();">
                  <option value="1">CIMIENTOS</option>
                  <option value="2">SOBRECIMIENTOS</option>
                  <option value="3">ZAPATAS PARA COLUMNAS</option>
                  <option value="4">ZAPATAS PARA MUROS</option>
                  <option value="5">MURO DE CONTENCION</option>
                  <option value="6">COLUMNAS</option>
                  <option value="7">PLACAS</option>
                  <option value="8">VIGAS</option>
                </select>
              </div>
              <img id="img1" class="item visible" src="<?php echo base_url(); ?>assets/img/1_CIMIENTOS.png"/>
              <img id="img2" class="item" src="<?php echo base_url(); ?>assets/img/2_SOBRECIMIENTOS.png"/>
              <img id="img3" class="item" src="<?php echo base_url(); ?>assets/img/3_ZAPATAS_PARA_COLUMNAS.png"/>
              <img id="img4" class="item" src="<?php echo base_url(); ?>assets/img/4_ZAPATAS_PARA_MUROS.png"/>
              <img id="img5" class="item" src="<?php echo base_url(); ?>assets/img/5_MUROS.png"/>
              <img id="img6" class="item" src="<?php echo base_url(); ?>assets/img/6_COLUMNAS.png"/>
              <img id="img7" class="item" src="<?php echo base_url(); ?>assets/img/7_VIGAS.png"/>
              <img id="img8" class="item" src="<?php echo base_url(); ?>assets/img/8_PLACAS.png"/>
              <style>
		          .item{
			          display: none;
		          }
		          .item.visible{
			          display: block;
		          }
	          </style>
              <script>
	              var base_url = "<?php echo base_url(); ?>";
	              function onChangeConstruccion(){
		              var selected = Number($("#cbo_construccion").val());
		              for(var i=1;i<=8;i++){
			              $("#img"+i).removeClass("visible");
		              }
			          $("#img"+selected).addClass("visible");
		              $("#btn_siguiente").attr("href", base_url+"maestros/calculadora/"+selected);
	              }
              </script>              
              <a id="btn_siguiente" href="<?php echo base_url(); ?>maestros/calculadora/1" class="btn_cantera">SIGUIENTE</a>
            </div>
          </div>
        </div>
     </div>
</main>