<!doctype html>
<html lang="en">
	<head>
		<meta charset="utf-8">
		<title>UNACEM - CANTERA</title>
		<meta name="viewport" content="width=device-width, initial-scale=1">
		<link rel="icon" type="image/x-icon" href="/favicon.ico">
		<link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.5.2/css/bootstrap.min.css"
			integrity="sha384-JcKb8q3iqJ61gNV9KGb8thSsNjpSL0n8PARn9HuZOnIxN0hoP+VmmDGMN5t9UJ0Z" crossorigin="anonymous">
		<link rel="stylesheet" href="<?php echo base_url(); ?>assets/css/fonts.css">
		<link rel="stylesheet" href="<?php echo base_url(); ?>assets/css/global.css">
		<link rel="stylesheet" href="<?php echo base_url(); ?>assets/css/comun.css">
		<link rel="stylesheet" href="<?php echo base_url(); ?>assets/css/inicio.css">
		<link rel="stylesheet" href="<?php echo base_url(); ?>assets/css/general.css">
		<script type="text/javascript">
		  let vh = window.innerHeight * 0.01 + 'px';
	          document.documentElement.style.setProperty('--vh', vh);
	          window.addEventListener('resize', function () {
	            var vh = window.innerHeight * 0.01 + 'px';
	            document.documentElement.style.setProperty('--vh', vh);
	          });
	  	</script>
	  	<!-- Google Tag Manager -->
	<script>(function(w,d,s,l,i){w[l]=w[l]||[];w[l].push({'gtm.start':
	new Date().getTime(),event:'gtm.js'});var f=d.getElementsByTagName(s)[0],
	j=d.createElement(s),dl=l!='dataLayer'?'&l='+l:'';j.async=true;j.src=
	'https://www.googletagmanager.com/gtm.js?id='+i+dl;f.parentNode.insertBefore(j,f);
	})(window,document,'script','dataLayer','GTM-WR3W5TM');</script>
	<!-- End Google Tag Manager -->
	</head>

	<body>
		<!-- Google Tag Manager (noscript) -->
<noscript><iframe src="https://www.googletagmanager.com/ns.html?id=GTM-WR3W5TM"
height="0" width="0" style="display:none;visibility:hidden"></iframe></noscript>
<!-- End Google Tag Manager (noscript) -->

		<div class="menu_ico">
			<img src="<?php echo base_url(); ?>assets/img/menu.svg">
		</div>
		<div class="menu_mobile_container">
			<div class="close"></div>
			<div class="menu_mobile" id="menu_mobile">
				<ul>
					<li>
						<a data-toggle="collapse"  href="#submenuMovil1" role="button">
							<img class="numero uno" src="<?php echo base_url(); ?>assets/img/n1.svg" /> Programa profesional
						</a>
						<ul class="collapse mt-3 mx-3" id="submenuMovil1" data-parent="#menu_mobile">
							<li><a href="<?php echo base_url(); ?>alumnos/programa_profesional">Información de cursos</a></li>
							<li><a href="<?php echo base_url(); ?>alumnos/ingreso">Ingreso Alumno</a></li>
						</ul>
					</li>
					<li>
						<a data-toggle="collapse" href="#submenuMovil2" role="button">
							<img class="numero" src="<?php echo base_url(); ?>assets/img/n2.svg" /> Capacitaciones</a>

						<ul class="collapse mt-3 mx-3" id="submenuMovil2" data-parent="#menu_mobile">
							<li><a href="<?php echo base_url(); ?>capacitaciones/">Información de capacitaciones</a></li>
							<li><a href="<?php echo base_url(); ?>capacitaciones/normativas">Protocolos Anticovid</a></li>
							<li><a href="<?php echo base_url(); ?>capacitaciones/calculo">Elaboración de Presupuestos</a></li>
							
							<li><a href="<?php echo base_url(); ?>capacitaciones/como_mejorar">Productividad en Construcción</a></li>
							<li><a href="<?php echo base_url(); ?>capacitaciones/riesgos">Riesgos en la Construcción</a></li>
							
							<!--
							<li><a href="<?php echo base_url(); ?>capacitaciones/tendencias">Evolución en Construcción</a></li>
							<li><a href="<?php echo base_url(); ?>capacitaciones/riesgos">Riesgos en la Construcción</a></li>-->
						</ul>
					</li>
					<li>
						<a data-toggle="collapse" href="#submenuMovil3" role="button">
							<img class="numero" src="<?php echo base_url(); ?>assets/img/n3.svg" /> Soluciones
							contructivas
						</a>

						<ul class="collapse mt-3 mx-3" id="submenuMovil3" data-parent="#menu_mobile">
							<li><a href="<?php echo base_url(); ?>constructores/soluciones_constructivas">Información de Soluciones
							contructivas</a></li>
							<li><a href="<?php echo base_url(); ?>constructores/calculadora">Calculadora de Materiales de construcción</a></li>
							<li><a href="<?php echo base_url(); ?>constructores/planos">Planos Casas Tipo</a></li>
						</ul>
					</li>
				</ul>
			</div>
		</div>

		<div class="video-header">
			
			
			<video id="video" src="<?php echo base_url(); ?>files/v1.mp4" autoplay loop playsinline muted></video>


			<img src="<?php echo base_url(); ?>assets/img/bg_video.jpg" class="cantera_maestro">
			<div class="overlay"></div>
			<div class="viewport-header">
				<div class="home_container">
					<div class="enlaces_left">

						<div class="home_mobile_footer_logos desktop">
							<a href="https://wa.me/+51955585810" target="_blank" class="whatsapp_ico">
								<img class="whatsapp" src="<?php echo base_url(); ?>assets/img/whatsapp.svg" />
								&nbsp;contáctanos
							</a>
						</div>
						<div class="lineas top"></div>

						<span>
							De la cantera<br/>solo salen<br/>los mejores
						</span>
						<div class="lineas bottom"></div>
						<a href="<?php echo base_url(); ?>video" class="btn_ver_video video-inicial">
							ver video
							<img src="<?php echo base_url(); ?>assets/img/flecha-derecha.svg">
						</a>
						<div class="bottom_line"></div>
						<div class="home_mobile_footer_logos mobile" style="text-align: left;">
							<a href="https://wa.me/+51955585810" target="_blank" class="whatsapp_ico">
								<img class="whatsapp" src="<?php echo base_url(); ?>assets/img/whatsapp.svg" />
								&nbsp;contáctanos
							</a>
						</div>
					</div>
					<div class="logo_cantera_container">
						<a href="<?= site_url(); ?>">
							<img class="logo_cantera_home" src="<?php echo base_url(); ?>assets/img/unacem-cantera-rojo.svg"
								alt="unacem-cantera" />
						</a>
						
						<a class="flecha_abajo"
							href="#que_es_cantera">
							<img src="<?php echo base_url(); ?>assets/img/flecha-abajo-circulo.svg"
								alt="unacem-cantera" />
						</a>
					</div>
					<div class="enlaces_top" id="menu_desktop">
						<ul class="lista_top">
							<li>
								<a data-toggle="collapse" href="#submenuDesk1">
									<img class="numero uno" src="<?php echo base_url(); ?>assets/img/n1.svg" /> Programa profesional
								</a>
								<ul class="collapse mt-3 mx-3" id="submenuDesk1" data-parent="#menu_desktop">
									<li><a href="<?php echo base_url(); ?>alumnos/ingreso">Ingreso Alumno</a></li>
								</ul>
							</li>
							<li>
								<a data-toggle="collapse" href="#submenuDesk2">
									<img class="numero" src="<?php echo base_url(); ?>assets/img/n2.svg" /> Capacitaciones
								</a>
								<ul class="collapse mt-3 mx-3" id="submenuDesk2" data-parent="#menu_desktop">
									<li><a href="<?php echo base_url(); ?>capacitaciones/normativas">Protocolos Anticovid</a></li>
									<li><a href="<?php echo base_url(); ?>capacitaciones/calculo">Elaboración de Presupuestos</a></li>
									<li><a href="<?php echo base_url(); ?>capacitaciones/como_mejorar">Productividad en Construcción</a></li>
									<li><a href="<?php echo base_url(); ?>capacitaciones/riesgos">Riesgos en la Construcción</a></li>
									<!--
									<li><a href="<?php echo base_url(); ?>capacitaciones/tendencias">Evolución en Construcción</a></li>
									
									<li><a href="<?php echo base_url(); ?>capacitaciones/riesgos">Riesgos en la Construcción</a></li>-->
								</ul>
							</li>
							<li>
								<a data-toggle="collapse" href="#submenuDesk3">
									<img class="numero" src="<?php echo base_url(); ?>assets/img/n3.svg" /> Soluciones constructivas
								</a>
								<ul class="collapse mt-3 mx-3" id="submenuDesk3" data-parent="#menu_desktop">
									<li><a href="<?php echo base_url(); ?>constructores/calculadora">Calculadora de Materiales de construcción</a></li>
									<li><a href="<?php echo base_url(); ?>constructores/planos">Planos Casas Tipo</a></li>
								</ul>
							</li>

						</ul>
						<div id="sound_ico" class="sound_ico" onclick="toggleMute()"></div>
					</div>
				</div>
			</div>
		</div>
		<div class="container_red_section cover-vh d-flex flex-column">
		<div class="container  d-flex py-4 mx-auto flex-column" id="que_es_cantera">
			<div class="menu_ico">
				<img src="<?php echo base_url(); ?>assets/img/menu.svg">
			</div>

			<header id="header">
				<div class="container">
					<div class="row align-items-end justify-content-between">
						<div class="col-md-auto order-md-1">
							<nav id="nav"
								class="navbar navbar-expand-md navbar-dark p-md-0 text-white align-items-center">
								<div class="collapse navbar-collapse" id="menu" class="menu">
									<ul class="navbar-nav align-items-center">
										<li class="nav-item">
											<a href="<?php echo base_url(); ?>constructores/programa_profesional" class="nav-link" >Programa profesional</a>	
										</li>
										<li class="nav-item">
											<span class="divisor d-none d-md-block">|</span>
										</li>
										<li class="nav-item">
											<a class="nav-link" href="<?php echo base_url(); ?>capacitaciones">Capacitaciones</a>
										</li>
										<span class="divisor d-none d-md-block">|</span>
										<li class="nav-item">
											<a href="<?php echo base_url(); ?>constructores/soluciones_constructivas" class="nav-link">Soluciones constructivas</a>
										</li>
									</ul>
								</div>
							</nav>
						</div>
						<div class="col-md-auto order-md-0">
							<a href="/" class="logo_red">
								<img class="logo d-block mx-auto logo_red"
									src="<?php echo base_url(); ?>assets/img/unacem-cantera.svg" alt="unacem-cantera"
									width="230" />
							</a>
						</div>
					</div>
					<hr>
				</div>
			</header>
			<div class="top_space_inicio"></div>
			<main role="main">
				<div class="row">
					<div class="col-md-12">
						<img class="logo_cantera_body" src="<?php echo base_url(); ?>assets/img/unacem-cantera.svg"
							alt="unacem-cantera" />
					</div>
				</div>

				<div class="row">
					<div class="col-md-6">
						<h1>¿Qué es cantera?</h1>
					</div>
				</div>

				<div class="row" id="ques_es_cantera">
					<div class="col-md-6">
						<div class="row">
							<div class="col-md-6">

								<p>Son miles los constructores de obra que se esfuerzan día a día. 
Son incontables las obras que construyeron de forma segura.
Son millones de peruanos los que se beneficiaron con ellas.
Y en UNACEM reconocemos el arduo trabajo que realizan para el país.
Por eso, hemos desarrollado la plataforma virtual de aprendizaje “Unacem Cantera”.</p>
							</div>
							<div class="col-md-6">
								<p>Para contribuir con el desarrollo profesional de todos los constructores.
Para impulsar un sector constructivo más formal en el Perú.
Para crear una comunidad comprometida en construir de forma segura,
y en donde puedan reforzar sus conocimientos.
Conoce los cursos y herramientas que tenemos disponibles para ti.</p>
							</div>
							
							





						</div>
					</div>
					<div class="col-md-6">
						<div class="circle_content">
							<div class="circle_wraper">
								<div class="circle">
									<img src="<?php echo base_url(); ?>assets/img/cemento-andino.svg"
										alt="cemento-andino" />
								</div>
								<div class="circle apu">
									<img src="<?php echo base_url(); ?>assets/img/cemento-apu.svg" alt="cemento-apu" />
								</div>
								<div class="circle">
									<img src="<?php echo base_url(); ?>assets/img/cemento-sol.svg" alt="cemento-sol" />
								</div>
							</div>
						</div>
					</div>
				</div>
			</main>

			<!--
			<footer class="mastfoot mt-auto">
				<div class="inner">
					<a href="" class="whatsapp_ico">
						<img class="whatsapp" src="<?php echo base_url(); ?>assets/img/whatsapp.svg" />
						&nbsp;contáctanos
					</a>
					<a href="" class="youtube_ico">
						<img class="youtube" src="<?php echo base_url(); ?>assets/img/youtube.svg" />
						&nbsp;cantera tv
					</a>
				</div>

			</footer>-->
			
			</div>
			<div class="footer_inicio mt-auto d-flex align-items-center" id="footer"> 
				<div class="container container_footer">
					<!--<div class="back_button" onclick="onBack();" ></div>-->
					<div class="row justify-content-center">
						<div class="col-auto d-flex text-white in">
							<div class="unacem">© 2020 UNACEM</div>
							<div class="copy">
								<div><b>Unión Andina de Cementos S.A.A</b>&nbsp;&nbsp;Av. Atocongo 2440. Villa María del Triunfo. Lima, Perú. T (511) 217 0200</div>
							</div>
						</div>
					</div>
				</div>
				
			</div>
		</div>
	</body>
	<link rel="stylesheet" href="<?php echo base_url(); ?>assets/css/magnific-popup.css">
	<script src="//ajax.googleapis.com/ajax/libs/jquery/1.9.1/jquery.min.js"></script>
	<script>window.jQuery || document.write('<script src="js/jquery.min.js"><\/script>')</script>
	<script src="<?php echo base_url(); ?>assets/js/jquery.magnific-popup.js"></script>
	<script src="https://stackpath.bootstrapcdn.com/bootstrap/4.5.2/js/bootstrap.min.js"
		integrity="sha384-B4gt1jrGC7Jh4AgTPSdUtOBvfO8shuf57BaghqFfPlYxofvL8/KUEfYiJOMMV+rV" crossorigin="anonymous">
	</script>
	<script type="text/javascript">
	var show = false;
	$(function() {
		console.log("video-inicial");
		//$('.video-inicial').magnificPopup({type:'iframe'});
		$('.menu_ico').on('touchstart mousedown', function(e) {
			e.preventDefault();
			if (!show) {
				$(".menu_mobile_container").show();
				show = true;
			} else {
				$(".menu_mobile_container").hide();
				show = false;
			}

		});

		$('.menu_mobile_container .close').on('mousedown', function(e) {
			$(".menu_mobile_container").hide();
		});

	});
	function toggleMute() {
		var video=document.getElementById("video")
		if(video.muted){
			$("#sound_ico").addClass("active");
			video.muted = false;
		} else {
			$("#sound_ico").removeClass("active");
			video.muted = true;
		}
	}
	</script>

</html>
