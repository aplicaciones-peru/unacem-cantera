<link rel="stylesheet" href="<?php echo base_url(); ?>assets/css/cuestionario.css">
<script type="text/javascript" src="<?php echo base_url(); ?>assets/js/jquery-2.1.1.min.js"></script>
<script>
	var num_preguntas = <?php echo count($cuestionario);?>;
	var quiz = JSON.parse('<?php echo json_encode($cuestionario); ?>');
	var url_finalizar = "<?php echo base_url()."capacitaciones/".$slug."/finalizar"; ?>";
</script>
<script type="text/javascript" src="<?php echo base_url(); ?>assets/js/quiz.js"></script>
<style>
	#btn_continuar{
		display: none;
	}
</style>
<main id="main" class="mb-3">
  <div class="container">
    <div class="row">
      <div class="col-lg-12">
        <div class="row" style="padding: 20px; display: block;">
			<p>
				Responde las siguientes preguntas sobre la lección y obtén tu constancia de participación: </p>

				<ul>
					<?php for($i=0;$i<count($cuestionario);$i++){?>
					
					<li>
						<div>
							<p><?php echo $cuestionario[$i]["pregunta"]; ?></p>
							<ul style="list-style-type: none;">
								<?php for($p=0;$p<count($cuestionario[$i]["opciones"]);$p++){?>
								<li class="option" id="p_<?php echo $i;?>_<?php echo $p;?>">
								<input type="radio" value="<?php echo $p;?>" name="pregunta_<?php echo $i;?>"> <span><?php echo $cuestionario[$i]["opciones"][$p]?></span></li>
								<?php }?>
							</ul>
						</div>
					</li>
					
					<?php }?>

				</ul>
				<div class="btn_cantera" id="btn_validar">
					VALIDAR
				</div>
				<div class="btn_cantera" id="btn_continuar">
					CONTINUAR
				</div>
				
        </div>

      </div>

    </div>

  </div>

</main>