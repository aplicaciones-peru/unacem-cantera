
 <?php
	$red = "";
	$flecha = "flecha-larga-derecha.svg";
	$flecha_small = "flecha-derecha.svg";
	if($tipo=="gris_claro"){
		$red="_red";
		$flecha = "fecha-larga-derecha_black.svg";
		$flecha_small = "flecha-derecha-black.svg";
	}
?>

<main id="main" class="mb-3">
			<div class="container">
			<div class="row">
				<div class="col-lg-5 col-md-12">
					<div class="box_capacitaciones">
						<div class="arrow_content">
								<div class="arrow">
									<img class="arrow-larga-derecha" src="<?php echo base_url(); ?>assets/img/<?php echo $flecha; ?>" alt="fecha-larga-derecha" />
							 </div>
						</div>
						<!--Lorem ipsum dolor sit amet, consectetur adipiscing elit,
						sed do eiusmod tempor incididunt ut labore et dolore magna
						aliqua. Ut enim ad minim veniam, quis nostrud exercitation
						ullamco laboris nisi ut aliquip ex ea commodo consequat.-->
						<div class="title">Soluciones Constructivas</div>
					</div>
				</div>
				
				

				<div class="col-lg-7 col-md-12">
					
					<div class="row">
						<div class="col-lg-12 col-md-12">
							<p class="top_text">En UNACEM, estamos comprometidos con la seguridad y la formalización del proceso constructivo en el país. Por ello, ponemos a tu disposición herramientas y soluciones constructivas útiles para el desarrollo y planificación de tus proyectos.
*Recuerda que todo proyecto de construcción debe pasar por la supervisión de un ingeniero civil que garantice y valide los procesos constructivos y la seguridad de la obra o proyecto.</p>
						</div>
					</div>
					<div class="row">
						<div class="col-lg-4 col-md-4">
							<hr>
							<div class="item">
								<div class="numero">
									<img class="numero_uno" src="<?php echo base_url(); ?>assets/img/n1<?php echo $red; ?>.svg" alt="n1" />
									<hr>
								</div>
								<a class="contenido" href="<?php echo base_url(); ?>constructores/calculadora">
									Calculadora de materiales de construcción

								</a>
								<div class="arrow_next">
									<img src="<?php echo base_url(); ?>assets/img/<?php echo $flecha_small; ?>" alt="flecha-derecha" />
								</div>
							</div>
						</div>
						<div class="col-lg-4 col-md-4">
							<hr>
							<div class="item">
								<div class="numero">
									<img src="<?php echo base_url(); ?>assets/img/n2<?php echo $red; ?>.svg" alt="n1" />
									<hr>
								</div>
								<a class="contenido" href="<?php echo base_url(); ?>constructores/planos">
									Planos Casas tipo

								</a>
								<div class="arrow_next">
									<img src="<?php echo base_url(); ?>assets/img/<?php echo $flecha_small; ?>" alt="flecha-derecha" />
								</div>
							</div>
						</div>
						<!--<div class="col-lg-4 col-md-4">
							<hr>
							<div class="item" style="opacity: 0.5;">
								<div class="numero">
									<img src="<?php echo base_url(); ?>assets/img/n3<?php echo $red; ?>.svg" alt="n1" />
									<hr>
								</div>
								<div class="contenido" href="<?php echo base_url(); ?>constructores/manual_construccion">
									El Manual de Construcción UNACEM

								</div>
								<div class="arrow_next">
									<img src="<?php echo base_url(); ?>assets/img/<?php echo $flecha_small; ?>" alt="flecha-derecha" />
								</div>
							</div>
						</div>-->

					</div>
					<!--
					<div class="puntos"></div>
					<div class="row">
						<div class="col-lg-4 col-md-4">
							<hr>
							<div class="item" style="opacity: 0.5;">
								<div class="numero">
									<img src="<?php echo base_url(); ?>assets/img/n4<?php echo $red; ?>.svg" alt="n1" />
									<hr>
								</div>
								<div class="contenido" href="<?php echo base_url(); ?>constructores/tablas_equivalencia">
									Tabla de equivalencia y dosificación

								</div>
								<div class="arrow_next">
									<img src="<?php echo base_url(); ?>assets/img/<?php echo $flecha_small; ?>" alt="flecha-derecha" />
								</div>
							</div>
						</div>
					</div>-->
				</div>
			</div>
		</main>