<meta name='viewport' 
     content='width=device-width, initial-scale=1.0, maximum-scale=1.0, 
     user-scalable=0' >
<link rel="stylesheet" href="<?php echo base_url(); ?>assets/css/calculadora.css">
<link rel="stylesheet" href="<?php echo base_url("assets/css/calculadora_int.css"); ?>">

<?php 

$items = [
	"CIMIENTOS",
	"SOBRECIMIENTOS",
	"ZAPATAS PARA COLUMNAS",
	"ZAPATAS PARA MUROS",
	"MURO DE CONTENCIÓN",
	"COLUMNAS",
	"PLACAS",
	"VIGAS",
]

?>

<main id="main" class="mb-3">
<div class="calculadora container">
  <div class="row align-items-stretch">
    <div class="col-sm-4">
      <div class="calculadora__modo">
		<h5>SELECCIONA LA CONSTRUCCIÓN A REALIZAR <?php echo $id; ?></h5>
        <select class="form-control" id="selector">
			<?php foreach( $items as $i => $item ): ?>
			
			<option value="<?= $i ?>" <?= ($i == $id - 1 ? "selected" : "") ?>><?= $item ?></option>
			<?php endforeach; ?>
        </select>
      </div><img class="img-fluid d-block my-3 mx-auto" id="imagen" src="data:image/gif;base64,R0lGODlhAQABAIAAAAAAAP///yH5BAEAAAAALAAAAAABAAEAAAIBRAA7" width="300"/>
    </div>
    <div class="col-sm-4">
      <div class="calculadora__entrada">
	  
        <div class="calculadora__grupo--muro">
          <div class="row align-items-end">
            <div class="col">
              <div class="form-group">
                <label>Forma de colocar el ladrillo</label>
                <div class="opciones">
                  <div class="form-check">
                    <input class="form-check-input" id="forma0" type="radio" name="forma" value="0" onchange="radioMuroOnChange(this)"/>
                    <label class="form-check-label" for="forma0">De Cabeza</label>
                  </div>
                  <div class="form-check">
                    <input class="form-check-input" id="forma1" type="radio" name="forma" value="1" onchange="radioMuroOnChange(this)"/>
                    <label class="form-check-label" for="forma1">De Soga</label>
                  </div>
                  <div class="form-check">
                    <input class="form-check-input" id="forma2" type="radio" name="forma" value="2" onchange="radioMuroOnChange(this)"/>
                    <label class="form-check-label" for="forma2">De Canto</label>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
        <div class="calculadora__grupo--muro">
          <div class="row align-items-end">
            <div class="col">
              <div class="form-group row align-items-end">
                <label class="col-sm-3 col-form-label">Largo</label>
                <div class="col-sm-9">
                  <input class="form-control w-100" id="largo-muro" type="number" name="largo" onchange="inputMuroChange(this)" value="5"/>
                </div>
              </div>
              <div class="form-group row align-items-end">
                <label class="col-sm-3 col-form-label">Ancho</label>
                <div class="col-sm-9">
                  <input class="form-control w-100" id="ancho-muro" type="number" name="ancho" onchange="inputMuroChange(this)" value="2.8"/>
                </div>
              </div>
            </div>
          </div>
        </div>
	  
        <div class="calculadora__grupo">
          <div class="row align-items-end">
            <div class="col-md-5">
              <div class="form-group" id="opciones">
                <label>Diseño</label>
                <div class="opciones"></div>
              </div>
            </div>
            <div class="col-md-7">
              <div class="form-group row align-items-end" style="margin-bottom: 0.5rem">
                <label class="col-sm-4 col-form-label"><label style="padding: 0px; line-height: 13px;">Tamaño maximo del agregado grueso</label></label>
                <div class="col-sm-8">
                  <input class="form-control w-100 i-calc" type="text" id="agregadoGruesoTamagno" readOnly="readOnly" style="margin-bottom: 15px;"/>
                </div>
              </div>
            </div>
          </div>
        </div>
        <div class="calculadora__grupo" >
          <div class="row" style="margin-top: 20px;">
            <div class="col-md-6">
              <div class="form-group">
                <label class="lbl-resistencia">Resistencia a la compresión</label>
                <input class="form-control w-100 i-calc" type="number" id="resistencia" readOnly="readOnly"/>
              </div>
            </div>
            <div class="col-md-6">
              <div class="form-group row">
                <label class="col-sm-3 col-form-label">Largo:</label>
                <div class="col-sm-9">
                  <input class="form-control w-100 i-calc" type="number" name="largo" id="largo" onchange="inputOnChange(this)"/>
                </div>
              </div>
              <div class="form-group row">
                <label class="col-sm-3 col-form-label">Ancho:</label>
                <div class="col-sm-9">
                  <input class="form-control w-100 i-calc" type="number" name="ancho" id="ancho" onchange="inputOnChange(this)"/>
                </div>
              </div>
              <div class="form-group row">
                <label class="col-sm-3 col-form-label">Altura:</label>
                <div class="col-sm-9">
                  <input class="form-control w-100 i-calc" type="number" name="alto" id="alto" onchange="inputOnChange(this)"/>
                </div>
              </div>
            </div>
          </div>
        </div>
        <div class="calculadora__grupo">
          <div class="row" style="margin-top: 20px;">
            <div class="col-md-5">
              <div class="row align-items-end">
                <div class="col label_resultado_content">
	                <label class="label_resultado">VOLUMEN TOTAL OBRA:</label>
	            </div>
                <div class="col">
                  <div class="volumen calculadora__valor"></div>
                </div>
              </div>
            </div>
            <div class="col-md-7 resultado_2">
              <div class="form-group row align-items-end">
	            <div class="col label_resultado_content">
                	<label class="label_resultado">Agua por bolsa de cemento:</label>
                </div>
                <div class="col nopadleftright">
                  <div class="aguaVolumen calculadora__valor"></div>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
    <div class="col-sm-4">
      <div class="calculadora__salida">
        <div class="row">
          <div class="col">
            <h5><b>RESULTADO</b></h5>
            <table class="table calculadora__resultados">
              <thead>
                <tr>
                  <th><span class="top_titulo"><b style="font-family: 'silkamedium';">COMPONENTES</br>DEL CONCRETO</br>POR m3</b></span> </th>
                  <th><span class="top_titulo"><b style="font-family: 'silkamedium';">PESO (KG)</b></span></th>
                  <th><span class="top_titulo"><b style="font-family: 'silkamedium';">PARA LA OBRA</b></span><div class="small_text">(con un desperdicio de 10%)</div></th>
                </tr>
              </thead>
              <tbody>
                <tr>
                  <th>Agregado grueso (piedra) <span class="agregadoGruesoTamagno"></span>"</th>
                  <td class="text-right">
                    <div class="agregadoGruesoPeso"></div>
                  </td>
                  <td class="text-right">
                    <div class="agregadoGruesoVolumen"></div>
                  </td>
                </tr>
                <tr>
                  <th>Agregado fino (arena gruesa)</th>
                  <td class="text-right">
                    <div class="agregadoFinoPeso"></div>
                  </td>
                  <td class="text-right">
                    <div class="agregadoFinoVolumen"></div>
                  </td>
                </tr>
                <tr>
                  <th>Bolsa de cemento</th>
                  <td class="text-right">
                    <div class="cementoPeso"></div>
                  </td>
                  <td class="text-right">
                    <div class="cementoVolumen"></div>
                  </td>
                </tr>
              </tbody>
            </table>
			
			
            <table class="table calculadora__resultados--muro">
              <tbody>
                <tr>
                  <th>Cantidad de ladrillo</th>
                  <td class="text-right">
                    <div class="muro-ladrillos"></div>
                  </td>
                </tr>
                <tr>
                  <th>Bolsas de cemento</th>
                  <td class="text-right">
                    <div class="muro-cemento"></div>
                  </td>
                </tr>
                <tr>
                  <th>Arena gruesa (m<sup>3</sup>)</th>
                  <td class="text-right">
                    <div class="muro-arena"></div>
                  </td>
                </tr>
              </tbody>
            </table>
			
            <br/>
			<p>Contacta y cotiza tus materiales de construcción en  </p>
            <a href="https://www.progresol.com/Cotizar/Productos" target="_blank" class="btn_cantera">PROGRESOL</a>
            
          </div>
        </div>
      </div>
    </div>
  </div>
</div>
</main>

<!--
<main id="main" class="mb-3">
	<div class="container">
        <div class="row">
          <div class="col-lg-4 col-md-12 line_right">
            <div class="square_calc_container">
              <div class="selecciona_construccion"> 
                <h5>SELECCIONA LA CONSTRUCCION A REALIZAR <?php echo $id; ?></h5>
                <select>
                  <option>CIMIENTOS</option>
                  <option>SOBRECIMIENTOS</option>
                  <option>ZAPATAS PARA COLUMNAS</option>
                  <option>ZAPATAS PARA MUROS</option>
                  <option>MURO DE CONTENCION</option>
                  <option>MUROS</option>
                  <option>COLUMNAS</option>
                  <option>PLACAS</option>
                  <option>VIGAS</option>
                </select>
              </div>
              <img src="assets/img/imagen1.png"/>
            </div>
          </div>
          <div class="col-lg-4 col-md-12 line_right">
            <div class="square_calc_container">
              <div class="selecciona_construccion">
				 <img src="assets/img/1_CIMIENTOS.png"/>	
              </div>
             
            </div>
          </div>
          <div class="col-lg-4 col-md-12">
            <div class="square_calc_container">
              <div class="selecciona_construccion">
				<img src="assets/img/calc2.png"/>
              </div>
              
            
            Contacta y cotiza tus materiales de construcción en  
              <a href="https://www.progresol.com/Cotizar/Productos" target="_blank" class="btn_cantera">PROGRESOL</a>
            
            </div>
          </div>
        </div>
        </div>
      </main>
-->

<script>
	const base_url = "<?= base_url(); ?>";
	const id = <?= $id - 1 ?>;
</script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/sugar/2.0.6/sugar.min.js"></script>
<script src="https://rawcdn.githack.com/jbreckmckye/trkl/00a4856987f4bd5f3d276177881f60715baeb0fb/trkl-min.js"></script>
<script src="<?php echo base_url("assets/js/calculadora.js"); ?>"></script>

