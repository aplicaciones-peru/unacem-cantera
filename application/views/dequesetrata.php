<div class="container">
    <div class="row">

      <div class="col-lg-12">
	      <div class="row" style="margin-top: 50px;">
		      <h2>¿De qué se trata?</h2>
	      </div>
        
        <div class="row" style="display: block; margin-top: 30px; padding-bottom: 30px;">
			
			
			
				<p>UNACEM con el objetivo de contribuir al desarrollo y formalización de los trabajadores de construcción del país, ha desarrollado un programa virtual de formación profesional y certificación gratuita, en alianza con el Instituto Capeco.</p>
	<p>
	El programa consta de 6 cursos especializados (lo hemos subrayado porque al hacer click debe mostrar la malla curricular, no como pop up, sino como una vista completa) que  complementarán la experiencia de cada participante con conocimiento teórico, lo que les permitirá mejorar el resultado final de sus obras y ser mejores profesionales.</p>
	<p>
	Si estás interesado en iniciar tu formación profesional postula  AQUÍ. </p>
	
	<p>Xxxxx (nombre del programa)</p>

			

        </div>

        
      </div>
    </div>
  </div>
 </div>
  