<!doctype html>
<html lang="en">
<head>
  <meta charset="utf-8">
  <title>UNACEM - CANTERA</title>
  <base href="/">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <link rel="icon" type="image/x-icon" href="/favicon.ico">
  <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.5.2/css/bootstrap.min.css" integrity="sha384-JcKb8q3iqJ61gNV9KGb8thSsNjpSL0n8PARn9HuZOnIxN0hoP+VmmDGMN5t9UJ0Z" crossorigin="anonymous">
  <link rel="stylesheet" href="<?php echo base_url(); ?>assets/css/fonts.css">
  <link rel="stylesheet" href="<?php echo base_url(); ?>assets/css/comun.css">
  <link rel="stylesheet" href="<?php echo base_url(); ?>assets/css/inicio.css">
  <script type="text/javascript">
	  let vh = window.innerHeight * 0.01 + 'px';
          document.documentElement.style.setProperty('--vh', vh);
          window.addEventListener('resize', function () {
            var vh = window.innerHeight * 0.01 + 'px';
            document.documentElement.style.setProperty('--vh', vh);
          });
  </script>
	<!-- Google Tag Manager -->
	<script>(function(w,d,s,l,i){w[l]=w[l]||[];w[l].push({'gtm.start':
	new Date().getTime(),event:'gtm.js'});var f=d.getElementsByTagName(s)[0],
	j=d.createElement(s),dl=l!='dataLayer'?'&l='+l:'';j.async=true;j.src=
	'https://www.googletagmanager.com/gtm.js?id='+i+dl;f.parentNode.insertBefore(j,f);
	})(window,document,'script','dataLayer','GTM-WR3W5TM');</script>
	<!-- End Google Tag Manager -->
  <style>
    html,body{
      background-color: black !important;
    }
    .btn_ver_video{
      margin-bottom: 90px;
    }
    /*.btn_bottom_container{
      position: relative;
    }*/
  </style>
  <style>
	  .mobile_texto{
		  
	  }
	  /*
		  <span class="mobile_texto">
            De la cantera<br/>solo salen<br/>los mejores
          </span>*/
  </style>
  <script>
	  /*var flag  = false;
	  setInterval(()=>{
		  if(!flag){
			  flag = true;
			 // document.getElementById("texto_cantera").innerHTML = "<span class='small_text_cantera'>Nueva plataforma virtual de capacitaciones profesionales que busca formar a los mejores constructores del país<span>";
		  }else{
			  flag = false;
			 // document.getElementById("texto_cantera").innerHTML = "De la cantera<br/>solo salen<br/>los mejores";
		  }
	  }, 5000)*/
	  



  </script>
</head>
<body>
	<!-- Google Tag Manager (noscript) -->
<noscript><iframe src="https://www.googletagmanager.com/ns.html?id=GTM-WR3W5TM"
height="0" width="0" style="display:none;visibility:hidden"></iframe></noscript>
<!-- End Google Tag Manager (noscript) -->
  <div class="video-header">
  <img src="<?php echo base_url(); ?>assets/img/bg_home.jpg" class="cantera_maestro_inicio">
  <div class="overlay"></div>
  <div class="viewport-header">
    <div class="home_container">
        <div class="enlaces_left inicio">
          <div class="lineas top"></div>
          <span class="mobile_texto">
	          <div class="desktop">
	            De la cantera<br/>solo salen<br/>los mejores
	          </div>
	          <div class="mobile">
		          
		          
		           <div id="texto_cantera">
					  <div class="item">De la cantera<br>solo salen<br>los mejores</div>
					  <div class="item small_text_cantera">Nueva plataforma virtual de capacitaciones profesionales que busca formar a los mejores constructores del país</div>
					</div>
	          	




		          
		          
	          </div>
          </span>
          
          <div class="lineas bottom"></div>
        </div>
        <div class="logo_cantera_container">
          <img class="logo_cantera_home" src="<?php echo base_url(); ?>assets/img/unacem-cantera-rojo.svg" alt="unacem-cantera" />
        </div>
        <div class="btn_bottom_container">
          <a href="<?php echo base_url(); ?>constructores" class="btn_cantera">
            CONSTRUCTORES
          </a>
          <a href="/empresas/" class="btn_cantera">
            EMPRESAS
          </a>
        </div>
        
        <div class="enlaces_right">
          <span>
            Nueva plataforma virtual de<br/>capacitaciones profesionales que<br/>busca formar a los mejores<br/>constructores del país
          </span>
        </div>

    </div>
  </div>
</div>


</body>
<script src="//ajax.googleapis.com/ajax/libs/jquery/1.9.1/jquery.min.js"></script>
<script>window.jQuery || document.write('<script src="js/jquery.min.js"><\/script>')</script>
<script src="https://stackpath.bootstrapcdn.com/bootstrap/4.5.2/js/bootstrap.min.js" integrity="sha384-B4gt1jrGC7Jh4AgTPSdUtOBvfO8shuf57BaghqFfPlYxofvL8/KUEfYiJOMMV+rV" crossorigin="anonymous"></script>
<script type="text/javascript">
var show = false;
	function cambiarTexto() {
		console.log("cambiarTexto")
		  const visible = $("#texto_cantera").find(".item.visible");
		
		  const oculto = $($("#texto_cantera").
		  find(".item:not(.visible)").get(0));
		  $("#texto_cantera").append(oculto);
		  
		  visible.removeClass("visible").
		  slideUp(1000);
		  oculto.addClass("visible").
		  slideDown(1000);
  }
  $(function() {
    $('.menu_ico').on('touchstart mousedown', function(e){
        e.preventDefault();
        if(!show){
          $(".menu_mobile_container").show();
          show=true;
        }else{
          $(".menu_mobile_container").hide();
          show=false;
        }

    });

    $('.menu_mobile_container .close').on('mousedown', function(e){
        $(".menu_mobile_container").hide();
    });
	const $textos = $("#texto_cantera");
	console.log("setInterval")
	setInterval(cambiarTexto, 5000);
	
	$("#texto_cantera").find(".item").removeClass("visible").hide();
	$($("#texto_cantera").find(".item").get(0)).addClass("visible").show();
	
  });
  
  	  	

		
</script>
</html>
