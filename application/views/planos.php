<style>
	.plano{
		max-width: 150px;
	}
	@media (max-width: 767px){
		.plano{
			max-width: 100px;
		}	
	}
</style>
<main id="main" class="mb-3">
			<div class="container">
			<div class="row">
				<div class="col-lg-5 col-md-12">
					<div class="box_capacitaciones">

						<div class="arrow_content">
								<div class="arrow">
									<img class="arrow-larga-derecha" src="<?php echo base_url(); ?>assets/img/fecha-larga-derecha.svg" alt="fecha-larga-derecha" />
							 </div>
						</div>
						<div class="title">PLANOS CASAS</div>
					</div>
				</div>
				<div class="col-lg-7 col-md-12">

					<div class="row">
						<div class="col-lg-12 col-md-12">
							<p class="top_text">Con el objetivo de mejorar la calidad de las construcciones y combatir la construcción informal en el Perú, ponemos a tu alcance 3 planos de casas tipo con formatos de 1, 2 y 3 pisos*, los cuales tienen la característica de ser sismo resistentes. En cada uno de estos planos encontrarás la distribución de los ambientes, los detalles de construcción de cada parte del inmueble y las especificaciones técnicas a considerar. <br/><br/>
*Los planos son referenciales, recuerda que es importante consultar con un ingeniero civil, ya que deben evaluar previamente las condiciones y características del terreno a construir.
</p>
						</div>
					</div>
					<div class="row">
						<div class="col-lg-4 col-md-4">
							<a href="<?php echo base_url(); ?>files/CASA_TIPO1.pdf" target="_blank" style="text-decoration: none;">
							<img class="plano" src="<?php echo base_url(); ?>assets/img/icono_plano.svg" alt="n1" />
							<hr>
							
							
							<div class="item">
								<div class="numero">
									<img class="numero_uno" src="<?php echo base_url(); ?>assets/img/n1.svg" alt="n1" />
									<hr>
								</div>
								<div class="contenido">
									PLANOS CASA DE 1 PISO

								</div>
								<div class="arrow_next">
									<img src="<?php echo base_url(); ?>assets/img/flecha-derecha.svg" alt="flecha-derecha" />
								</div>
							</div>
							</a>
							
						</div>
						<div class="col-lg-4 col-md-4">
							<a target="_blank" href="<?php echo base_url(); ?>files/CASA_TIPO2.pdf" target="_blank" style="text-decoration: none;">
								<img class="plano" src="<?php echo base_url(); ?>assets/img/icono_plano.svg" alt="n1" />
								<hr>
								<div class="item">
									<div class="numero">
										<img src="<?php echo base_url(); ?>assets/img/n2.svg" alt="n1" />
										<hr>
									</div>
									<div class="contenido">
										PLANOS CASA DE 2 PISOS
	
									</div>
									<div class="arrow_next">
										<img src="<?php echo base_url(); ?>assets/img/flecha-derecha.svg" alt="flecha-derecha" />
									</div>
								</div>
							</a>
						</div>
						<div class="col-lg-4 col-md-4">
							<a target="_blank" href="<?php echo base_url(); ?>files/CASA_TIPO3.pdf" target="_blank" style="text-decoration: none;">
								<img class="plano" src="<?php echo base_url(); ?>assets/img/icono_plano.svg" alt="n1" />
								<hr>
								<div class="item">
									<div class="numero">
										<img src="<?php echo base_url(); ?>assets/img/n3.svg" alt="n1" />
										<hr>
									</div>
									<div class="contenido">
										PLANOS CASA DE 3 PISOS
	
									</div>
									<div class="arrow_next">
										<img src="<?php echo base_url(); ?>assets/img/flecha-derecha.svg" alt="flecha-derecha" />
									</div>
								</div>
							</a>
						</div>

					</div>


				</div>
			</div>
		</main>