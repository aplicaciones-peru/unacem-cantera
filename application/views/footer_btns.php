<div class="footer_spacing">&nbsp;</div>
		<footer id="footer" class="mt-auto d-flex align-items-center">
			<div class="container container_footer">
				<div class="back_button" onclick="onBack();" ></div>
				<div class="row justify-content-center">
					<div class="col-auto d-flex text-white in">
						<div class="unacem">© 2020 UNACEM</div>
						<div class="copy">
							<div><b>Unión Andina de Cementos S.A.A</b>&nbsp;&nbsp;Av. Atocongo 2440. Villa María del Triunfo. Lima, Perú. T (511) 217 0200</div>
						</div>
					</div>
				</div>
			</div>
		</footer>
	</div>
	</div>
</body>

<link rel="stylesheet" href="<?php echo base_url(); ?>assets/css/magnific-popup.css">
<script src="//ajax.googleapis.com/ajax/libs/jquery/1.9.1/jquery.min.js"></script>
<script>window.jQuery || document.write('<script src="js/jquery.min.js"><\/script>')</script>
<script src="<?php echo base_url(); ?>assets/js/jquery.magnific-popup.js"></script>
<script src="https://stackpath.bootstrapcdn.com/bootstrap/4.5.2/js/bootstrap.min.js" integrity="sha384-B4gt1jrGC7Jh4AgTPSdUtOBvfO8shuf57BaghqFfPlYxofvL8/KUEfYiJOMMV+rV" crossorigin="anonymous"></script>
<script type="text/javascript">
	//alert("*")
var show = false;
  $(function() {
	$('.video-turorial').magnificPopup({type:'iframe'});
    $('.menu_ico').on('touchstart mousedown', function(e){
        e.preventDefault();
        if(!show){
          $(".menu_mobile_container").show();
          show=true;
        }else{
          $(".menu_mobile_container").hide();
          show=false;
        }
    });
    $('.menu_mobile_container .close').on('mousedown', function(e){
        $(".menu_mobile_container").hide();
    });
  });
</script>
</html>