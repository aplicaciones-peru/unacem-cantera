<style>
	.modulo{
		background-color: transparent;
		width: 100%;
		text-align: center;
		text-transform: uppercase;
	}
	.top_space{
		height: 40px;
	}
	.mobile{
		display: none;
	}
	@media (max-width: 750px){
		.mobile{
			display: block;
		}
		.desktop{
			display: none;
		}
		.modulo{
			margin-top: 20px;
		}
	}
</style>
<main id="main" class="mb-3">
			<div class="container">
			<div class="row">

				<div class="col-lg-12 col-md-12">
					<h1>ESTRUCTURA DEL PROGRAMA</h1>
				</div>


				<div class="col-lg-12 col-md-12">

					<div class="row">
						<div class="col-lg-4 col-md-4">
							<div class="modulo">módulo 1</div>
							<hr>
							<div class="item">
								<div class="numero">
									<img class="numero_uno" src="<?php echo base_url(); ?>assets/img/n1_red.svg" alt="n1" />
									<hr>
								</div>
								<div class="contenido" >
									
									<a href="#">Curso 1. Lectura de Planos</a>
									<div class="contenido_add">
										Semana 01 - Planos de arquitectura<br/>
										Semana 02 - Planos de estructuras<br/>
										Semana 03 - Planos instalaciones eléctricas<br/>
										Semana 04 - Planos instalaciones sanitarias<br/>
								  </div>
								</div>
								<div class="arrow_next">
									<img src="<?php echo base_url(); ?>assets/img/flecha-derecha-black.svg" alt="flecha-derecha" />
								</div>
							</div>
						</div>
						
						<div class="col-lg-4 col-md-4 mobile">
							<hr>
							<div class="item">
								<div class="numero">
									<img src="<?php echo base_url(); ?>assets/img/n2_red.svg" alt="n1" />
									<hr>
								</div>
								<div class="contenido" >
									
									<a href="#">Curso 2. Especificaciones de los Materiales de Construcción 	</a>
									<div class="contenido_add">
										Semana 01 – Agregados y aglomerantes<br/>
										Semana 02 – Acero y Arcilla<br/>
										Semana 03 – Tipos de cementos<br/>
										Semana 04 - PVC
									</div>
								</div>
								<div class="arrow_next">
									<img src="<?php echo base_url(); ?>assets/img/flecha-derecha-black.svg" alt="flecha-derecha" />
								</div>
							</div>
						</div>
						
						
						<div class="col-lg-4 col-md-4">
							<div class="modulo">módulo 2</div>
							<hr>
							<div class="item">
								<div class="numero">
									<img src="<?php echo base_url(); ?>assets/img/n3_red.svg" alt="n1" />
									<hr>
								</div>
								<div class="contenido" >
									
									
									<a href="#">Curso 3. Dosificación y Mezcla del Concreto</a>
									<div class="contenido_add">
										Semana 01 – Concreto simple<br/>
										Semana 02 – Concreto armado<br/>
										Semana 03 – Concreto caravista<br/>
										Semana 04 – Métodos de curado
									</div>
								</div>
								<div class="arrow_next">
									<img src="<?php echo base_url(); ?>assets/img/flecha-derecha-black.svg" alt="flecha-derecha" />
								</div>
							</div>
						</div>
						<div class="col-lg-4 col-md-4 mobile">
							<hr>
							<div class="item">
								<div class="numero">
									<img src="<?php echo base_url(); ?>assets/img/n4_red.svg" alt="n1" />
									<hr>
								</div>
								<div class="contenido" >
									<a href="#">Curso 4. Sistema Constructivo de Albañilería 	</a>
									<div class="contenido_add">
										Semana 01 – Tipos de cimentación<br/>
										Semana 02 – Muros de albañilería<br/>
										Semana 03 – Tipos de armaduras<br/>
										Semana 04 – Tipos de aligerado
									</div>
								</div>
								<div class="arrow_next">
									<img src="<?php echo base_url(); ?>assets/img/flecha-derecha-black.svg" alt="flecha-derecha" />
								</div>
							</div>
						</div>
						<div class="col-lg-4 col-md-4">
							<div class="modulo">módulo 3</div>
							<hr>
							<div class="item">
								<div class="numero">
									<img src="<?php echo base_url(); ?>assets/img/n5_red.svg" alt="n1" />
									<hr>
								</div>
								<div class="contenido" >
									
									
									<div class="contenido" >
									<a href="#">Curso 5. Acabados de Concreto Caravista </a>
									<div class="contenido_add">
										Semana 01 – Tipos de concreto caravista<br/>
										Semana 02 – Usos de concreto caravista<br/>
										Semana 03 - Encofrado de concreto caravista<br/>
										Semana 04 – Aditivos para concreto caravista
									</div>
								</div>
								</div>
								<div class="arrow_next">
									<img src="<?php echo base_url(); ?>assets/img/flecha-derecha-black.svg" alt="flecha-derecha" />
								</div>
							</div>
						</div>

					</div>
					<div class="puntos"></div>
					<div class="row">
						<div class="col-lg-4 col-md-4 desktop">
							<hr>
							<div class="item">
								<div class="numero">
									<img src="<?php echo base_url(); ?>assets/img/n2_red.svg" alt="n1" />
									<hr>
								</div>
								<div class="contenido" >
									
									<a href="#">Curso 2. Especificaciones de los Materiales de Construcción 	</a>
									<div class="contenido_add">
										Semana 01 – Agregados y aglomerantes<br/>
										Semana 02 – Acero y Arcilla<br/>
										Semana 03 – Tipos de cementos<br/>
										Semana 04 - PVC
									</div>
								</div>
								<div class="arrow_next">
									<img src="<?php echo base_url(); ?>assets/img/flecha-derecha-black.svg" alt="flecha-derecha" />
								</div>
							</div>
						</div>
						<div class="col-lg-4 col-md-4 desktop">
							<hr>
							<div class="item">
								<div class="numero">
									<img src="<?php echo base_url(); ?>assets/img/n4_red.svg" alt="n1" />
									<hr>
								</div>
								<div class="contenido" >
									<a href="#">Curso 4. Sistema Constructivo de Albañilería 	</a>
									<div class="contenido_add">
										Semana 01 – Tipos de cimentación<br/>
										Semana 02 – Muros de albañilería<br/>
										Semana 03 – Tipos de armaduras<br/>
										Semana 04 – Tipos de aligerado
									</div>
								</div>
								<div class="arrow_next">
									<img src="<?php echo base_url(); ?>assets/img/flecha-derecha-black.svg" alt="flecha-derecha" />
								</div>
							</div>
						</div>
						<div class="col-lg-4 col-md-4">
							<hr>
							<div class="item">
								<div class="numero">
									<img src="<?php echo base_url(); ?>assets/img/n6_red.svg" alt="n1" />
									<hr>
								</div>
								<div class="contenido" >
									<a href="#">Curso 6. Acabados Húmedos y secos </a>
									<div class="contenido_add">
										Semana 01 – Cerámicos y porcelanatos<br/>
										Semana 02 – Piedras ornamentales<br/>
										Semana 03 – Pisos de madera y laminado<br/>
										Semana 04 – Pisos de PVC
									</div>
								</div>
								<div class="arrow_next">
									<img src="<?php echo base_url(); ?>assets/img/flecha-derecha-black.svg" alt="flecha-derecha" />
								</div>
							</div>
						</div>
					</div>


					<div class="puntos"></div>
					<br/>
					<br/>
					<div class="row">
						<div class="col-lg-4 col-md-4">
							<div class="item">
								<div class="numero" style="width:20px;"></div>
								<div class="contenido" >
									<div>MATERIAL DEL PROGRAMA</div>
									<div class="contenido_add">
										Guía didáctica del curso<br/>
										Recursos semanales online<br/>
										Presentación multimedia semanal<br/>
										Sesiones de clases grabadas<br/>
										Lectura básica semanal<br/>
										Documentación complementaria semanal<br/>
										Cuestionario de evaluación online por cada tema semanal finalizado.
									</div>
								</div>
							</div>
						</div>
						<div class="col-lg-4 col-md-4">
							
							<div class="item">
								<div class="numero" style="width:20px;"></div>
								<div class="contenido" >
									<div>DURACIÓN</div>
									<div class="contenido_add">
									El tiempo de duración de cada Curso Virtual es de un mes
									</div>
								</div>
							</div>
							<div class="item" style="margin-top:20px;">
								<div class="numero" style="width:20px;"></div>
								<div class="contenido" >
									<div>EVALUACIÓN</div>
									<div class="contenido_add">
									Se realizará una evaluación online semanal; al finalizar el curso obtendrás cuatro notas y estas generaran el promedio ponderado final.
									</div>
								</div>
							</div>
						</div>
						<div class="col-lg-4 col-md-4">
							
							<div class="item">
								<div class="numero" style="width:20px;"></div>
								<div class="contenido" >
									<div>CERTIFICACIÓN</div>
									<div class="contenido_add">
										Lo obtendrás al alcanzar un promedio final aprobatorio mayor a 13 como promedio ponderado final y deberás participar en las actividades programadas en el curso. El certificado será emitido a nombre de Unacem y el Insituto Capeco, por las 216 horas cumplidas y aprobadas.
									</div>
								</div>
							</div>
						</div>
					</div>



				</div>


			</div>
		</main>