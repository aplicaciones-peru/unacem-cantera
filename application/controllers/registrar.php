<?php

class Registrar extends CI_Controller{

     public function __construct()
    {
		parent::__construct();

    }
    public function index() {
	    if(isset($_POST['email'])){
			$nombre 		= $_POST['nombre'];
			$apellidos 		= $_POST['apellidos'];
			$email 			= $_POST['email'];
			$dni 			= $_POST['dni'];
			$celular 		= $_POST['celular'];
			$telefono 		= $_POST['telefono'];
			$ciudad			= $_POST['ciudad'];		
			$password 		= $_POST['password'];

			$insert_data = array();

			$insert_data["user_name"] 		= $dni;
			$insert_data["pass_word"] 		= md5($password);
			$insert_data["pass_word_visible"] = $password;

			$insert_data["nombre"] 			= $nombre;
			$insert_data["apellidos"] 		= $apellidos;
			$insert_data["email"] 			= $email;
			$insert_data["dni"] 			= $dni;
			$insert_data["celular"] 		= $celular;
			$insert_data["telefono"] 		= $telefono;
			$insert_data["ciudad"] 			= $ciudad;
			$insert_data["fecha"] 			= date('Y-m-d H:i:s');
			//$insert_data["perfil"] 			= 'Usuario';
			$this->db->insert("membership", $insert_data);
			$datos = array();
			$datos['user_name'] = $dni;
			//$this->sentEmail($email,$password,$nombres);
			
			$id_usuario = $this->db->insert_id();
			$data = array(
				'user_name' => $dni,
				'id' => $id_usuario,
				'perfil' => "Usuario",
				'is_logged_in' => true
			);
			$this->session->set_userdata($data);
			redirect('/capacitaciones?registro=ok');
		}
    }
    
 }