<?php
	date_default_timezone_set('America/Lima');
	class Admin extends CI_Controller {

    public function __construct()
    {
		parent::__construct();
		$this->load->library('grocery_CRUD');
		$this->load->helper('url');
		if(!$this->session->userdata('is_logged_in')){
			redirect('cms');
		}else{
			$perfil = $this->session->userdata('perfil');
			if($perfil=="Usuario"){
				redirect('/');
			}
		}
    }
    public function index(){
	 	if(!$this->session->userdata('is_logged_in')){
			redirect('cms');
		}
	}
    public function home()
	{
		if(!$this->session->userdata('is_logged_in')){
			redirect('cms');
			exit();
		}
		$params = array();
		$params["no_crud"] = true;
		$this->load->view('admin/header');
		$this->load->view('home.php');
		$this->load->view('admin/footer', $params);
	}
	public function usuarios(){
		
	    $crud = new grocery_CRUD();
	    $crud->set_subject('Usuarios');
		$crud->set_table('membership');
		$crud->where('perfil','Usuario');
		
		$crud->unset_print();
		$crud->unset_read();
		$crud->unset_add();
		//$crud->unset_export();
		$c = array('nombre','apellidos','email','dni');
		$crud->columns($c);

		$crud->edit_fields($c);
		$crud->add_fields($c);

		$output = $crud->render();
		$params["crud"]=true;
		$this->load->view('admin/header');
		$this->load->view('admin/crud.php',$output);
		$this->load->view('admin/footer',$params);
	}
	public function alumnos(){
		
	    $crud = new grocery_CRUD();
	    $crud->set_subject('Alumnos');
		$crud->set_table('alumnos');

		$crud->unset_print();
		$crud->unset_read();
		$crud->unset_add();
		
		
		if (in_array("export", $this->uri->rsegment_array())) {
			$c = array('dni','nombres','apellido_paterno','Sexo','Celular','Email','codigo','password','notas');
			
			$crud->columns($c);
		}else{
			$c = array('dni','nombres','apellido_paterno','Email','codigo','password','notas');
			$crud->columns($c);
		}

		//$c = array('dni','nombres','apellido_paterno','Email','codigo','password','notas');
		//$crud->columns($c);
		
		$crud->display_as('dni','DNI');
		
		$crud->display_as('notas','Modulo');

		$crud->edit_fields($c);
		$crud->add_fields($c);

		$output = $crud->render();
		$params["crud"]=true;
		$this->load->view('admin/header');
		$this->load->view('admin/crud.php',$output);
		$this->load->view('admin/footer',$params);
	}
	public function postula(){
		
	    $crud = new grocery_CRUD();
	    $crud->set_subject('Postula');
		$crud->set_table('postula');
		$crud->unset_print();
		//$crud->unset_read();
		$crud->unset_add();
		$crud->unset_edit();
		
		if (in_array("export", $this->uri->rsegment_array())) {
			$c = array('nombres','apellidos','dni','email','celular','telefono','experiencia','ocupacion','ocupacion_otros');
			$crud->columns($c);
		}else{
			$c = array('nombres','apellidos','dni','email','celular','experiencia','ocupacion');
			$crud->columns($c);
		}
		
		//$crud->add_fields(array('email','nombres','apellidos','usuario','password'));
		//$crud->edit_fields(array('email','nombres','apellidos','usuario','password'));
		
		$output = $crud->render();
		$params["crud"]=true;
		$this->load->view('admin/header');
		$this->load->view('admin/crud.php',$output);
		$this->load->view('admin/footer',$params);
	}
	public function cursos(){
		
	    $crud = new grocery_CRUD();
	    $crud->set_subject('Cursos');
		$crud->set_table('cursos');
		$crud->unset_print();
		$crud->unset_read();
		//$crud->unset_add();
		//$c = array('nombres','apellidos','dni','email','ocupacion');
		//$crud->columns($c);
		//$crud->add_fields(array('email','nombres','apellidos','usuario','password'));
		//$crud->edit_fields(array('email','nombres','apellidos','usuario','password'));
		
		$output = $crud->render();
		$params["crud"]=true;
		$this->load->view('admin/header');
		$this->load->view('admin/crud.php',$output);
		$this->load->view('admin/footer',$params);
	}

	
	
	public function asistencia()
    {
	    $crud = new grocery_CRUD();
	    $crud->set_subject('Asistencia');
		$crud->set_table('estadisticas');
		$crud->where('jf8032d5c.id !=','1');
		$crud->set_primary_key("usuario");
		
		$crud->set_relation("usuario", "membership", "{first_name} {last_name} - {email_addres} ");
		
		
		$crud->unset_print();
		$crud->unset_read();
		$crud->unset_edit();
		$crud->unset_delete();
		$crud->unset_add();
		$output = $crud->render();
		$params["crud"]=true;
		$this->load->view('admin/header');
		$this->load->view('admin/crud.php',$output);
		$this->load->view('admin/footer',$params);
	
	}
	public function trivia_respuestas()
    {
	    $crud = new grocery_CRUD();
	    $crud->set_subject('Trivia');
		$crud->set_table('trivia_respuestas');
		$crud->columns('preguntaid');
		
		//$crud->display_as('p1','Pregunta 1');
		//$crud->display_as('p2','Pregunta 2');
		//$crud->display_as('p3','Pregunta 3');
		//$crud->display_as('p4','Pregunta 4');
		
		
		//$crud->unset_export();
		$crud->unset_print();
		$crud->unset_read();
		$crud->unset_edit();
		$crud->unset_add();
		$output = $crud->render();
		$params["crud"]=true;
		$this->load->view('admin/header');
		$this->load->view('admin/crud.php',$output);
		$this->load->view('admin/footer',$params);
    }
    
	public function encuestas()
    {
	    $crud = new grocery_CRUD();
	    $crud->set_subject('Registros');
		$crud->set_table('encuesta');
		$crud->columns('p1','p2','p3','p4','fecha');
		
		$crud->display_as('p1','Pregunta 1');
		$crud->display_as('p2','Pregunta 2');
		$crud->display_as('p3','Pregunta 3');
		$crud->display_as('p4','Pregunta 4');
		
		
		//$crud->unset_export();
		$crud->unset_print();
		$crud->unset_read();
		$crud->unset_edit();
		$crud->unset_add();
		$output = $crud->render();
		$params["crud"]=true;
		$this->load->view('admin/header');
		$this->load->view('admin/crud.php',$output);
		$this->load->view('admin/footer',$params);
    }
	public function clientes_email()
    {
	    $crud = new grocery_CRUD();
	    $crud->set_subject('Registros');
		$crud->set_table('membership');
		$crud->where('perfil','Usuario');
		$crud->unset_export();
		$crud->unset_print();
		$crud->unset_read();
		$crud->unset_edit();
		$crud->unset_add();
		//$c = array('user_name','first_name','last_name','email_addres');

		$crud->columns('enviar_1','enviar_2','user_name','pass_word_visible','first_name','last_name','email_addres','email1','email2');
		$crud->callback_column('enviar_1',array($this,'_enviar_email_1'));
		$crud->callback_column('enviar_2',array($this,'_enviar_email_2'));
		//$crud->callback_before_insert(array($this,'encrypt_password_callback'));
		//$crud->callback_before_update(array($this,'encrypt_password_callback_before_update'));
		//$crud->callback_edit_field('pass_word',array($this,'set_password_input_to_empty'));

		//$c = array('user_name','pass_word','first_name','last_name','email_addres','perfil');
		$fields = array('user_name','perfil');
		$crud->required_fields($fields);
		//$crud->set_rules('user_name','Usuario','max_length[11]');
		$crud->display_as('pass_word_visible','Password');
		$crud->display_as('pass_word','Password');
		$crud->display_as('user_name','Email');
		$crud->display_as('first_name','Nombres');
		$crud->display_as('last_name','Apellidos');
		$crud->display_as('email_addres','Email');
		
		$crud->display_as('email1','Email Recordatorio');
		$crud->display_as('email2','Email Agradecimiento');

		$output = $crud->render();
		$params["crud"]=true;
		$this->load->view('admin/header');
		$this->load->view('admin/crud.php',$output);
		$this->load->view('admin/footer',$params);
	}
	
	
	public function clientes_email_prueba()
    {
	    $crud = new grocery_CRUD();
	    $crud->set_subject('Registros');
		$crud->set_table('membership2'); 
		//$crud->where('perfil','Usuario');
		$crud->unset_export();
		$crud->unset_print(); 
		$crud->unset_read();
		$crud->unset_edit();
		$crud->unset_add();
		$crud->columns('enviar_1','enviar_2','user_name','pass_word_visible','first_name','last_name','email_addres','email1','email2');
		$crud->callback_column('enviar_1',array($this,'_enviar_email_1'));
		$crud->callback_column('enviar_2',array($this,'_enviar_email_2'));
		$fields = array('user_name','perfil');
		$crud->required_fields($fields);
		$crud->display_as('pass_word_visible','Password');
		$crud->display_as('pass_word','Password');
		$crud->display_as('user_name','Email');
		$crud->display_as('first_name','Nombres');
		$crud->display_as('last_name','Apellidos');
		$crud->display_as('email_addres','Email');
		$crud->display_as('email1','Email Recordatorio');
		$crud->display_as('email2','Email Agradecimiento');
		$output = $crud->render();
		$params["crud"]=true;
		$this->load->view('admin/header');
		$this->load->view('admin/crud.php',$output);
		$this->load->view('admin/footer',$params);
	}
	
	
	
	public function _enviar_email_1($value, $row)
	{
	  return "<a href='".site_url('api/sent_email1/'.$row->id)."?v=".rand()."'>Enviar E1</a>";
	}
	public function _enviar_email_2($value, $row)
	{
	  return "<a href='".site_url('api/sent_email2/'.$row->id)."?v=".rand()."'>Enviar E2</a>";
	}
	
	
	function set_password_input_to_empty() {
		return "<input type='password' name='pass_word' class='form-control' value='' />";
	}
	function encrypt_password_callback($post_array) {
			$post_array['pass_word'] = md5($post_array['pass_word']);
			return $post_array;
	}
	function encrypt_password_callback_before_update($post_array, $primary_key) {
	    if(!empty($post_array['pass_word'])){
	        $post_array['pass_word'] = md5($post_array['pass_word']);
	    }else{
	        unset($post_array['pass_word']);
	    }
	  return $post_array;
	}
	
	
	
	
	
	
	public function desactivar_preguntas($sala){
		$sql = "UPDATE preguntas SET activo=0 WHERE sala='$sala'";
		$query = $this->db->query($sql);
		redirect("admin/trivia_sala_".$sala);
	
		
	}
	public function borrar_respuestas($sala){
		$sql = "DELETE FROM trivia_respuestas WHERE sala='$sala'";
		$query = $this->db->query($sql);
		redirect("admin/trivia_sala_".$sala);
	}
	public function borrar_usados($sala){
		$sql = "UPDATE preguntas SET usado=0 WHERE sala='$sala'";
		$query = $this->db->query($sql);
		redirect("admin/trivia_sala_".$sala);
	}
	
	public function trivia_sala_1(){
		$crud = new grocery_CRUD();
	    $crud->set_subject('Trivia');
	    
	    $crud->columns('activar','pregunta','opciones','usado');
		$crud->set_table('preguntas');
		$crud->where('sala',1);
		
		$crud->unset_print();
		$crud->unset_read();
		
		$crud->callback_column('activar',array($this,'_callback_webpage_url'));
		$crud->callback_column('pregunta',array($this,'_callback_pregunta'));
		$crud->callback_column('usado',array($this,'_callback_usado'));
		$crud->callback_after_insert(array($this, 'trivia_sala_1_after_insert'));
		
		
		$crud->add_fields(array('pregunta','opciones'));
		$crud->edit_fields(array('pregunta','opciones'));
		
		$crud->unset_texteditor('opciones','full_text');
		
	    $output = $crud->render();
		$params["crud"]=true;
		$this->load->view('admin/header');
		$this->load->view('admin/crud.php',$output);
		$this->load->view('admin/footer',$params);
    }
    function trivia_sala_1_after_insert($post_array,$primary_key){
	    $up_preguntas = array( "sala" => 1);
	    $this->db->where('id', $primary_key);
	    $this->db->update('preguntas',$up_preguntas);
	    return true;
	}
	function trivia_sala_2_after_insert($post_array,$primary_key){
	    $up_preguntas = array( "sala" => 2);
	    $this->db->where('id', $primary_key);
	    $this->db->update('preguntas',$up_preguntas);
	    return true;
	}
    public function trivia_sala_2(){
		$crud = new grocery_CRUD();
	    $crud->set_subject('Trivia');
	    
	    $crud->columns('activar','pregunta','opciones','usado');
		$crud->set_table('preguntas');
		$crud->where('sala',2);
		$crud->unset_print();
		$crud->unset_read();
		
		$crud->callback_column('activar',array($this,'_callback_webpage_url'));
		$crud->callback_column('pregunta',array($this,'_callback_pregunta'));
		$crud->callback_column('usado',array($this,'_callback_usado'));
		$crud->callback_after_insert(array($this, 'trivia_sala_2_after_insert'));
		
		$crud->unset_texteditor('opciones','full_text');
		
		$crud->add_fields(array('pregunta','opciones'));
		$crud->edit_fields(array('pregunta','opciones'));
		
	    $output = $crud->render();
		$params["crud"]=true;
		$this->load->view('admin/header');
		$this->load->view('admin/crud.php',$output);
		$this->load->view('admin/footer',$params);
    }
    public function respuestas(){
		$crud = new grocery_CRUD();
	    $crud->set_subject('Respuestas');
	    
	    //$crud->columns('activar','pregunta','opciones','usado');
		$crud->set_table('trivia_respuestas');
		$crud->unset_print();
		$crud->unset_read();
	
		$crud->unset_texteditor('opciones','full_text');
		
	    $output = $crud->render();
		$params["crud"]=true;
		$this->load->view('admin/header');
		$this->load->view('admin/crud.php',$output);
		$this->load->view('admin/footer',$params);
    }
    
    public function expositores(){
		$crud = new grocery_CRUD();
	    $crud->set_subject('Expositores');
		$crud->set_table('expositores');
		$crud->columns('activar','expositor','activo');
		$crud->unset_print();
		$crud->unset_read();
		$crud->unset_add();
		$crud->unset_delete();
		$crud->unset_export();
		$crud->edit_fields(array("expositor"));
		
		//$crud->callback_field('phone',array($this,'example_callback'));
		$crud->callback_field('activo',array($this,'field_callback_1'));
		$crud->callback_column('activar',array($this,'_callback_webpage_url'));
		
		//$crud->unset_texteditor('opciones','full_text');
		
	    $output = $crud->render();
		$params["crud"]=true;
		$this->load->view('admin/header');
		$this->load->view('admin/crud.php',$output);
		$this->load->view('admin/footer',$params);
    }
    private function field_callback_1($value = '', $primary_key = null)
	{
	    return '*';
	    //+30 <input type="text" maxlength="50" value="'.$value.'" name="phone" style="width:462px">';
	}
    public function _callback_webpage_url($value, $row)
	{
	  return "<a href='javascript:activarPregunta(".$row->id.")'>Activar</a>";
	}
	public function _callback_usado($value, $row)
	{
		if(intval($value)==1){
			return "<b style='color:#D33F30;'>SI<b/>";
		}else{
			return "NO";
		}
		
	}
	public function _callback_pregunta($value, $row)
	{
		if(intval($row->activo)==1){
			return "<b style='color:#D33F30;'>".$value."<b/>";
		}else{
			return $value;
		}
		
	}
	
	public function activar($id,$sala){
		$sql = "UPDATE preguntas SET activo=0 WHERE sala='$sala'";
		$query = $this->db->query($sql);
		
		$sql = "UPDATE preguntas SET activo='1',usado='1' WHERE id='$id'";
		$query = $this->db->query($sql);
		if(intval($sala)==1){
			redirect("admin/trivia_sala_1");
		}else{
			redirect("admin/trivia_sala_2");
		}
		
	}



}
