<?php
class Cms extends CI_Controller
{

    public function __construct(){
		parent::__construct();

    }
    public function index() {
	    if($this->session->userdata('is_logged_in')){
		    $perfil = $this->session->userdata('perfil');
			if($perfil=="Administrador"){
				redirect('cms/inicio');
				exit();
			}
		}
	    $this->load->view('admin/login');
    }
    public function inicio() {
	    if(!$this->session->userdata('is_logged_in')){
			redirect('cms');
		}
		$perfil = $this->session->userdata('perfil');
		if($perfil=="Usuario"){
			redirect('cms');
		}
	    $params = array();
		$params["no_crud"] = true;
		$this->load->view('admin/header');
		$this->load->view('admin/home.php');
		$this->load->view('admin/footer', $params);
    }
    
}