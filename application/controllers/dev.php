<?php
class Dev extends CI_Controller{
    public function __construct(){
		parent::__construct();
		parent::__construct();
		$this->load->helper('url');
		if(!$this->session->userdata('is_logged_in')){
			$this->session->set_userdata("redireccion", base64_encode(uri_string()));
			redirect('login');
		}
    }
    function _remap($param) {
        $this->index($param);
    }

    public function index($token="") {
	    $data = array();
	    $data["token"] = $token;
		$this->load->view('home',$data);
		//$this->load->view('pre');
	}

}
