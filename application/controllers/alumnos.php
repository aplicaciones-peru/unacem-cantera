<?php
class Alumnos extends CI_Controller
{

    public function __construct(){
		parent::__construct();

    }
    public function index() {

	}
	public function programa_profesional(){
	    $this->load->view('header',array("tipo"=>"gris_claro","seccion"=>"programa_profesional"));
	    $this->load->view('programa_profesional');
	    $this->load->view('footer');
    }
    
    public function postula(){
	    $this->load->view('header');
	    $this->load->view('postula');
	    $this->load->view('footer');
    }
    public function ingreso(){
	    $this->load->view('header',array("tipo"=>"blanco","seccion"=>"programa_profesional"));
	    $this->load->view('ingreso',array("tipo"=>"blanco"));
	    $this->load->view('footer');
    }
    public function graduados(){
	    $this->load->view('header',array("tipo"=>"blanco","seccion"=>"programa_profesional"));
	    $this->load->view('graduados');
	    $this->load->view('footer');
    }
    public function avance(){
	    
	    $this->load->view('header',array("tipo"=>"rojo","seccion"=>"programa_profesional"));
	    $this->load->view('avance');
	    $this->load->view('footer');
    }

    
    
    
}
