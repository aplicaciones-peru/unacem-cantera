<?php
class Instrucciones extends CI_Controller{
    public function __construct(){
		parent::__construct();
		parent::__construct();
		$this->load->helper('url');
		if(!$this->session->userdata('is_logged_in')){
			$this->session->set_userdata("redireccion", base64_encode(uri_string()));
			redirect('login');
		}
    }
    public function index() {
		$this->load->view('instrucciones');
	}


}
