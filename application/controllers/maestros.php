<?php
class Maestros extends CI_Controller
{

    public function __construct(){
		parent::__construct();

    }
    public function index() {
	    //$this->load->view('inicio_temp');
	    //$this->load->view('selector');
	    $this->load->view('contruyendo_maestros_unacem');
	}
	public function inicio() {
	    $this->load->view('selector');
	}
    public function selector(){
	    $this->load->view('header_external');
	    $this->load->view('selector');
	    $this->load->view('footer');
    }
    public function contruyendo_maestros_unacem(){
	    
	    //$this->load->view('header');
	    $this->load->view('contruyendo_maestros_unacem');
	    //$this->load->view('footer');
	    
    }
    public function maestros(){
	    $this->load->view('contruyendo_maestros_unacem');
    }
    
    
     public function canal_empresas(){
	    echo "canal_empresas";
    }
    
    
    
    public function programa_profesional(){
	    $this->load->view('header',array("tipo"=>"gris_claro","seccion"=>"programa_profesional"));
	    $this->load->view('programa_profesional');
	    $this->load->view('footer');
    }
    
    
    public function dequesetrata(){
	    $this->load->view('header');
	    $this->load->view('dequesetrata');
	    $this->load->view('footer');
    }
     public function estructura(){
	    $this->load->view('header');
	    $this->load->view('estructura');
	    $this->load->view('footer');
    }
    
    public function cursos(){
	    $this->load->view('header',array("tipo"=>"blanco","seccion"=>"programa_profesional"));
	    $this->load->view('cursos');
	    $this->load->view('footer');
    }
    public function soluciones_constructivas(){
	    $this->load->view('header',array("tipo"=>"gris_claro","seccion"=>"soluciones_constructivas"));
	    $this->load->view('soluciones_constructivas');
	    $this->load->view('footer');
    }
    
    
    public function calculadora($id=""){
	    $actual_link = (isset($_SERVER['HTTPS']) && $_SERVER['HTTPS'] === 'on' ? "https" : "http") . "://$_SERVER[HTTP_HOST]$_SERVER[REQUEST_URI]";
		if(!$this->session->userdata('is_logged_in')){
			redirect('login?r='.$actual_link);
        }
        
	    $this->load->view('header',array("tipo"=>"blanco","seccion"=>"soluciones_constructivas"));
	    if($id!=""){
		    $params = array();
		    $params["id"] = $id;
		    $this->load->view('calculadora_int',$params);
	    }else{
		    $this->load->view('calculadora');
	    }
	    $this->load->view('footer');
    }
    public function calculadora_int(){
	    $this->load->view('header',array("tipo"=>"blanco","seccion"=>"soluciones_constructivas"));
	    $this->load->view('calculadora_int');
	    $this->load->view('footer');
    }
    public function planos(){
	    $actual_link = (isset($_SERVER['HTTPS']) && $_SERVER['HTTPS'] === 'on' ? "https" : "http") . "://$_SERVER[HTTP_HOST]$_SERVER[REQUEST_URI]";
		if(!$this->session->userdata('is_logged_in')){
			redirect('login?r='.$actual_link);
        }
	    $this->load->view('header',array("tipo"=>"gris_oscuro","seccion"=>"soluciones_constructivas"));
	    $this->load->view('planos');
	    $this->load->view('footer');
    }
	public function tablas_equivalencia(){
	    $this->load->view('header',array("tipo"=>"gris_oscuro","seccion"=>"soluciones_constructivas"));
	    $this->load->view('tablas_equivalencia');
	    $this->load->view('footer');
    }

	public function manual_construccion(){
	    $this->load->view('header');
	    $this->load->view('manual_construccion');
	    $this->load->view('footer');
    }

	public function buenas_practicas(){
	    $this->load->view('header');
	    $this->load->view('buenas_practicas');
	    $this->load->view('footer');
    }
    
    
}
