<?php
class Email extends CI_Controller{
    public function __construct(){
		parent::__construct();
		$this->load->helper('url');
    }
	public function enviar($curso) {
	    header('Content-Type: application/json');
		$this->load->library('email');
		$this->email->from('contacto@unacem.com.pe', 'Contacto');
		$usuario = $this->session->userdata('id');
		$email = "";
		$sql = "SELECT email FROM membership WHERE id='$usuario'";
	    $query = $this->db->query($sql);
		foreach ($query->result() as $row) {
			$email = $row->email;
		}
		$this->email->to($email);
		$this->email->subject('Envio Constancia Unacem Cantera');
		$this->email->set_mailtype("html");
		$url = base_url()."exports/pdf/certificado_save.php?u=".$this->session->userdata('id')."&curso=".$curso;
		file_get_contents($url);
		$file = "/var/www/html/exports/pdf/files/certificado_".$usuario.$curso.".pdf";
		$this->email->attach($file); 
		$msg = 'Hola Maestro, gracias por tu empeño y participación en las capacitaciones de Unacem Cantera.
Te hacemos llegar la constancia de participación del curso que acabas de desarrollar, gracias por tu interés y esperamos seguir contando contigo.';
		$this->email->message($msg);
		$this->email->send();
		$response = array();
		$response["status"] = "OK";
		$response["email"] = $email;
		echo json_encode($response);
		
	} 


}
