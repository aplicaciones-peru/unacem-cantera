<?php

class Login extends CI_Controller
{

    /**
     * Check if the user is logged in, if he's not,
     * send him to the login page
     * @return void
     */
    public function __construct()
    {
		parent::__construct();
		if($this->session->userdata('is_logged_in')){
			redirect('/maestros/programa_profesional');
		}
    }
    public function index() {
		$this->load->view('header',array("tipo"=>"gris_oscuro","seccion"=>"programa_profesional"));
	    $this->load->view('login');
	    $this->load->view('footer');
	    
	}

}