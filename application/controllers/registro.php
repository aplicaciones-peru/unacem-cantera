<?php

class Registro extends CI_Controller
{

    /**
     * Check if the user is logged in, if he's not,
     * send him to the login page
     * @return void
     */
     public function __construct()
    {
		parent::__construct();
		if($this->session->userdata('is_logged_in')){
			redirect('/alumnos/programa_profesional');
		}
    }
    public function index() {
		$this->load->view('header',array("tipo"=>"gris_oscuro","seccion"=>"programa_profesional"));
	    $this->load->view('registro');
	    $this->load->view('footer');
	    
	}
	public function completado() {
		$this->load->view('header',array("tipo"=>"gris_oscuro","seccion"=>"programa_profesional"));
		$this->load->view('registro_completado');
		 $this->load->view('footer');
	}
    public function save(){
	    
	    
	    $sql = "SELECT COUNT(*) as total FROM usuario WHERE email='".$_POST['email']."'";
	    $query = $this->db->query($sql);
		foreach ($query->result() as $row) {
			if(intval($row->total)>0){
				redirect('registro?error=email_registrado');
				exit();
			}
		}
		
	   // echo "test";
	   // if(isset($_POST['email'])){
			$nombres 		= $_POST['nombres'];
			$apellidos 		= $_POST['apellidos'];
			$dni 			= $_POST['dni'];
			$email 			= $_POST['email'];
			$telefono 		= $_POST['telefono'];
			$direccion 		= $_POST['direccion'];
			$distrito 		= $_POST['distrito'];
			$password 		= $_POST['password'];

			$insert_data = array();

			//$password = strtolower($this->generate_string(5));

			//$insert_data["user_name"] 		= $email;
			$insert_data["password"] 		= md5($password);
			$insert_data["password_visible"] = $password;
			$insert_data["nombres"] 		= $nombres;
			$insert_data["apellidos"] 		= $apellidos;
			$insert_data["dni"] 			= $dni;
			$insert_data["email"] 			= $email;
			$insert_data["telefono"] 		= $telefono;
			$insert_data["direccion"] 		= $direccion;
			$insert_data["distrito"] 		= $distrito;
			$insert_data["nro_activacion"] 	= 2;
			//$insert_data["perfil"] 			= 'Usuario';
			$this->db->insert("usuario", $insert_data);
			$id = $this->db->insert_id();
			/*
			$this->load->library('email');
			$this->email->from('contacto@lagranbusqueda.com', 'La Gran Búsqueda HP');
			$this->email->to($email);
			$this->email->subject('La Gran Búsqueda HP - Gracias por registrarte');
			$this->email->set_mailtype("html");
			$msg_alt = "Disfruta de esta nueva experiencia participando este 18 y 19 de Julio.\n";
			$msg_alt .= "Los horarios para participar en ambos días es de 17:00 a 18:00 hrs.\n";
			$msg_alt .= " Averigua tantas pistas como puedas, recolecta los mapas escondidos y conviértete en uno de los posibles ganadores de nuestros premios.\n";
			$msg_alt .= "El viaje comienza el Sábado 18 a las 17:00 horas.";

			$msg = '<center>
			<img alt="Gracias por registrarte" height="1072" src="https://lagranbusqueda.com/resource/lgbhp1819072020.jpg" style="width:693px;height:1072px" width="693" class="CToWUd"><br/><font size="1" color="#000000" face="ARIAL">
Este mensaje contiene gráficos.<br/>Si no ve los gráficos, <a href="https://lagranbusqueda.com/resource/lgbhp1819072020.jpg" target="_blank">
Haga clic aquí para ver</a>.</font><font face="ARIAL" color="#808080" size="2"><br></font></center>';
			
			$this->email->message($msg);
			//$this->email->set_alt_message($msg_alt);
			$this->email->send();
			*/
			$redireccion = $this->session->userdata("redireccion");
			$this->session->set_userdata("redireccion", false);

			$data = array(
				'user_name' => $email,
				'nombres' => "$nombres",
				'id' => $id,
				// 'perfil' => $row->perfil,
				'is_logged_in' => true
			);
			$this->session->set_userdata($data);

			if ($redireccion) {
				//echo "redireccion".$redireccion;
				redirect(base64_decode($redireccion));
			}
			else{
				redirect('inicio');
			}

			//$this->sentEmail($email,$password,$nombres);
			// $this->load->view('registro_completado',$datos);
		//}
    }
}
