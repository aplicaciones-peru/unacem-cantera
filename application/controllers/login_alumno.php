<?php

class Login_alumno extends CI_Controller
{

    /**
     * Check if the user is logged in, if he's not,
     * send him to the login page
     * @return void
     */
    public function __construct()
    {
		parent::__construct();

    }
    public function index() {
		$this->load->view('header',array("tipo"=>"gris_oscuro","seccion"=>"programa_profesional"));
	    $this->load->view('login_alumno');
	    $this->load->view('footer');
	    
	}

}