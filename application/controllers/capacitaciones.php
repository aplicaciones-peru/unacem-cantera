<?php
class Capacitaciones extends CI_Controller
{

    public function __construct(){
		parent::__construct();

    }
    public function index() {
	    
	    
	    $this->load->view('header',array("tipo"=>"gris_claro","seccion"=>"capacitaciones"));
	    $this->load->view('capacitaciones',array("tipo"=>"gris_claro"));
	    $this->load->view('footer');
	}
	
	public function quiz($slug) {
		
		$actual_link = (isset($_SERVER['HTTPS']) && $_SERVER['HTTPS'] === 'on' ? "https" : "http") . "://$_SERVER[HTTP_HOST]$_SERVER[REQUEST_URI]";
		if(!$this->session->userdata('is_logged_in')){
			redirect('login?r='.$actual_link);
        }
        
        
		$cuestionario = array();
		$descripcion = "";
		if($slug=="normativas"){
			$descripcion = "Nuestra responsabilidad como empresa referente en el sector constructivo es velar en todo momento por la seguridad, integridad y salud de los trabajadores de construcción. Por eso, ponemos a tu disposición la normativa para afrontar los proyectos post Covid-19, para conocer los nuevos protocolos sanitarios que deben cumplirse de manera obligatoria en todos los proyectos de construcción. De esta forma podrás vigilar, prevenir y controlar la propagación del Covid-19 en tus obras de construcción.";
			$pregunta = array();
			$pregunta["pregunta"]="1. ¿Cuál es la resolución Ministerial que indica el inicio gradual e incremental de las actividades en la Reanudación de las Actividades frente a la propagación del Covid-19?";
			$opciones = array();
			$opciones[0] = "RM 085";
			$opciones[1] = "RM 087";
			$opciones[2] = "El protocolo sanitario";
			$opciones[3] = "El plan de vigilancia";
			$pregunta["opciones"]=$opciones;
			$pregunta["c"]=0;
			array_push($cuestionario, $pregunta);
			
			$pregunta = array();
			$pregunta["pregunta"]="2. Los lineamientos son aplicados:";
			$opciones = array();
			$opciones[0] = "Solo en Piura";
			$opciones[1] = "Solo para el sur del país";
			$opciones[2] = "A nivel nacional.";
			$opciones[3] = "Solo para edificaciones de gran altura";
			$pregunta["opciones"]=$opciones;
			$pregunta["c"]=1;
			array_push($cuestionario, $pregunta);
			
			$pregunta = array();
			$pregunta["pregunta"]="3. Cuando el RM N°087-2020-VIVIENDA habla de Construcción se refiere a: ";
			$opciones = array();
			$opciones[0] = "La construcción de casas y habilitaciones urbanas";
			$opciones[1] = "Las obras de edificación nueva ";
			$opciones[2] = "Obras de ampliación, reconstrucción, refacción, remodelación, acondicionamiento ";
			$opciones[3] = "Todas las anteriores ";
			$pregunta["opciones"]=$opciones;
			$pregunta["c"]=2;
			array_push($cuestionario, $pregunta);
			
			$pregunta = array();
			$pregunta["pregunta"]="4. Sobre la movilización del personal hasta la obra y su ingreso se exige: ";
			$opciones = array();
			$opciones[0] = "Que sea en transporte publico utilizado a un 85% de su capacidad ";
			$opciones[1] = "Que sea en transporte privado utilizado a un 50% de su capacidad ";
			$opciones[2] = "Ingreso a obra por intervalos de 30 minutos ";
			$opciones[3] = "La B y la C";
			$pregunta["opciones"]=$opciones;
			$pregunta["c"]=3;
			array_push($cuestionario, $pregunta);
			
			$pregunta = array();
			$pregunta["pregunta"]="5. Al ingresar a una obra, los trabajadores deberán primero pasar por : ";
			$opciones = array();
			$opciones[0] = "La Zona de Control Previo; luego por la Zona de Desinfección; luego acudirán a los Vestuarios y, posteriormente, recién entrarán a la Zona de Trabajo. ";
			$opciones[1] = "La Zona de Desinfección; luego por la Zona de Control Previo; luego acudirán a los Vestuarios y, posteriormente, recién entrarán a la Zona de Trabajo.";
			$opciones[2] = "La Zona de Trabajo; luego por la Zona de Control Previo; luego acudirán a los Vestuarios y, posteriormente, recién entrarán a la Zona de Desinfección ";
			$opciones[3] = "Ninguna de las anteriores.";
			$pregunta["opciones"]=$opciones;
			$pregunta["c"]=0;
			array_push($cuestionario, $pregunta);
		}
		if($slug=="calculo"){
			$descripcion = "El presupuesto de construcción es un documento que resume el cálculo anticipado del costo de una obra; y es importante porque te permite formalizar tu servicio con cada cliente. Por eso, ponemos a tu disposición este paso a paso, en donde verás cómo se elabora un presupuesto de construcción y la información que se debe tomar en cuenta para realizarlo. De esta forma, tendrás una idea más clara de la ganancia que obtendrás por cada proyecto a tu cargo.";
			$pregunta = array();
			$pregunta["pregunta"]="1. ¿Qué es un presupuesto?";
			$opciones = array();
			$opciones[0] = "Es tomar medidas de los planos.";
			$opciones[1] = "Es estimar el costo de los materiales.";
			$opciones[2] = "Es el monto que tiene el cliente para su construcción.";
			$opciones[3] = "Es calcular de manera anticipada el costo de construcción.";
			$opciones[4] = "Es un documento que consigo en internet.";
			$pregunta["opciones"]=$opciones;
$pregunta["c"]=0;
			array_push($cuestionario, $pregunta);
			
			$pregunta = array();
			$pregunta["pregunta"]="2. ¿Qué es metrar?";
			$opciones = array();
			$opciones[0] = "Es dibujar planos.";
			$opciones[1] = "Es tomar medidas de los planos a construir, en función a las partidas que se describen en la plantilla del presupuesto. ";
			$opciones[2] = "Son las unidades con que se mide cada partida del presupuesto.";
			$opciones[3] = "Son precios de la mano de obra.";
			$opciones[4] = "Son rendimientos de los APU´s.";
			$pregunta["opciones"]=$opciones;
$pregunta["c"]=0;
			array_push($cuestionario, $pregunta);
			
			$pregunta = array();
			$pregunta["pregunta"]="3. Para elaborar un Presupuesto, ¿es necesario saber el plazo de la obra?";
			$opciones = array();
			$opciones[0] = "No, los tarifas de los equipos siempre son las mismos.";
			$opciones[1] = "Si, para saber cuando me pagarán.";
			$opciones[2] = "Si, para poder metrar.";
			$opciones[3] = "Si, para estimar el tiempo que contrataré al personal de supervisión de la obra.";
			$opciones[4] = "Todas las anteriores.";
			$pregunta["opciones"]=$opciones;
$pregunta["c"]=0;
			array_push($cuestionario, $pregunta);
			
			$pregunta = array();
			$pregunta["pregunta"]="4. Es un afirmación correcta:";
			$opciones = array();
			$opciones[0] = "Un presupuesto es el mismo para cualquier obra.";
			$opciones[1] = "No es necesario presupuestar, el cliente me paga lo justo por mi trabajo.";
			$opciones[2] = "Todos los precios unitarios son iguales.";
			$opciones[3] = "Para elaborar un presupuesto, es necesario conocer la ubicación de la obra.";
			$opciones[4] = "Ninguna de las anteriores.";
			$pregunta["opciones"]=$opciones;
$pregunta["c"]=0;
			array_push($cuestionario, $pregunta);
			
			$pregunta = array();
			$pregunta["pregunta"]="El presupuesto esta conformado por…";
			$opciones = array();
			$opciones[0] = "Los metrados.";
			$opciones[1] = "Los Precios Unitarios.";
			$opciones[2] = "El costo de la mano de obra, equipos, materiales y de las herramientas.";
			$opciones[3] = "El rendimiento de la mano de obra y los equipos.";
			$opciones[4] = "El costo directo, los gastos generales y la utilidad.";
			$pregunta["opciones"]=$opciones;
$pregunta["c"]=0;
			array_push($cuestionario, $pregunta);
			

		}

		if($slug=="tendencias"){
			$descripcion = "Para ser competitivos en la rama de la construcción es importante evaluar el nivel tecnológico actual, para así incorporar nuevas tendencias que permitan obtener mejores resultados y hagan eficiente el proceso constructivo. Por eso ponemos a tu disposición información sobre la evolución de la construcción en nuestro país y las últimas tendencias técnologicas que se están aplicando en el Perú y el mundo.";
			$pregunta = array();
			$pregunta["pregunta"]="1. Una de las ventajas para el asentado de ladrillo de concreto es: ";
			$opciones = array();
			$opciones[0] = "Menor consumo de mortero de asentado. ";
			$opciones[1] = "Necesita bastante agua";
			$opciones[2] = "Necesita mucho tarrajeo";
			$opciones[3] = "Asentado lento";
			$opciones[4] = "Ninguna";
			$pregunta["opciones"]=$opciones;
$pregunta["c"]=0;
			array_push($cuestionario, $pregunta);
			
			$pregunta = array();
			$pregunta["pregunta"]="2. ¿En qué consiste el BIM?";
			$opciones = array();
			$opciones[0] = "Son planos en AutoCad";
			$opciones[1] = "Sirve para dibujar los planos de la construcción en la escala menor";
			$opciones[2] = "Simulación virtual de proyectos de ingeniería o arquitectura a escala";
			$opciones[3] = "Ninguna";
			$pregunta["opciones"]=$opciones;
$pregunta["c"]=0;
			array_push($cuestionario, $pregunta);
			
			$pregunta = array();
			$pregunta["pregunta"]="3. ¿Cuál es el beneficio del uso de los robots en la construcción?:";
			$opciones = array();
			$opciones[0] = "a. Permite que la mano de obra trabaje más rápido";
			$opciones[1] = "b. Permite mejorar el rendimiento de la mano de obra, optimizando tiempos y costos.";
			$opciones[2] = "c. Evita que se use agua en el curado de concreto";
			$opciones[3] = "d. Se encargan de los acabados finales en una obra";
			$pregunta["opciones"]=$opciones;
$pregunta["c"]=0;
			array_push($cuestionario, $pregunta);
			
			$pregunta = array();
			$pregunta["pregunta"]="4. ¿Qué ventaja nos brinda los Encofrados metálicos en los techos?:";
			$opciones = array();
			$opciones[0] = "Mayor tiempo de encofrado";
			$opciones[1] = "Difícil de maniobrar";
			$opciones[2] = "Despedicio de Concreto en el vaciado";
			$opciones[3] = "Mantener horizontal y nivelado el techo";
			$pregunta["opciones"]=$opciones;
$pregunta["c"]=0;
			array_push($cuestionario, $pregunta);
			
			$pregunta = array();
			$pregunta["pregunta"]="5. ¿Qué es la albañilera Armada?:";
			$opciones = array();
			$opciones[0] = "Es la construcción de ladrillos con columna de amarre";
			$opciones[1] = "Es la construcción de una vivienda trabajada solo con ladrillos de arcilla";
			$opciones[2] = "Albañilería reforzada interiormente con varillas de acero distribuidas vertical y horizontalmente e integrada mediante concreto líquido.";
			$opciones[3] = "Ninguna la anteriores";
			$pregunta["opciones"]=$opciones;
$pregunta["c"]=0;
			array_push($cuestionario, $pregunta);
			/*
			$pregunta = array();
			$pregunta["pregunta"]="PREGUNTA";
			$opciones = array();
			$opciones[0] = "OPTION";
			$opciones[1] = "OPTION";
			$opciones[2] = "OPTION";
			$opciones[3] = "OPTION";
			$pregunta["opciones"]=$opciones;
$pregunta["c"]=0;
			array_push($cuestionario, $pregunta);*/
		}


		if($slug=="como_mejorar"){
			$descripcion = "En los últimos años, el concepto de productividad ha ganado importancia en la industria de la construcción, ya que permite medir la eficiencia del trabajo realizado. Por eso, ponemos a tu disposición la siguiente información, en donde te mostraremos cómo administrar de manera eficiente los recursos y así mejorar la productividad de tus proyectos de construcción. De esta forma, podrás planificar y optimizar los tiempos y costos del proyecto, para hacerlo más rentable.";
			$pregunta = array();
			$pregunta["pregunta"]="1. ¿Cuál es el factor principal que influye en el exceso de costos en los proyectos de construcción?";
			$opciones = array();
			$opciones[0] = "La productividad";
			$opciones[1] = "Los retrabajos";
			$opciones[2] = "La mala planificación";
			$opciones[3] = "La b y la c";
			$opciones[4] = "Ninguna de las anteriores";
			$pregunta["opciones"]=$opciones;
$pregunta["c"]=0;
			array_push($cuestionario, $pregunta);
			$pregunta = array();
			$pregunta["pregunta"]="2. ¿A qué reemplazan las viguetas pretensadas?";
			$opciones = array();
			$opciones[0] = "A las viguetas tradicionales";
			$opciones[1] = "A las Vigas principales";
			$opciones[2] = "A las vigas secundarias";
			$opciones[3] = "A los empalmes de viga con columnas";
			$opciones[4] = "Ninguna de las anteriores";
			$pregunta["opciones"]=$opciones;
$pregunta["c"]=0;
			array_push($cuestionario, $pregunta);
			$pregunta = array();
			$pregunta["pregunta"]="3. ¿Cuál de las siguientes opciones de software sirve para mejorar la productividad?";
			$opciones = array();
			$opciones[0] = "PlanGrid";
			$opciones[1] = "S10";
			$opciones[2] = "Excel";
			$opciones[3] = "Autocad";
			$opciones[4] = "Ninguno";
			$pregunta["opciones"]=$opciones;
$pregunta["c"]=0;
			array_push($cuestionario, $pregunta);
			$pregunta = array();
			$pregunta["pregunta"]="4. El uso de prefabricados en la construcción nos ayudan a:";
			$opciones = array();
			$opciones[0] = "Disminuir los costos";
			$opciones[1] = "Aumentar la calidad";
			$opciones[2] = "Reducir los tiempos de ejecución";
			$opciones[3] = "Son correctas a y b";
			$opciones[4] = "Son correctas a, b y c";
			$pregunta["opciones"]=$opciones;
$pregunta["c"]=0;
			array_push($cuestionario, $pregunta);
			$pregunta = array();
			$pregunta["pregunta"]="5. ¿Para qué se usaban los Diagramas de Barras?";
			$opciones = array();
			$opciones[0] = "Para disminuir los costos de obra";
			$opciones[1] = "Para realizar cronogramas de tiempos de ejecución";
			$opciones[2] = "Reducir los tiempos de ejecución";
			$opciones[3] = "Son correctas la b y c";
			$pregunta["opciones"]=$opciones;
$pregunta["c"]=0;
			array_push($cuestionario, $pregunta);
		}
		if($slug=="riesgos"){
			$descripcion = "Generalmente nos encontramos con una serie de errores en calidad, eficiencia y tiempo de ejecución al iniciar y/o durante un proyecto de construcción. Por eso, es importante conocer los principales riesgos y errores que se comenten en la construcción, y sobretodo, aprender a cómo reducirlos. De esta manera, podrás garantizar el bienestar de tu personal y los correctos procedimientos de seguridad.";
			$pregunta = array();
			$pregunta["pregunta"]="1. ¿Cuál es el beneficio de planificar una obra?";
			$opciones = array();
			$opciones[0] = "El conocimiento anticipado de la obra";
			$opciones[1] = "Los retrabajos";
			$opciones[2] = "Saber que tiene un buen jefe de obra";
			$opciones[3] = "La organización de una obra";
			$opciones[4] = "Ninguna de las anteriores";
			$pregunta["opciones"]=$opciones;
$pregunta["c"]=0;
			array_push($cuestionario, $pregunta);
			$pregunta = array();
			$pregunta["pregunta"]="2. ¿Qué es el riesgo?";
			$opciones = array();
			$opciones[0] = "Es la probabilidad de que una amenza se convierta en peligro o desastre dentro de la obra. ";
			$opciones[1] = "Es el potencial de hacer daño o pérdida ";
			$opciones[2] = "No hacer nada";
			$opciones[3] = "Ninguna de las anteriores";
			$pregunta["opciones"]=$opciones;
$pregunta["c"]=0;
			array_push($cuestionario, $pregunta);
			$pregunta = array();
			$pregunta["pregunta"]="3. ¿En todo tipo de obra el precio del cemento es igual?";
			$opciones = array();
			$opciones[0] = "Varía de acuerdo a la zona de trabajo";
			$opciones[1] = "Sí, es igual por ser de la misma fábrica";
			$opciones[2] = "No es importante el precio, depende de cómo trabajas";
			$opciones[3] = "Solo si son diferentes tipos de cemento";
			$opciones[4] = "Ninguno";
			$pregunta["opciones"]=$opciones;
$pregunta["c"]=0;
			array_push($cuestionario, $pregunta);
			$pregunta = array();
			$pregunta["pregunta"]="4. ¿Quién es el actor más importante en un Proyecto de Edificación?";
			$opciones = array();
			$opciones[0] = "El Propietario";
			$opciones[0] = "El Proyectista";
			$opciones[1] = "Proyectista y Constructor";
			$opciones[2] = "Propietario y Proyectista";
			$opciones[3] = "Propietario, Proyectista y Ejecutor";
			$pregunta["opciones"]=$opciones;
$pregunta["c"]=0;
			array_push($cuestionario, $pregunta);
			$pregunta = array();
			$pregunta["pregunta"]="5. ¿Qué implica la seguridad en la obra?";
			$opciones = array();
			$opciones[0] = "Entregar los EPP";
			$opciones[1] = "Charlas diarias";
			$opciones[2] = "Pasar los exámenes médicos";
			$opciones[3] = "Conocer la diferencia entre un riesgo y un peligro";
			$opciones[4] = "Todas las anteriores";
			$pregunta["opciones"]=$opciones;
$pregunta["c"]=0;
			array_push($cuestionario, $pregunta);
		}

	    $this->load->view('header',array("tipo"=>"gris_intermedio","seccion"=>"capacitaciones"));
	    $params = array();
	    $params["slug"] = $slug;
	    $params["descripcion"] = $descripcion;
	    $params["cuestionario"] = $cuestionario;
	    $this->load->view('quiz',$params);
	    $this->load->view('footer_btns');
	}
	public function finalizado(){
	    $this->load->view('header');
	    $params = array();
	    $params["titulo"] = "Normativas para afrontar los proyectos Post COVID.";
	    $params["curso"] = 1;
	    $this->load->view('finalizado',$params);
	    $this->load->view('footer_btns');
    }
    public function normativas($finalizado=""){
		$actual_link = (isset($_SERVER['HTTPS']) && $_SERVER['HTTPS'] === 'on' ? "https" : "http") . "://$_SERVER[HTTP_HOST]$_SERVER[REQUEST_URI]";
		if(!$this->session->userdata('is_logged_in')){
			redirect('login?r='.$actual_link);
        }
	    $params = array();
	    $params["titulo"] = "Normativa vigente para la reanudación de actividades frente a la propagación de Covid-19.";
	    $params["curso"] = 1;
	    $params["slug"] = "normativas";
	    $params["certificado_directo"] = false;
	    $params["tipo"] = "gris_oscuro";
	    $params["descripcion"]  = "Nuestra responsabilidad como empresa referente en el sector constructivo es velar en todo momento por la seguridad, integridad y salud de los trabajadores de construcción. Por eso, ponemos a tu disposición la normativa para afrontar los proyectos post Covid-19, para conocer los nuevos protocolos sanitarios que deben cumplirse de manera obligatoria en todos los proyectos de construcción. De esta forma podrás vigilar, prevenir y controlar la propagación del Covid-19 en tus obras de construcción.";
	    if($finalizado==""){
		    $this->load->view('header',array("tipo"=>"gris_oscuro","seccion"=>"capacitaciones"));
		    $this->load->view('capacitacion',$params);
	    }else{
		    $this->load->view('header',array("tipo"=>"gris_oscuro","seccion"=>"capacitaciones"));
		    $this->load->view('finalizado',$params);
	    }
	    
	    $this->load->view('footer_btns');
    }
    public function calculo($finalizado=""){
		$actual_link = (isset($_SERVER['HTTPS']) && $_SERVER['HTTPS'] === 'on' ? "https" : "http") . "://$_SERVER[HTTP_HOST]$_SERVER[REQUEST_URI]";
		if(!$this->session->userdata('is_logged_in')){
			redirect('login?r='.$actual_link);
        }
	    $params = array();
	    $params["titulo"] = "Elaboración de presupuestos de obras de construcción. ";
	    $params["curso"] = 2;
	    $params["slug"] = "calculo";
	    $params["certificado_directo"] = false;
	    $params["descripcion"]  = "El presupuesto de construcción es un documento que resume el cálculo anticipado del costo de una obra; y es importante porque te permite formalizar tu servicio con cada cliente. Por eso, ponemos a tu disposición este paso a paso, en donde verás cómo se elabora un presupuesto de construcción y la información que se debe tomar en cuenta para realizarlo. De esta forma, tendrás una idea más clara de la ganancia que obtendrás por cada proyecto a tu cargo.";
	    if($finalizado==""){
		    $this->load->view('header',array("tipo"=>"gris_oscuro","seccion"=>"capacitaciones"));
		    $this->load->view('capacitacion',$params);
	    }else{
		    $this->load->view('header',array("tipo"=>"gris_oscuro","seccion"=>"capacitaciones"));
		    $this->load->view('finalizado',$params);
	    }
	    $this->load->view('footer_btns');
    }
	public function tendencias($finalizado=""){
	   
	    $params = array();
	    $params["titulo"] = "Tendencias Tecnológicas para construir mejor. ";
	    $params["slug"] = "tendencias";
	    $params["certificado_directo"] = true;
	    $params["descripcion"]  = "Para ser competitivos en la rama de la construcción es importante evaluar el nivel tecnológico actual, para así incorporar nuevas tendencias que permitan obtener mejores resultados y hagan eficiente el proceso constructivo. Por eso ponemos a tu disposición información sobre la evolución de la construcción en nuestro país y las últimas tendencias técnologicas que se están aplicando en el Perú y el mundo.";
	    if($finalizado==""){
		    $this->load->view('header',array("tipo"=>"gris_oscuro","seccion"=>"capacitaciones"));
		    $this->load->view('capacitacion',$params);
	    }else{
		    $this->load->view('header',array("tipo"=>"gris_oscuro","seccion"=>"capacitaciones"));
		    $this->load->view('finalizado',$params);
	    }
	    $this->load->view('footer_btns');
    }
	public function como_mejorar($finalizado=""){
		$actual_link = (isset($_SERVER['HTTPS']) && $_SERVER['HTTPS'] === 'on' ? "https" : "http") . "://$_SERVER[HTTP_HOST]$_SERVER[REQUEST_URI]";
		if(!$this->session->userdata('is_logged_in')){
			redirect('login?r='.$actual_link);
        }
	    $params = array();
	    $params["titulo"] = "Cómo mejorar la productividad en la construcción. ";
	    $params["curso"] = 3;
	    $params["slug"] = "como_mejorar";
	    $params["certificado_directo"] = true;
	    $params["descripcion"]  = "En los últimos años, el concepto de productividad ha ganado importancia en la industria de la construcción, ya que permite medir la eficiencia del trabajo realizado. Por eso, ponemos a tu disposición la siguiente información, en donde te mostraremos cómo administrar de manera eficiente los recursos y así mejorar la productividad de tus proyectos de construcción. De esta forma, podrás planificar y optimizar los tiempos y costos del proyecto, para hacerlo más rentable.";
	    if($finalizado==""){
		    $this->load->view('header',array("tipo"=>"gris_oscuro","seccion"=>"capacitaciones"));
		    $this->load->view('capacitacion',$params);
	    }else{
		    $this->load->view('header',array("tipo"=>"gris_oscuro","seccion"=>"capacitaciones"));
		    $this->load->view('finalizado',$params);
	    }
	    $this->load->view('footer_btns');
    }
	public function riesgos($finalizado=""){
	    $actual_link = (isset($_SERVER['HTTPS']) && $_SERVER['HTTPS'] === 'on' ? "https" : "http") . "://$_SERVER[HTTP_HOST]$_SERVER[REQUEST_URI]";
		if(!$this->session->userdata('is_logged_in')){
			redirect('login?r='.$actual_link);
        }
	    $params = array();
	    $params["titulo"] = "Los seis principales riesgos de construcción y cómo reducirlos.";
	     $params["slug"] = "riesgos";
	     $params["certificado_directo"] = true;
	     $params["descripcion"]  = "Generalmente nos encontramos con una serie de errores en calidad, eficiencia y tiempo de ejecución al iniciar y/o durante un proyecto de construcción. Por eso, es importante conocer los principales riesgos y errores que se comenten en la construcción, y sobretodo, aprender a cómo reducirlos. De esta manera, podrás garantizar el bienestar de tu personal y los correctos procedimientos de seguridad.";
	    if($finalizado==""){
		    $this->load->view('header',array("tipo"=>"gris_oscuro","seccion"=>"capacitaciones"));
		    $this->load->view('capacitacion',$params);
	    }else{
		    $this->load->view('header',array("tipo"=>"blanco","seccion"=>"capacitaciones"));
		    $this->load->view('finalizado',$params);
	    }
	    $this->load->view('footer_btns');
    }
    
    
}
