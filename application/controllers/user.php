<?php

class User extends CI_Controller {

    /**
    * Check if the user is logged in, if he's not,
    * send him to the login page
    * @return void
    */
	function index()
	{
		/*
		if($this->session->userdata('is_logged_in')){
			$redireccion = $this->session->userdata("redireccion");

			if ($redireccion) {
				redirect(base64_decode($redireccion));
			}
			else{
				redirect('inicio');
			}
        }else{
			//$this->load->view('login');
        }*/
         $this->load->view('pre');
	}
	function test()
    {
	 	//echo "*";
	 	//$this->load->view('beneficios.php');


	 	 $this->load->view('beneficios.php');
	}
    /**
    * encript the password
    * @return mixed
    */
    function __encrip_password($password) {
        return md5($password);
    }

    /**
    * check the username and the password with the database
    * @return void
    */
    function validate_alumno()
	{
		$codigo	  = $_POST["codigo"];
		$password = $_POST["password"];
		$sql = "SELECT * FROM alumnos WHERE codigo='$codigo' AND password='$password'";
		$query = $this->db->query($sql);
		$response = array();
		$flag=false;
		$item = array();
		foreach ($query->result() as $fila){
			$item["id"] 				= $fila->id;
			$item["apellido_paterno"] 	= $fila->apellido_paterno;
			$item["apellido_materno"] 	= $fila->apellido_materno;
			$item["modulo"] 	= $fila->notas;
			$flag=true;
			break;
		}
		if($flag){
			$response["status"] = "OK";
			$response["data"] = $item;
		}else{
			$response["status"] = "NO_ENCONTRADO";
		}
		echo json_encode($response);
	}
	function validate_credentials_admin(){

		$this->load->model('Users_model');
		$user_name = $this->input->post('user_name');
		$password = $this->__encrip_password($this->input->post('password'));
		
		


		$query = $this->Users_model->validate($user_name, $password);
		
		/*echo $user_name."<br>";
		echo $password."<br>";
		echo $query->num_rows."<br>";
		
		exit();*/
		if($query->num_rows == 1){
			
			foreach ($query->result() as $row){
				if($row->perfil=="Administrador"){
					$data = array(
						'user_name' => $user_name,
						'id' => $row->id,
						'perfil' => "Administrador",
						'is_logged_in' => true
					);
					$this->session->set_userdata($data);
					redirect('/cms/inicio');
				}else{
					$data['message_error'] = TRUE;
					$this->load->view('admin/login',$data);
				}
				break;
			}

		}else{
			$data['message_error'] = TRUE;
			$this->load->view('admin/login',$data);
		}
	}
	function validate_credentials(){

		$this->load->model('Users_model');
		$this->load->model('Users_model');

		$user_name = $this->input->post('user_name');
		$password = $this->__encrip_password($this->input->post('password'));

		$query = $this->Users_model->validate($user_name, $password);
		if($query->num_rows == 1)
		{
			foreach ($query->result() as $row)
			{
				if($row->perfil=="Usuario"){
					$redireccion = $this->session->userdata("redireccion");
					$this->session->set_userdata("redireccion", false);
					$data = array(
						'user_name' => $user_name,
						'id' => $row->id,
						'perfil' => "Usuario",
						'is_logged_in' => true
					);
					$this->session->set_userdata($data);
					if(isset($_REQUEST["r"])){
						redirect($_REQUEST["r"]);
					}else{
						redirect('/capacitaciones');
					}
				}else{
					$data['message_error'] = TRUE;
					$this->load->view('header',array("tipo"=>"gris_oscuro","seccion"=>"programa_profesional"));
					$this->load->view('login', $data);
					$this->load->view('footer');
				}
				
				break;
			}

		}else{
			$data['message_error'] = TRUE;
			$this->load->view('header',array("tipo"=>"gris_oscuro","seccion"=>"programa_profesional"));
			$this->load->view('login', $data);
			$this->load->view('footer');
		}
	}

    /**
    * The method just loads the signup view
    * @return void
    */
	function signup()
	{
		$this->load->view('admin/signup_form');
	}


    /**
    * Create new user and store it in the database
    * @return void
    */
	function create_member()
	{
		$this->load->library('form_validation');

		// field name, error message, validation rules
		$this->form_validation->set_rules('first_name', 'Name', 'trim|required');
		$this->form_validation->set_rules('last_name', 'Last Name', 'trim|required');
		$this->form_validation->set_rules('email_address', 'Email Address', 'trim|required|valid_email');
		$this->form_validation->set_rules('username', 'Username', 'trim|required|min_length[4]');
		$this->form_validation->set_rules('password', 'Password', 'trim|required|min_length[4]|max_length[32]');
		$this->form_validation->set_rules('password2', 'Password Confirmation', 'trim|required|matches[password]');
		$this->form_validation->set_error_delimiters('<div class="alert alert-error"><a class="close" data-dismiss="alert">×</a><strong>', '</strong></div>');

		if($this->form_validation->run() == FALSE)
		{
			$this->load->view('admin/signup_form');
		}

		else
		{
			$this->load->model('Users_model');

			if($query = $this->Users_model->create_member())
			{
				$this->load->view('admin/signup_successful');
			}
			else
			{
				$this->load->view('admin/signup_form');
			}
		}

	}

	/**
    * Destroy the session, and logout the user.
    * @return void
    */
	function logout()
	{
		$this->session->sess_destroy();
		redirect('login');
	}
	function logout_sistema()
	{
		$this->session->sess_destroy();
		redirect('cms');
	}

}
