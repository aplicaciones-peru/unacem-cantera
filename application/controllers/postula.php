<?php

class Postula extends CI_Controller{

    public function __construct(){
		parent::__construct();
    }
    public function index() {
	    
	    header('Content-Type: application/json');
	    
		$nombres = $_POST["nombres"];
		$apellidos = $_POST["apellidos"];
		$dni = $_POST["dni"];
		$email = $_POST["email"];
		$celular = $_POST["celular"];
		$telefono = $_POST["telefono"];
		$ocupacion = $_POST["ocupacion"];
		$experiencia = $_POST["experiencia"];
		$ocupacion_otros = $_POST["ocupacion_otros"];
		
		$insert_data = array();
		$insert_data["nombres"] 		= $nombres;
		$insert_data["apellidos"] 		= $apellidos;
		$insert_data["dni"] 			= $dni;
		$insert_data["email"] 			= $email;
		$insert_data["celular"] 		= $celular;
		$insert_data["telefono"] 		= $telefono;
		$insert_data["ocupacion"] 		= $ocupacion;
		$insert_data["ocupacion_otros"] = $ocupacion_otros;
		$insert_data["experiencia"] 	= $experiencia;

		$insert_data["fecha"] 			= date('Y-m-d H:i:s');
		
		
		$response = array();
		if($this->validateEmail($email)){
			$this->db->insert("postula", $insert_data);	  
			$response["status"] = "OK";
			echo json_encode($response); 
		}else{
			$response["status"] = "ERROR";
			$response["message"] = "Email no válido";
			echo json_encode($response); 
		}

    }
	function validateEmail($email){
	    $emailIsValid = FALSE;
		if (!empty($email)){
	        $domain = ltrim(stristr($email, '@'), '@') . '.';
	        $user   = stristr($email, '@', TRUE);
			if
	                (
	                    !empty($user) &&
	                    !empty($domain) &&
	                    checkdnsrr($domain)
	                )
	                {$emailIsValid = TRUE;}
	        }
	
	    return $emailIsValid;
	}
}